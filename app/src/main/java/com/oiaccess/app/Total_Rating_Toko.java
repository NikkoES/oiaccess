package com.oiaccess.app;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class Total_Rating_Toko extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.total_rating_toko);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ulasan Rating");
        setTitleColor(R.color.white);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                finish();
            }
        });


        RecyclerView recyclerView= findViewById(R.id.totalratingtoko);
        List<Isi_Rating_Toko> mlist= new ArrayList<>();

        mlist.add(new Isi_Rating_Toko(R.drawable.addimage, "aliando","Barang Bagus banget ga nyesel beli di sini"));
        mlist.add(new Isi_Rating_Toko(R.drawable.fashion1, "Paradita","lebih murah di toko sebelah ternyata"));
        mlist.add(new Isi_Rating_Toko(R.drawable.image_1, "Jois","kereeeeenn"));
        mlist.add(new Isi_Rating_Toko(R.drawable.image_2, "Bang Hamka","Harus di tingkatkan lagi untuk kecepatan pengiriman nya ya"));
        mlist.add(new Isi_Rating_Toko(R.drawable.image_3, "Ahmad iswan","tidak mengecewakan lah"));
        mlist.add(new Isi_Rating_Toko(R.drawable.image_8, "Dida","Mantabsss siaa"));



        Adapter_Rating_Toko adapter = new Adapter_Rating_Toko(this, mlist);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }
}
