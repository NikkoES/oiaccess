package com.oiaccess.app;

public class Isi_Rating_Toko {
    int profiluser;
    String namauser;
    String isideskripsi;

    public Isi_Rating_Toko(){
    }

    public Isi_Rating_Toko(int profiluser, String namauser, String isideskripsi){
        this.profiluser = profiluser;
        this.namauser = namauser;
        this.isideskripsi = isideskripsi;

    }

    public int getBackground(){
        return profiluser;
    }

    public String getProfilName(){
        return namauser;
    }

    public String getIsideskripsi(){
        return isideskripsi;
    }

//    public int getProfilPhoto(){
//        return profilPhoto;
//    }
//
//    public int getNbfollower(){
//        return nbfollower;
//    }

    public void setBackground(int background){
        this.profiluser = background;
    }

    public void setProfilName(String profilName){ this.namauser = profilName;
    }

    public void setIsideskripsi(String isideskripsi){
        this.isideskripsi = isideskripsi;
    }
//
//    public void setProfilPhoto(int profilPhoto){
//        this.profilPhoto = profilPhoto;
//    }
//
//    public void setFollower(int nbfollower){
//        this.nbfollower = nbfollower;
//    }
}
