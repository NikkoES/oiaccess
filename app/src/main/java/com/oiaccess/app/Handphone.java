package com.oiaccess.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import com.oiaccess.app.ui.activity.product.SearchProductActivity;
import com.oiaccess.app.ui.adapter.recycler_view.KategoriAdapter;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class Handphone extends AppCompatActivity {

    CarouselView carouselView;
    int[] sampleImages = {R.drawable.image_1, R.drawable.image_2, R.drawable.image_3, R.drawable.image_4};

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handphone);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(imageListener);

        RecyclerView recyclerView= findViewById(R.id.myhandphone);
        List<IsiHandphone> mlist= new ArrayList<>();

        mlist.add(new IsiHandphone(R.drawable.dumyhp, "HANDPHONE"));
        mlist.add(new IsiHandphone(R.drawable.dumylcd, "KOMPONEN TABLET"));
        mlist.add(new IsiHandphone(R.drawable.dumykomponen, "KOMPONEN HANDPHONE"));
        mlist.add(new IsiHandphone(R.drawable.dumypowerbank, "POWER BANK"));
        mlist.add(new IsiHandphone(R.drawable.dumysmartwatch, "SMARTWATCH"));
        mlist.add(new IsiHandphone(R.drawable.dumytablet, "TABLET"));
        mlist.add(new IsiHandphone(R.drawable.dumyaksesoris, "AKSESORIS HANDPHONE"));
        mlist.add(new IsiHandphone(R.drawable.dumyaksesoristablet2, "AKSESORIS TABLET"));

        Adapter_Handphone adapter = new Adapter_Handphone(this, mlist);
        recyclerView.setAdapter(adapter);

      //  recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };
}

