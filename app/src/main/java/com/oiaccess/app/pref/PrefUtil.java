package com.oiaccess.app.pref;

/**
 * Created by Dion on 3/17/2017.
 */

public class PrefUtil {
    public static final String PREF_NAME = "myPref";
    public static final String PREF_USER_IS_LOGIN = "user_login_status";
    public static final String PREF_USER_SIGNATURE = "user_sig";
    public static final String PREF_USER_REFRESH_SIGNATURE = "refresh_sig";
    public static final String PREF_USER_EMAIL = "user_email";
    public static final String PREF_USERNAME = "username";
    public static final String PREF_USER_FULLNAME = "fullname";
    public static final String PREF_USER_PHONE = "user_phone";
    public static final String PREF_VACC = "vacc_number";
    public static final String PREF_USER_PASSWORD = "user_pass";
    public static final String PREF_USER_ID = "userid";
    public static final String PREF_VERIFIED = "is_verified";
    public static final String PREF_STATUS_VERIFICATION = "user_status_verification";
    public static final String PREF_TOKEN_EXPIRE_MILLIS = "token_duration";
    public static final String PREF_OLD_MILLIS = "old_millis";
    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    public static final String PREF_USER_FIRST_TOPUP = "user_first_topup";
    public static final String PREF_USER_OTP = "user_otp";
    public static final String PREF_PRODUCT_TYPE = "product_type";
    public static final String PREF_PRODUCT_PROVIDER = "product_provider";
    public static final String PREF_PRODUCT_NAME = "product_name";
    public static final String PREF_MY_REFERRAL = "user_my_referral";
    public static final String PREF_USER_OTP_FINISH = "user_otp_finished";
    public static final String PREF_USER_LATLONG = "user_lat_long";
    public static final String PREF_LOGIN_VERIFIKASI_STATUS = "status_verifikasi";
    public static final String PREF_LOGIN_VERIFIKASI_MSG = "verifikasi_msg";
    public static final String PREF_USER_IS_CONFIRMED_EMAIL = "is_confirmed_email";
    public static final String PREF_PLAY_INSTALL_REFERRER = "play_install_referrer";

    public static final String PREF_GROUP_DESC = "group_desc";
    public static final String PREF_GROUP_NAME = "group_name";
    public static final String PREF_GROUP_ID = "group_id";
    public static final String PREF_POST_ID = "post_id";
    public static final String PREF_ALLOW_REFRESH = "allow_refresh";

    public static final String PREF_LOGIN_STATUS = "0";


    public static final String ZENDESK_ACCOUNT_KEY = "5A6569GSmGkflvtaIYlwvCy1eUNZDohw";

    public static final String PREF_PROFILE_PHOTO = "profile_photo";

    public static final String PREF_MYFIRST_PERMISSIONS = "first_permission";


}
