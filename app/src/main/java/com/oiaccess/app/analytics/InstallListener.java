package com.oiaccess.app.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by anya on 11/17/2017.
 */

public class InstallListener extends BroadcastReceiver {
    final String INSTALL_PREFERENCE = "installPrefrences";
    final String REFERRAL_URL = "InstallReferral";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Log.i("installReferralTracker", "installReferralTracker Broadcast Receiver info");

        //Getting install url from intent object
        String uri = intent.toURI();

        if (uri != null && uri.length() > 0) {
            int index = uri.indexOf("utm_source=");
            if (index > -1) {

                //Getting require parameters from url
                uri = uri.substring(index, uri.length() - 4);
                Log.i("installReferralTracker", "Referral URI: " + uri);

                //Store paramaters using SharedPreferences
                SharedPreferences settings = context.getSharedPreferences(INSTALL_PREFERENCE, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(REFERRAL_URL, uri);
                editor.commit();

                Log.i("installReferralTracker", "Cached Referral URI: " + uri);
            } else
                Log.i("installReferralTracker", "No Referral URL.");
        }
        Log.i("installReferralTracker", "End");
    }
}
