package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaItem implements Serializable {

    @SerializedName("id_product")
    @Expose
    private Long id_product;

    @SerializedName("jumlah")
    @Expose
    private int jumlah;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("product")
    @Expose
    private MajaProduct product;

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public MajaProduct getProduct() {
        return product;
    }

    public void setProduct(MajaProduct product) {
        this.product = product;
    }
}
