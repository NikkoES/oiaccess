package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaBalance implements Serializable {

    @SerializedName("vacc_number")
    @Expose
    private String vacc_number;

    @SerializedName("balance")
    @Expose
    private int balance;

    @SerializedName("poin")
    @Expose
    private int poin;

    @SerializedName("user_id")
    @Expose
    private Long user_id;

    public String getVacc_number() {
        return vacc_number;
    }

    public void setVacc_number(String vacc_number) {
        this.vacc_number = vacc_number;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getPoin() {
        return poin;
    }

    public void setPoin(int poin) {
        this.poin = poin;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }
}
