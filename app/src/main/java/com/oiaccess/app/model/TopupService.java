package com.oiaccess.app.model;

/**
 * Created by 666 on 29-Mar-18.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TopupService {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("result")
    @Expose
    private TopupServiceResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public TopupServiceResult getResult() {
        return result;
    }

    public void setResult(TopupServiceResult result) {
        this.result = result;
    }

}
