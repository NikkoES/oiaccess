package com.oiaccess.app.model;

import java.util.Date;
import java.util.List;

public class MajaDataKategoriList {

    private List<MajaDataKategori> kategoris;
    private Date lastUpdate;

    public List<MajaDataKategori> getKategoris() {
        return kategoris;
    }

    public void setKategoris(List<MajaDataKategori> kategoris) {
        this.kategoris = kategoris;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
