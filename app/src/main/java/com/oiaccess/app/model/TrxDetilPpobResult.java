package com.oiaccess.app.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrxDetilPpobResult {



    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("trx_date")
    @Expose
    private String trxDate;
    @SerializedName("admin_fee")
    @Expose
    private String adminFee;
    @SerializedName("commision")
    @Expose
    private String commision;
    @SerializedName("trx_status")
    @Expose
    private String trxStatus;
    @SerializedName("trx_desc")
    @Expose
    private String trxDesc;
    @SerializedName("trx_amount")
    @Expose
    private String trxAmount;
    @SerializedName("reffnum")
    @Expose
    private String reffnum;
    @SerializedName("biller_price")
    @Expose
    private String billerPrice;
    @SerializedName("price_after_discount")
    @Expose
    private String priceAfterDiscount;
    @SerializedName("voucher_code")
    @Expose
    private String voucherCode;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("base_price")
    @Expose
    private String basePrice;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("admin_bank")
    @Expose
    private String adminBank;
    @SerializedName("cashback")
    @Expose
    private String cashback;
    @SerializedName("nominal")
    @Expose
    private String nominal;
    @SerializedName("voucher_nominal")
    @Expose
    private String voucherNominal;
    @SerializedName("trx_id2")
    @Expose
    private String trxId2;
    @SerializedName("customer_number")
    @Expose
    private String customerNumber;
    @SerializedName("detil1_txt")
    @Expose
    private String detil1Txt;
    @SerializedName("detil1_val")
    @Expose
    private String detil1Val;
    @SerializedName("detil2_txt")
    @Expose
    private String detil2Txt;
    @SerializedName("detil2_val")
    @Expose
    private String detil2Val;
    @SerializedName("detil3_txt")
    @Expose
    private String detil3Txt;
    @SerializedName("detil3_val")
    @Expose
    private String detil3Val;
    @SerializedName("detil4_txt")
    @Expose
    private String detil4Txt;
    @SerializedName("detil4_val")
    @Expose
    private String detil4Val;
    @SerializedName("detil5_txt")
    @Expose
    private String detil5Txt;
    @SerializedName("detil5_val")
    @Expose
    private String detil5Val;
    @SerializedName("detil6_txt")
    @Expose
    private String detil6Txt;
    @SerializedName("detil6_val")
    @Expose
    private String detil6Val;
    @SerializedName("detil7_txt")
    @Expose
    private String detil7Txt;
    @SerializedName("detil7_val")
    @Expose
    private String detil7Val;
    @SerializedName("detil8_txt")
    @Expose
    private String detil8Txt;
    @SerializedName("detil8_val")
    @Expose
    private String detil8Val;
    @SerializedName("detil9_txt")
    @Expose
    private String detil9Txt;
    @SerializedName("detil9_val")
    @Expose
    private String detil9Val;
    @SerializedName("detil10_txt")
    @Expose
    private String detil10Txt;
    @SerializedName("detil10_val")
    @Expose
    private String detil10Val;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(String adminFee) {
        this.adminFee = adminFee;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getTrxStatus() {
        return trxStatus;
    }

    public void setTrxStatus(String trxStatus) {
        this.trxStatus = trxStatus;
    }

    public String getTrxDesc() {
        return trxDesc;
    }

    public void setTrxDesc(String trxDesc) {
        this.trxDesc = trxDesc;
    }

    public String getTrxAmount() {
        return trxAmount;
    }

    public void setTrxAmount(String trxAmount) {
        this.trxAmount = trxAmount;
    }

    public String getReffnum() {
        return reffnum;
    }

    public void setReffnum(String reffnum) {
        this.reffnum = reffnum;
    }

    public String getBillerPrice() {
        return billerPrice;
    }

    public void setBillerPrice(String billerPrice) {
        this.billerPrice = billerPrice;
    }

    public String getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(String priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getAdminBank() {
        return adminBank;
    }

    public void setAdminBank(String adminBank) {
        this.adminBank = adminBank;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getVoucherNominal() {
        return voucherNominal;
    }

    public void setVoucherNominal(String voucherNominal) {
        this.voucherNominal = voucherNominal;
    }

    public String getTrxId2() {
        return trxId2;
    }

    public void setTrxId2(String trxId2) {
        this.trxId2 = trxId2;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDetil1Txt() {
        return detil1Txt;
    }

    public void setDetil1Txt(String detil1Txt) {
        this.detil1Txt = detil1Txt;
    }

    public String getDetil1Val() {
        return detil1Val;
    }

    public void setDetil1Val(String detil1Val) {
        this.detil1Val = detil1Val;
    }

    public String getDetil2Txt() {
        return detil2Txt;
    }

    public void setDetil2Txt(String detil2Txt) {
        this.detil2Txt = detil2Txt;
    }

    public String getDetil2Val() {
        return detil2Val;
    }

    public void setDetil2Val(String detil2Val) {
        this.detil2Val = detil2Val;
    }

    public String getDetil3Txt() {
        return detil3Txt;
    }

    public void setDetil3Txt(String detil3Txt) {
        this.detil3Txt = detil3Txt;
    }

    public String getDetil3Val() {
        return detil3Val;
    }

    public void setDetil3Val(String detil3Val) {
        this.detil3Val = detil3Val;
    }

    public String getDetil4Txt() {
        return detil4Txt;
    }

    public void setDetil4Txt(String detil4Txt) {
        this.detil4Txt = detil4Txt;
    }

    public String getDetil4Val() {
        return detil4Val;
    }

    public void setDetil4Val(String detil4Val) {
        this.detil4Val = detil4Val;
    }

    public String getDetil5Txt() {
        return detil5Txt;
    }

    public void setDetil5Txt(String detil5Txt) {
        this.detil5Txt = detil5Txt;
    }

    public String getDetil5Val() {
        return detil5Val;
    }

    public void setDetil5Val(String detil5Val) {
        this.detil5Val = detil5Val;
    }

    public String getDetil6Txt() {
        return detil6Txt;
    }

    public void setDetil6Txt(String detil6Txt) {
        this.detil6Txt = detil6Txt;
    }

    public String getDetil6Val() {
        return detil6Val;
    }

    public void setDetil6Val(String detil6Val) {
        this.detil6Val = detil6Val;
    }

    public String getDetil7Txt() {
        return detil7Txt;
    }

    public void setDetil7Txt(String detil7Txt) {
        this.detil7Txt = detil7Txt;
    }

    public String getDetil7Val() {
        return detil7Val;
    }

    public void setDetil7Val(String detil7Val) {
        this.detil7Val = detil7Val;
    }

    public String getDetil8Txt() {
        return detil8Txt;
    }

    public void setDetil8Txt(String detil8Txt) {
        this.detil8Txt = detil8Txt;
    }

    public String getDetil8Val() {
        return detil8Val;
    }

    public void setDetil8Val(String detil8Val) {
        this.detil8Val = detil8Val;
    }

    public String getDetil9Txt() {
        return detil9Txt;
    }

    public void setDetil9Txt(String detil9Txt) {
        this.detil9Txt = detil9Txt;
    }

    public String getDetil9Val() {
        return detil9Val;
    }

    public void setDetil9Val(String detil9Val) {
        this.detil9Val = detil9Val;
    }

    public String getDetil10Txt() {
        return detil10Txt;
    }

    public void setDetil10Txt(String detil10Txt) {
        this.detil10Txt = detil10Txt;
    }

    public String getDetil10Val() {
        return detil10Val;
    }

    public void setDetil10Val(String detil10Val) {
        this.detil10Val = detil10Val;
    }


}
