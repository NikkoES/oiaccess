package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OiNotifikasi {

    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorcode() {
        return error_code;
    }

    public void setErrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrormesagge() {
        return error_message;
    }

    public void setErrormesagge(String error_message) {
        this.error_message = error_message;
    }

    public static class Result {
/*
        public Profile(String load) {
        }
*/
        @SerializedName("ids")
        @Expose
        private String ids;

        public String getIds() {
            return ids;
        }
        public void setIds(String ids) {
            this.ids = ids;
        }

        @SerializedName("notifikasi")
        @Expose
        private String notifikasi;

        public String getNotifikasi() {
            return notifikasi;
        }
        public void setNotifikasi(String notifikasi) {
            this.notifikasi = notifikasi;
        }

        @SerializedName("userid")
        @Expose
        private String userid;

        public String getUserid() {
            return userid;
        }
        public void setUserid(String userid) {
            this.userid = userid;
        }


        @SerializedName("created_on")
        @Expose
        private String created_on;

        public String getCreated_on() {
            return created_on;
        }
        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

    }

}
