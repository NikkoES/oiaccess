package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Common {

    @SerializedName("result")
    private List<String> result;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
/*
    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;
*/
    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
/*
    public String geterrorcode() {
        return error_code;
    }

    public void seterrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String geterrormessage() {
        return error_message;
    }

    public void seterrormessage(String error_message) {
        this.error_message = error_message;
    }
*/

}
