package com.oiaccess.app.model;

public class MajaKategori {
    private MajaDataKategoriList datakategorilist;

    public MajaDataKategoriList getDataKategoriList() {
        return datakategorilist;
    }

    public void setDataKategoriList(MajaDataKategoriList datakategorilist) {
        this.datakategorilist = datakategorilist;
    }
}
