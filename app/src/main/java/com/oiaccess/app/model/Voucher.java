package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by don on 10/27/2017.
 */

public class Voucher {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public List<Result> result = new ArrayList<>();

    public class Result {

        @SerializedName("ids")
        @Expose
        public String ids;
        @SerializedName("reward_poin_id")
        @Expose
        public String rewardPoinId;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("id_voucher")
        @Expose
        public String idVoucher;
        @SerializedName("create_on")
        @Expose
        public String createOn;
        @SerializedName("poin_reward_desc")
        @Expose
        public String poinRewardDesc;
        @SerializedName("poin_reward_value")
        @Expose
        public String poinRewardValue;
        @SerializedName("voucher_code")
        @Expose
        public String voucherCode;
        @SerializedName("start_date")
        @Expose
        public String startDate;
        @SerializedName("end_date")
        @Expose
        public String endDate;
        @SerializedName("poin_reward_image")
        @Expose
        public String poin_reward_image;

        public String getIds() {
            return ids;
        }

        public void setIds(String ids) {
            this.ids = ids;
        }

        public String getRewardPoinId() {
            return rewardPoinId;
        }

        public void setRewardPoinId(String rewardPoinId) {
            this.rewardPoinId = rewardPoinId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getIdVoucher() {
            return idVoucher;
        }

        public void setIdVoucher(String idVoucher) {
            this.idVoucher = idVoucher;
        }

        public String getCreateOn() {
            return createOn;
        }

        public void setCreateOn(String createOn) {
            this.createOn = createOn;
        }

        public String getPoinRewardDesc() {
            return poinRewardDesc;
        }

        public void setPoinRewardDesc(String poinRewardDesc) {
            this.poinRewardDesc = poinRewardDesc;
        }

        public String getPoinRewardValue() {
            return poinRewardValue;
        }

        public void setPoinRewardValue(String poinRewardValue) {
            this.poinRewardValue = poinRewardValue;
        }

        public String getVoucherCode() {
            return voucherCode;
        }

        public void setVoucherCode(String voucherCode) {
            this.voucherCode = voucherCode;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getPoin_reward_image() {
            return poin_reward_image;
        }

        public void setPoin_reward_image(String poin_reward_image) {
            this.poin_reward_image = poin_reward_image;
        }
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }
}
