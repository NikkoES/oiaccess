package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by don on 10/9/2017.
 */

public class UpdateProfile {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("fullname")
        @Expose
        public String fullname;
        @SerializedName("email")
        @Expose
        public String emailAddresss;
        @SerializedName("no_hp")
        @Expose
        public String noHp;
        @SerializedName("gerai_name")
        @Expose
        public String geraiName;
        @SerializedName("is_verified")
        @Expose
        public String isVerified;
        @SerializedName("is_active")
        @Expose
        public String isActive;
        @SerializedName("fcm_id")
        @Expose
        public String fcmId;
        @SerializedName("referal_code")
        @Expose
        public String referalCode;
        @SerializedName("my_referal_code")
        @Expose
        public String myReferalCode;
        @SerializedName("id_card_number")
        @Expose
        public String idCardNumber;
        @SerializedName("id_card_number_photo")
        @Expose
        public String idCardNumberPhoto;
        @SerializedName("id_card_number_selfi")
        @Expose
        public String idCardNumberSelfi;
        @SerializedName("avatar")
        @Expose
        public String avatar;
        @SerializedName("is_confirmed_hp")
        @Expose
        public String isConfirmedHp;
        @SerializedName("is_confirmed_email")
        @Expose
        public String isConfirmedEmail;
        @SerializedName("village")
        @Expose
        public String village;
        @SerializedName("districts")
        @Expose
        public String districts;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("province")
        @Expose
        public String province;
        @SerializedName("postalcode")
        @Expose
        public String postalcode;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("dob")
        @Expose
        public String dob;
        @SerializedName("pob")
        @Expose
        public String pob;
        @SerializedName("job")
        @Expose
        public String job;
        @SerializedName("mom")
        @Expose
        public String mom;
        @SerializedName("gender")
        @Expose
        public String gender;

        @SerializedName("profile_photo")
        @Expose
        public String profilephoto;

        @SerializedName("status_nikah")
        @Expose
        public String status_nikah;

        @SerializedName("gol_darah")
        @Expose
        public String gol_darah;

        @SerializedName("pendidikan")
        @Expose
        public String pendidikan;

        @SerializedName("gelar_sarjana")
        @Expose
        public String gelar_sarjana;

        @SerializedName("keahlian_profesional1")
        @Expose
        public String keahlian_profesional1;

        @SerializedName("keahlian_profesional2")
        @Expose
        public String keahlian_profesional2;

        @SerializedName("th_masuk_anggota")
        @Expose
        public String th_masuk_anggota;

        @SerializedName("oi_group_id")
        @Expose
        public String oi_group_id;

        @SerializedName("no_anggota")
        @Expose
        public String no_anggota;

        @SerializedName("verifikasi_msg")
        @Expose
        public String verifikasi_msg;

        @SerializedName("group_name")
        @Expose
        public String group_name;

        @SerializedName("bpw_oi")
        @Expose
        public String bpw_oi;

        @SerializedName("bpk_oi")
        @Expose
        public String bpk_oi;


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return emailAddresss;
        }

        public void setEmail(String email) {
            this.emailAddresss = email;
        }

        public String getNoHp() {
            return noHp;
        }

        public void setNoHp(String noHp) {
            this.noHp = noHp;
        }

        public String getGeraiName() {
            return geraiName;
        }

        public void setGeraiName(String geraiName) {
            this.geraiName = geraiName;
        }

        public String getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(String isVerified) {
            this.isVerified = isVerified;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getFcmId() {
            return fcmId;
        }

        public void setFcmId(String fcmId) {
            this.fcmId = fcmId;
        }

        public String getReferalCode() {
            return referalCode;
        }

        public void setReferalCode(String referalCode) {
            this.referalCode = referalCode;
        }

        public String getMyReferalCode() {
            return myReferalCode;
        }

        public void setMyReferalCode(String myReferalCode) {
            this.myReferalCode = myReferalCode;
        }

        public String getIdCardNumber() {
            return idCardNumber;
        }

        public void setIdCardNumber(String idCardNumber) {
            this.idCardNumber = idCardNumber;
        }

        public String getIdCardNumberPhoto() {
            return idCardNumberPhoto;
        }

        public void setIdCardNumberPhoto(String idCardNumberPhoto) {
            this.idCardNumberPhoto = idCardNumberPhoto;
        }

        public String getIdCardNumberSelfi() {
            return idCardNumberSelfi;
        }

        public void setIdCardNumberSelfi(String idCardNumberSelfi) {
            this.idCardNumberSelfi = idCardNumberSelfi;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getIsConfirmedHp() {
            return isConfirmedHp;
        }

        public void setIsConfirmedHp(String isConfirmedHp) {
            this.isConfirmedHp = isConfirmedHp;
        }

        public String getIsConfirmedEmail() {
            return isConfirmedEmail;
        }

        public void setIsConfirmedEmail(String isConfirmedEmail) {
            this.isConfirmedEmail = isConfirmedEmail;
        }

        public String getDistricts() {
            return districts;
        }

        public void setDistricts(String districts) {
            this.districts = districts;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getPostalcode() {
            return postalcode;
        }

        public void setPostalcode(String postalcode) {
            this.postalcode = postalcode;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getPob() {
            return pob;
        }

        public void setPob(String pob) {
            this.pob = pob;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public String getMom() {
            return mom;
        }

        public void setMom(String mom) {
            this.mom = mom;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getProfilephoto() {
            return profilephoto;
        }

        public void setProfilephoto(String profilephoto) {
            this.profilephoto = profilephoto;
        }

        public String getstatus_nikah() {
            return status_nikah;
        }

        public void setstatus_nikah(String status_nikah) {
            this.status_nikah = status_nikah;
        }

        public String getgol_darah() {
            return gol_darah;
        }

        public void setgol_darah(String gol_darah) {
            this.gol_darah = gol_darah;
        }

        public String getpendidikan() {
            return pendidikan;
        }

        public void setpendidikan(String pendidikan) {
            this.pendidikan = pendidikan;
        }

        public String getgelar_sarjana() {
            return gelar_sarjana;
        }

        public void setgelar_sarjana(String gelar_sarjana) {
            this.gelar_sarjana = gelar_sarjana;
        }

        public String getkeahlian_profesional1() {
            return keahlian_profesional1;
        }

        public void setkeahlian_profesional1(String keahlian_profesional1) {
            this.keahlian_profesional1 = keahlian_profesional1;
        }

        public String getkeahlian_profesional2() {
            return keahlian_profesional2;
        }

        public void setkeahlian_profesionall2(String keahlian_profesional2) {
            this.keahlian_profesional2 = keahlian_profesional2;
        }

        public String getth_masuk_anggota() {
            return th_masuk_anggota;
        }

        public void setth_masuk_anggota(String th_masuk_anggota) {
            this.th_masuk_anggota = th_masuk_anggota;
        }

        public String getoi_group_id() {
            return oi_group_id;
        }

        public void setoi_group_id(String oi_group_id) {
            this.oi_group_id = oi_group_id;
        }

        public String getno_anggota() {
            return no_anggota;
        }

        public void setno_anggota(String no_anggota) {
            this.no_anggota = no_anggota;
        }

        public String getVerifikasi_msg() {
            return verifikasi_msg;
        }

        public void setVerifikasi_msgr(String verifikasi_msg) {
            this.verifikasi_msg = verifikasi_msg;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getBpw_oi() {
            return bpw_oi;
        }

        public void setBpw_oi(String bpw_oi) {
            this.bpw_oi = bpw_oi;
        }

        public String getBpk_oi() {
            return bpk_oi;
        }

        public void setBpk_oi(String bpk_oi) {
            this.bpk_oi = bpk_oi;
        }
    }
}

