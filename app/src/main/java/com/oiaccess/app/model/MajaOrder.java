package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MajaOrder implements Serializable {

    @SerializedName("orders")
    @Expose
    private List<MajaAddOrder> orders;

    public List<MajaAddOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<MajaAddOrder> orders) {
        this.orders = orders;
    }
}
