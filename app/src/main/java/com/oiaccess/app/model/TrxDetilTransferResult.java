package com.oiaccess.app.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrxDetilTransferResult {

    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("trx_type")
    @Expose
    private String trxType;
    @SerializedName("trx_date")
    @Expose
    private String trxDate;
    @SerializedName("trx_desc")
    @Expose
    private String trxDesc;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("dk")
    @Expose
    private String dk;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("vacc_number")
    @Expose
    private String vaccNumber;
    @SerializedName("vacc_from")
    @Expose
    private String vaccFrom;
    @SerializedName("vacc_to")
    @Expose
    private String vaccTo;
    @SerializedName("trx_id2")
    @Expose
    private String trxId2;
    @SerializedName("vacc_from_fullname")
    @Expose
    private String vaccFromFullname;
    @SerializedName("vacc_to_fullname")
    @Expose
    private String vaccToFullname;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxDesc() {
        return trxDesc;
    }

    public void setTrxDesc(String trxDesc) {
        this.trxDesc = trxDesc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getVaccNumber() {
        return vaccNumber;
    }

    public void setVaccNumber(String vaccNumber) {
        this.vaccNumber = vaccNumber;
    }

    public String getVaccFrom() {
        return vaccFrom;
    }

    public void setVaccFrom(String vaccFrom) {
        this.vaccFrom = vaccFrom;
    }

    public String getVaccTo() {
        return vaccTo;
    }

    public void setVaccTo(String vaccTo) {
        this.vaccTo = vaccTo;
    }

    public String getTrxId2() {
        return trxId2;
    }

    public void setTrxId2(String trxId2) {
        this.trxId2 = trxId2;
    }

    public String getVaccFromFullname() {
        return vaccFromFullname;
    }

    public void setVaccFromFullname(String vaccFromFullname) {
        this.vaccFromFullname = vaccFromFullname;
    }

    public String getVaccToFullname() {
        return vaccToFullname;
    }

    public void setVaccToFullname(String vaccToFullname) {
        this.vaccToFullname = vaccToFullname;
    }
}
