package com.oiaccess.app.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrxDetilTransfer {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("result")
    @Expose
    private TrxDetilTransferResult result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public TrxDetilTransferResult getResult() {
        return result;
    }

    public void setResult(TrxDetilTransferResult result) {
        this.result = result;
    }
}

