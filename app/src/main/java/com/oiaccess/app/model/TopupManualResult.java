package com.oiaccess.app.model;

/**
 * Created by 666 on 24-Apr-18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopupManualResult {
    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("norek")
    @Expose
    private String norek;
    @SerializedName("rek_name")
    @Expose
    private String rek_name;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("expired_on")
    @Expose
    private String expiredOn;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public String getRekName() {
        return rek_name;
    }

    public void setRekName(String rek_name) {
        this.norek = rek_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getExpiredOn() {
        return expiredOn;
    }

    public void setExpiredOn(String expiredOn) {
        this.expiredOn = expiredOn;
    }
}
