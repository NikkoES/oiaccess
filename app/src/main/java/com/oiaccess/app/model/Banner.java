package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by don on 9/28/2017.
 */

public class Banner {


    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public List<String> result = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }
}
