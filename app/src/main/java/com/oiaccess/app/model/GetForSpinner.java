package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anya on 4/6/2017.
 */

public class GetForSpinner {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public List<Result> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("province")
        @Expose
        public String provinsi;

        @SerializedName("city")
        @Expose
        public String city;

        @SerializedName("districts")
        @Expose
        public String districts;

        @SerializedName("village")
        @Expose
        public String village;

        @SerializedName("pekerjaan")
        @Expose
        public String pekerjaan;

        @SerializedName("jenis")
        @Expose
        public String jenis;

        @SerializedName("nama_bank_va")
        @Expose
        public String nama_bank_va;

        @SerializedName("prod_category")
        @Expose
        public String prod_category;

        @SerializedName("ids")
        @Expose
        public String ids;

        @SerializedName("toko_id")
        @Expose
        public String toko_id;

        @SerializedName("postalcode")
        @Expose
        public String kodepos;


        public String getPekerjaan() { return pekerjaan; }

        public void setPekerjaan(String pekerjaan) { this.pekerjaan = pekerjaan; }

        public String getProvinsi() {
            return provinsi;
        }

        public void setProvinsi(String provinsi) {
            this.provinsi = provinsi;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistricts() {
            return districts;
        }

        public void setDistricts(String districts) {
            this.districts = districts;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String kelurahan) {
            this.village = village;
        }

        public String getKodepos() {
            return kodepos;
        }

        public void setKodepos(String kodepos) {
            this.kodepos = kodepos;
        }

        public String getProd_category() { return prod_category; }

        public void setProd_category(String prod_category) { this.prod_category = prod_category; }

        public String getJenis() { return jenis; }

        public void setJenis(String jenis) { this.jenis = jenis; }

        public String getNama_bank_va() { return nama_bank_va; }

        public void setNama_bank_va(String nama_bank_va) { this.nama_bank_va = nama_bank_va; }

        public String getIds() {
            return ids;
        }

        public void setIds(String ids) {
            this.ids = ids;
        }

        public String getToko_id() {
            return toko_id;
        }

        public void setToko_id(String toko_id) {
            this.toko_id = toko_id;
        }
    }
}
