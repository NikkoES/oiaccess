package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anya on 4/5/2017.
 */

public class VoucherData {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("error_code")
    @Expose
    public String errorCode;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("error_message")
    @Expose
    public String errorMessage;
    @SerializedName("result")
    @Expose
    public List<Result> result = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public class Result {

        @SerializedName("ids")
        @Expose
        public String ids;
        @SerializedName("voucher_code")
        @Expose
        public String voucher_code;
        @SerializedName("voucher_desc")
        @Expose
        public String voucher_desc;
        @SerializedName("voucher_value")
        @Expose
        public String voucher_value;
        @SerializedName("product")
        @Expose
        public String product;
        @SerializedName("start_date")
        @Expose
        public String start_date;
        @SerializedName("end_date")
        @Expose
        public String end_date;
        @SerializedName("is_percent")
        @Expose
        public String is_percent;
        @SerializedName("user_id")
        @Expose
        public String user_id;
        @SerializedName("is_active")
        @Expose
        public String is_active;
        @SerializedName("create_on")
        @Expose
        public String create_on;
        @SerializedName("create_by")
        @Expose
        public String create_by;
        @SerializedName("update_on")
        @Expose
        public String update_on;
        @SerializedName("update_by")
        @Expose
        public String update_by;
        @SerializedName("filename")
        @Expose
        public String filename;
        @SerializedName("point_value")
        @Expose
        public String point_value;

        public String getIds() {
            return ids;
        }

        public void setIds(String ids) {
            this.ids = ids;
        }


        public String getVoucher_Code() {
            return voucher_code;
        }

        public void setVoucher_Code(String voucher_code) {
            this.voucher_code = voucher_code;
        }

        public String getVoucher_Desc() {
            return voucher_desc;
        }

        public void setVoucher_Desc(String email) {
            this.voucher_desc = voucher_desc;
        }

        public String getvoucher_value() {
            return voucher_value;
        }

        public void setvoucher_value(String voucher_value) {
            this.voucher_value = voucher_value;
        }

        public String getproduct() {
            return product;
        }

        public void setproduct(String product) {
            this.product = product;
        }

        public String getstart_date() {
            return start_date;
        }

        public void setstart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getend_date() {
            return end_date;
        }

        public void setend_date(String end_date) {
            this.end_date = end_date;
        }

        public String getis_percent() {
            return is_percent;
        }

        public void setis_percent(String is_percent) {
            this.is_percent = is_percent;
        }

        public String getuser_id() {
            return user_id;
        }

        public void setuser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getis_active() {
            return is_active;
        }

        public void setis_active(String is_active) {
            this.is_active = is_active;
        }

        public String getcreate_on() {
            return create_on;
        }

        public void setcreate_on(String create_on) {
            this.create_on = create_on;
        }

        public String getcreate_by() {
            return create_by;
        }

        public void setcreate_by(String create_by) {
            this.create_by = create_by;
        }

        public String getupdate_on() {
            return update_on;
        }

        public void setupdate_on(String update_on) {
            this.update_on = update_on;
        }

        public String getupdate_by() {
            return update_by;
        }

        public void setupdate_by(String update_by) {
            this.update_by = update_by;
        }

        public String getfilename() {
            return filename;
        }

        public void setfilename(String filename) {
            this.filename = filename;
        }

        public String getpoint_value() {
            return point_value;
        }

        public void setpoint_value(String point_value) {
            this.point_value = point_value;
        }

    }
}
