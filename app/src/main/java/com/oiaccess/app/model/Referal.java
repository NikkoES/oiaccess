package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anya on 11/13/2017.
 */

public class Referal {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public List<Result> result = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("fullname")
        @Expose
        public String fullname;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("vacc_number")
        @Expose
        public String vaccNumber;

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getVaccNumber() {
            return vaccNumber;
        }

        public void setVaccNumber(String vaccNumber) {
            this.vaccNumber = vaccNumber;
        }
    }
}
