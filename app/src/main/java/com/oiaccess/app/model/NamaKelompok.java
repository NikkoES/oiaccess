package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NamaKelompok {

    @SerializedName("result")
    private List<Result> result;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorcode() {
        return error_code;
    }

    public void setErrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrormesagge() {
        return error_message;
    }

    public void setErrormesagge(String error_message) {
        this.error_message = error_message;
    }

    public class Result {

        @SerializedName("ids")
        @Expose
        private String ids;

        public String getIds() {
            return ids;
        }

        public void setIds(String ids) {
            this.ids = ids;
        }

        @SerializedName("group_name")
        @Expose
        private String group_name;

        public String getGroupname() {
            return group_name;
        }

        public void setGroupname(String group_name) {
            this.group_name = group_name;
        }

        @SerializedName("city")
        @Expose
        private String city;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }


    }

}
