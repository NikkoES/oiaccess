package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by don on 9/25/2017.
 */

public class TopUp {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("trx_id")
        @Expose
        public String trxId;
        @SerializedName("virtual_account")
        @Expose
        public String virtualAccount;
        @SerializedName("trx_amount")
        @Expose
        public String trxAmount;
        @SerializedName("admin_fee")
        @Expose
        public String adminFee;
        @SerializedName("trx_total")
        @Expose
        public String trxTotal;

        public String getTrxId() {
            return trxId;
        }

        public void setTrxId(String trxId) {
            this.trxId = trxId;
        }

        public String getVirtualAccount() {
            return virtualAccount;
        }

        public void setVirtualAccount(String virtualAccount) {
            this.virtualAccount = virtualAccount;
        }

        public String getTrxAmount() {
            return trxAmount;
        }

        public void setTrxAmount(String trxAmount) {
            this.trxAmount = trxAmount;
        }

        public String getAdminFee() {
            return adminFee;
        }

        public void setAdminFee(String adminFee) {
            this.adminFee = adminFee;
        }

        public String getTrxTotal() {
            return trxTotal;
        }

        public void setTrxTotal(String trxTotal) {
            this.trxTotal = trxTotal;
        }
    }
}
