package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MajaCourierService implements Serializable {

    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("cost")
    @Expose
    private List<MajaCost> cost;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MajaCost> getCost() {
        return cost;
    }

    public void setCost(List<MajaCost> cost) {
        this.cost = cost;
    }
}
