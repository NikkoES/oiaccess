package com.oiaccess.app.model;

/**
 * Created by 666 on 24-Apr-18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopupHistoryResult {


    @SerializedName("topup_id")
    @Expose
    private String topupId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("uniqid")
    @Expose
    private String uniqid;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("norek")
    @Expose
    private String norek;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("confirmed_on")
    @Expose
    private String confirmedOn;
    @SerializedName("confirmed_by")
    @Expose
    private String confirmedBy;
    @SerializedName("approval_on")
    @Expose
    private String approvalOn;
    @SerializedName("approval_by")
    @Expose
    private String approvalBy;
    @SerializedName("expired_on")
    @Expose
    private String expiredOn;
    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("admin_fee")
    @Expose
    private String adminFee;
    @SerializedName("trx_status")
    @Expose
    private String trxStatus;
    @SerializedName("rek_name")
    @Expose
    private String rekName;
    @SerializedName("image1")
    @Expose
    private String image1;
    @SerializedName("image2")
    @Expose
    private String image2;
    @SerializedName("type")
    @Expose
    public String type;

    public TopupHistoryResult(String type) {
        this.type = type;
    }

    public String getTopupId() {
        return topupId;
    }

    public void setTopupId(String topupId) {
        this.topupId = topupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUniqid() {
        return uniqid;
    }

    public void setUniqid(String uniqid) {
        this.uniqid = uniqid;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getConfirmedOn() {
        return null;
    }

    public void setConfirmedOn(String confirmedOn) {
        this.confirmedOn = confirmedOn;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public String getApprovalOn() {
        return approvalOn;
    }

    public void setApprovalOn(String approvalOn) {
        this.approvalOn = approvalOn;
    }

    public String getApprovalBy() {
        return approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public String getExpiredOn() {
        return expiredOn;
    }

    public void setExpiredOn(String expiredOn) {
        this.expiredOn = expiredOn;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(String adminFee) {
        this.adminFee = adminFee;
    }

    public String getTrxStatus() {
        return trxStatus;
    }

    public void setTrxStatus(String trxStatus) {
        this.trxStatus = trxStatus;
    }

    public String getRekName() {
        return rekName;
    }

    public void setRekName(String rekName) {
        this.rekName = rekName;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
