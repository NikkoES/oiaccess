package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaAddressDistrict implements Serializable {

    @SerializedName("subdistrict_id")
    @Expose
    private String subdistrict_id;

    @SerializedName("province_id")
    @Expose
    private String province_id;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("city_id")
    @Expose
    private String city_id;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("subdistrict_name")
    @Expose
    private String subdistrict_name;

    public String getSubdistrict_id() {
        return subdistrict_id;
    }

    public void setSubdistrict_id(String subdistrict_id) {
        this.subdistrict_id = subdistrict_id;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubdistrict_name() {
        return subdistrict_name;
    }

    public void setSubdistrict_name(String subdistrict_name) {
        this.subdistrict_name = subdistrict_name;
    }
}
