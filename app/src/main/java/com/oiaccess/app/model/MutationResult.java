package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by don on 10/2/2017.
 */

public class MutationResult {
    @SerializedName("rows")
    @Expose
    public List<Row> rows = new ArrayList<>();
    @SerializedName("row_per_page")
    @Expose
    public Integer rowPerPage;
    @SerializedName("total_rows")
    @Expose
    public Integer totalRows;
    @SerializedName("total_page")
    @Expose
    public Integer total_page;

    public static class Row {

        @SerializedName("itemno")
        @Expose
        public Integer itemno;
        @SerializedName("trx_id")
        @Expose
        public String trxId;
        @SerializedName("vacc_number")
        @Expose
        public String vaccNumber;
        @SerializedName("dk")
        @Expose
        public String dk;
        @SerializedName("trx_date")
        @Expose
        public String trxDate;
        @SerializedName("trx_desc")
        @Expose
        public String trxDesc;
        @SerializedName("amount")
        @Expose
        public String amount;
        @SerializedName("saldo")
        @Expose
        public String saldo;
        @SerializedName("trx_status")
        @Expose
        public String trxStatus;
        @SerializedName("agent_fee")
        @Expose
        public String agent_fee;
        @SerializedName("type")
        @Expose
        public String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getItemno() {
            return itemno;
        }

        public void setItemno(Integer itemno) {
            this.itemno = itemno;
        }

        public String getTrxId() {
            return trxId;
        }

        public void setTrxId(String trxId) {
            this.trxId = trxId;
        }

        public String getVaccNumber() {
            return vaccNumber;
        }

        public void setVaccNumber(String vaccNumber) {
            this.vaccNumber = vaccNumber;
        }

        public String getDk() {
            return dk;
        }

        public void setDk(String dk) {
            this.dk = dk;
        }

        public String getTrxDate() {
            return trxDate;
        }

        public void setTrxDate(String trxDate) {
            this.trxDate = trxDate;
        }

        public String getTrxDesc() {
            return trxDesc;
        }

        public void setTrxDesc(String trxDesc) {
            this.trxDesc = trxDesc;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getSaldo() {
            return saldo;
        }

        public void setSaldo(String saldo) {
            this.saldo = saldo;
        }

        public String getAgent_fee() {
            return agent_fee;
        }

        public void setAgent_fee(String agent_fee) {
            this.agent_fee = agent_fee;
        }

        public String getTrxStatus() {
            return trxStatus;
        }

        public void setTrxStatus(String trxStatus) {
            this.trxStatus = trxStatus;
        }
        public Row(String type) {
            this.type = type;
        }

    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public Integer getRowPerPage() {
        return rowPerPage;
    }

    public void setRowPerPage(Integer rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    public void setTotal_page(Integer total_page) {
        this.total_page = total_page;
    }
}
