package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class MajaDiscussion implements Serializable {

    @SerializedName("user_name")
    @Expose
    private String user_name;

    @SerializedName("discussion_date")
    @Expose
    private Date discussion_date;

    @SerializedName("discussion_message")
    @Expose
    private String discussion_message;

    @SerializedName("store_name")
    @Expose
    private String store_name;

    @SerializedName("replayment_date")
    @Expose
    private Date replayment_date;

    @SerializedName("replayment_message")
    @Expose
    private String replayment_message;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Date getDiscussion_date() {
        return discussion_date;
    }

    public void setDiscussion_date(Date discussion_date) {
        this.discussion_date = discussion_date;
    }

    public String getDiscussion_message() {
        return discussion_message;
    }

    public void setDiscussion_message(String discussion_message) {
        this.discussion_message = discussion_message;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public Date getReplayment_date() {
        return replayment_date;
    }

    public void setReplayment_date(Date replayment_date) {
        this.replayment_date = replayment_date;
    }

    public String getReplayment_message() {
        return replayment_message;
    }

    public void setReplayment_message(String replayment_message) {
        this.replayment_message = replayment_message;
    }
}
