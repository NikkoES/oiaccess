package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by don on 10/25/2017.
 */

public class Point {
    @SerializedName("status")
    @Expose
    public String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public List<Result> result = new ArrayList<>();


    public class Result {

        @SerializedName("poin_reward_id")
        @Expose
        public String poinRewardId;
        @SerializedName("poin_reward_image")
        @Expose
        public String poinRewardImage;
        @SerializedName("poin_reward_desc")
        @Expose
        public String poinRewardDesc;

        public String getPoinRewardId() {
            return poinRewardId;
        }

        public void setPoinRewardId(String poinRewardId) {
            this.poinRewardId = poinRewardId;
        }

        public String getPoinRewardImage() {
            return poinRewardImage;
        }

        public void setPoinRewardImage(String poinRewardImage) {
            this.poinRewardImage = poinRewardImage;
        }

        public String getPoinRewardDesc() {
            return poinRewardDesc;
        }

        public void setPoinRewardDesc(String poinRewardDesc) {
            this.poinRewardDesc = poinRewardDesc;
        }
    }
}
