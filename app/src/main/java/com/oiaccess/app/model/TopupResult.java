package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by don on 10/3/2017.
 */

public class TopupResult {

    @SerializedName("rows")
    @Expose
    public List<TopupRow> rows = new ArrayList<>();



    @SerializedName("total_rows")
    @Expose
    public String totalRows;

    public List<TopupRow> getRows() {
        return rows;
    }

    public void setRows(List<TopupRow> rows) {
        this.rows = rows;
    }

    public String getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(String totalRows) {
        this.totalRows = totalRows;
    }


    public static class TopupRow {

        @SerializedName("itemno")
        @Expose
        public Integer itemno;
        @SerializedName("trx_id")
        @Expose
        public String trxId;
        @SerializedName("date")
        @Expose
        public String date;
        @SerializedName("virtual_account")
        @Expose
        public String virtualAccount;
        @SerializedName("trx_amount")
        @Expose
        public String trxAmount;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("admin_fee")
        @Expose
        public String adminFee;
        @SerializedName("trx_total")
        @Expose
        public String trxTotal;
        @SerializedName("type")
        @Expose
        public String type;

        public Integer getItemno() {
            return itemno;
        }

        public void setItemno(Integer itemno) {
            this.itemno = itemno;
        }

        public String getTrxId() {
            return trxId;
        }

        public void setTrxId(String trxId) {
            this.trxId = trxId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getVirtualAccount() {
            return virtualAccount;
        }

        public void setVirtualAccount(String virtualAccount) {
            this.virtualAccount = virtualAccount;
        }

        public String getTrxAmount() {
            return trxAmount;
        }

        public void setTrxAmount(String trxAmount) {
            this.trxAmount = trxAmount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAdminFee() {
            return adminFee;
        }

        public void setAdminFee(String adminFee) {
            this.adminFee = adminFee;
        }

        public String getTrxTotal() {
            return trxTotal;
        }

        public void setTrxTotal(String trxTotal) {
            this.trxTotal = trxTotal;
        }

        public TopupRow(String type) {
            this.type = type;
        }


    }

}
