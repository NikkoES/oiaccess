package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MajaProduct implements Serializable {

    @SerializedName("id_product")
    @Expose
    private Long id_product;

    @SerializedName("id_toko")
    @Expose
    private Long id_toko;

    @SerializedName("id_category")
    @Expose
    private Long id_category;

    @SerializedName("nama_product")
    @Expose
    private String nama_product;

    @SerializedName("harga")
    @Expose
    private int harga;

    @SerializedName("harga_diskon")
    @Expose
    private int harga_diskon;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("weight")
    @Expose
    private String weight;

    @SerializedName("stock")
    @Expose
    private String stock;

    @SerializedName("image_name")
    @Expose
    private String image_name;

    @SerializedName("diskusi")
    @Expose
    private String diskusi;

    @SerializedName("nama_toko")
    @Expose
    private String nama_toko;

    @SerializedName("category_name")
    @Expose
    private String category_name;

    @SerializedName("images")
    @Expose
    private List<String> images;

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_toko() {
        return id_toko;
    }

    public void setId_toko(Long id_toko) {
        this.id_toko = id_toko;
    }

    public Long getId_category() {
        return id_category;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public String getNama_product() {
        return nama_product;
    }

    public void setNama_product(String nama_product) {
        this.nama_product = nama_product;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getHarga_diskon() {
        return harga_diskon;
    }

    public void setHarga_diskon(int harga_diskon) {
        this.harga_diskon = harga_diskon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getDiskusi() {
        return diskusi;
    }

    public void setDiskusi(String diskusi) {
        this.diskusi = diskusi;
    }

    public String getNama_toko() {
        return nama_toko;
    }

    public void setNama_toko(String nama_toko) {
        this.nama_toko = nama_toko;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
