package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anya on 4/21/2017.
 */

public class PostalCode {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private Result result;

    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorcode() {
        return error_code;
    }

    public void setErrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrormesagge() {
        return error_message;
    }

    public void setErrormesagge(String error_message) {
        this.error_message = error_message;
    }

    public class Result {

        @SerializedName("postalcode")
        @Expose
        private String kodepos;

        public String getKodepos() {
            return kodepos;
        }

        public void setKodepos(String kodepos) {
            this.kodepos = kodepos;
        }

    }
}
