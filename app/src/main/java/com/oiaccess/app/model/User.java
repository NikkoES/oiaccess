package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anya on 4/5/2017.
 */

public class User {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("error_code")
    @Expose
    public String errorCode;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("error_message")
    @Expose
    public String errorMessage;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public class Result {

        @SerializedName("imei")
        @Expose
        public String imei;
        @SerializedName("is_active")
        @Expose
        public String isActive;
        @SerializedName("pass")
        @Expose
        public String pass;
        @SerializedName("create_on")
        @Expose
        public String create_on;
        @SerializedName("token_expire")
        @Expose
        public int token_expire;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("no_hp")
        @Expose
        public String noHp;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("dob")
        @Expose
        public String dob;
        @SerializedName("pob")
        @Expose
        public String pob;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("province")
        @Expose
        public String province;
        @SerializedName("city")
        @Expose
        public String city;
        @SerializedName("districts")
        @Expose
        public String districts;
        @SerializedName("village")
        @Expose
        public String village;
        @SerializedName("postalcode")
        @Expose
        public String postalcode;
        @SerializedName("job")
        @Expose
        public String job;
        @SerializedName("id_card_number")
        @Expose
        public String idCardNumber;
        @SerializedName("gerai_name")
        @Expose
        public String geraiName;
        @SerializedName("photo_ktp")
        @Expose
        public Object photoKtp;
        @SerializedName("photo_selfie_ktp")
        @Expose
        public Object photoSelfieKtp;
        @SerializedName("is_verified")
        @Expose
        public String isVerified;
        @SerializedName("referal")
        @Expose
        public Object referal;


        @SerializedName("avatar")
        @Expose
        public Object avatar;
        @SerializedName("fcm_id")
        @Expose
        public Object fcmId;

        @SerializedName("my_referal_code")
        @Expose
        public String myReferalCode;
        @SerializedName("access_token")
        @Expose
        public String accessToken;
        @SerializedName("refresh_token")
        @Expose
        public String refreshToken;
        @SerializedName("otp")
        @Expose
        public String otp;
        @SerializedName("postalcode_id")
        @Expose
        public String postalcodeId;
        @SerializedName("vacc_number")
        @Expose
        public String vaccNumber;
        @SerializedName("fullname")
        @Expose
        public String fullname;
        @SerializedName("saldo")
        @Expose
        public String saldo;
        @SerializedName("poin")
        @Expose
        public String poin;

        @SerializedName("status_verifikasi")
        @Expose
        public String status_verifikasi;

        @SerializedName("verifikasi_msg")
        @Expose
        public String verifikasi_msg;

        @SerializedName("is_confirmed_email")
        @Expose
        public String is_confirmed_email;

        public String getIsConfirmedEmail() {
            return is_confirmed_email;
        }

        public void setIsConfirmedEmail(String is_confirmed_email) {
            this.imei = is_confirmed_email;
        }



        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getCreate_on() {
            return create_on;
        }

        public void setCreate_on(String create_on) {
            this.create_on = create_on;
        }

        public int getToken_expire() {
            return token_expire;
        }

        public void setToken_expire(int token_expire) {
            this.token_expire = token_expire;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNoHp() {
            return noHp;
        }

        public void setNoHp(String noHp) {
            this.noHp = noHp;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getPob() {
            return pob;
        }

        public void setPob(String pob) {
            this.pob = pob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistricts() {
            return districts;
        }

        public void setDistricts(String districts) {
            this.districts = districts;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getPostalcode() {
            return postalcode;
        }

        public void setPostalcode(String postalcode) {
            this.postalcode = postalcode;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public String getIdCardNumber() {
            return idCardNumber;
        }

        public void setIdCardNumber(String idCardNumber) {
            this.idCardNumber = idCardNumber;
        }

        public String getGeraiName() {
            return geraiName;
        }

        public void setGeraiName(String geraiName) {
            this.geraiName = geraiName;
        }

        public Object getPhotoKtp() {
            return photoKtp;
        }

        public void setPhotoKtp(Object photoKtp) {
            this.photoKtp = photoKtp;
        }

        public Object getPhotoSelfieKtp() {
            return photoSelfieKtp;
        }

        public void setPhotoSelfieKtp(Object photoSelfieKtp) {
            this.photoSelfieKtp = photoSelfieKtp;
        }

        public String getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(String isVerified) {
            this.isVerified = isVerified;
        }

        public Object getReferal() {
            return referal;
        }

        public void setReferal(Object referal) {
            this.referal = referal;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public Object getFcmId() {
            return fcmId;
        }

        public void setFcmId(Object fcmId) {
            this.fcmId = fcmId;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getPostalcodeId() {
            return postalcodeId;
        }

        public void setPostalcodeId(String postalcodeId) {
            this.postalcodeId = postalcodeId;
        }

        public String getVaccNumber() {
            return vaccNumber;
        }

        public void setVaccNumber(String vaccNumber) {
            this.vaccNumber = vaccNumber;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getSaldo() {
            return saldo;
        }

        public void setSaldo(String saldo) {
            this.saldo = saldo;
        }

        public String getPoin() {
            return poin;
        }

        public void setPoin(String poin) {
            this.poin = poin;
        }

        public String getMyReferalCode() {
            return myReferalCode;
        }

        public void setMyReferalCode(String myReferalCode) {
            this.myReferalCode = myReferalCode;
        }



        public String getStatusVerifikasi() {
            return status_verifikasi;
        }

        public void setStatusVerifikasi(String status_verifikasi) {
            this.fullname = status_verifikasi;
        }

        public String getVerifikasiMsg() {
            return verifikasi_msg;
        }

        public void setVerifikasiMsg(String verifikasi_msg) {
            this.fullname = verifikasi_msg;
        }


    }
}
