package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class MajaOrderList implements Serializable {

    @SerializedName("id_order")
    @Expose
    private Long id_order;

    @SerializedName("order_date")
    @Expose
    private Date order_date;

    @SerializedName("id_user")
    @Expose
    private Long id_user;

    @SerializedName("id_toko")
    @Expose
    private Long id_toko;

    @SerializedName("total_harga")
    @Expose
    private int total_harga;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("courier")
    @Expose
    private String courier;

    @SerializedName("payment_method")
    @Expose
    private String payment_method;

    @SerializedName("status")
    @Expose
    private String status;

    public Long getId_order() {
        return id_order;
    }

    public void setId_order(Long id_order) {
        this.id_order = id_order;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Long getId_toko() {
        return id_toko;
    }

    public void setId_toko(Long id_toko) {
        this.id_toko = id_toko;
    }

    public int getTotal_harga() {
        return total_harga;
    }

    public void setTotal_harga(int total_harga) {
        this.total_harga = total_harga;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCourier() {
        return courier;
    }

    public void setCourier(String courier) {
        this.courier = courier;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
