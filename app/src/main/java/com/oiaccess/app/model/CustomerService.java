package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anya on 12/18/2017.
 */

public class CustomerService {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("cs_email")
        @Expose
        public String csEmail;
        @SerializedName("cs_phone")
        @Expose
        public String csPhone;
        @SerializedName("cs_whatsapp")
        @Expose
        public String csWhatsapp;

        public String getCsEmail() {
            return csEmail;
        }

        public void setCsEmail(String csEmail) {
            this.csEmail = csEmail;
        }

        public String getCsPhone() {
            return csPhone;
        }

        public void setCsPhone(String csPhone) {
            this.csPhone = csPhone;
        }

        public String getCsWhatsapp() {
            return csWhatsapp;
        }

        public void setCsWhatsapp(String csWhatsapp) {
            this.csWhatsapp = csWhatsapp;
        }
    }
}
