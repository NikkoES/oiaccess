package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterUserHP {
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("error_code")
    @Expose
    private String error_code;
    @SerializedName("error_message")
    @Expose
    private String error_message;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorCode() {
        return error_code;
    }

    public void setErrorCode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public void setErrorMessage(String error_message) {
        this.error_message = error_message;
    }

    public class Result {

        @SerializedName("fullname")
        @Expose
        private String fullname;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("salt")
        @Expose
        private String salt;
        @SerializedName("pass")
        @Expose
        private String pass;
        @SerializedName("pin")
        @Expose
        private String pin;
        @SerializedName("is_confirmed_hp")
        @Expose
        private String isConfirmedHp;
        @SerializedName("is_confirmed_email")
        @Expose
        private String isConfirmedEmail;
        @SerializedName("is_active")
        @Expose
        private String isActive;
        @SerializedName("is_verified")
        @Expose
        private String isVerified;
        @SerializedName("referal_code")
        @Expose
        private String referalCode;
        @SerializedName("fcm_id")
        @Expose
        private String fcmId;
        @SerializedName("verifikasi_msg")
        @Expose
        private String verifikasiMsg;
        @SerializedName("status_verifikasi")
        @Expose
        private String statusVerifikasi;
        @SerializedName("email_key")
        @Expose
        private String emailKey;
        @SerializedName("my_referal_code")
        @Expose
        private String myReferalCode;

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getIsConfirmedHp() {
            return isConfirmedHp;
        }

        public void setIsConfirmedHp(String isConfirmedHp) {
            this.isConfirmedHp = isConfirmedHp;
        }

        public String getIsConfirmedEmail() {
            return isConfirmedEmail;
        }

        public void setIsConfirmedEmail(String isConfirmedEmail) {
            this.isConfirmedEmail = isConfirmedEmail;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(String isVerified) {
            this.isVerified = isVerified;
        }

        public String getReferalCode() {
            return referalCode;
        }

        public void setReferalCode(String referalCode) {
            this.referalCode = referalCode;
        }

        public String getFcmId() {
            return fcmId;
        }

        public void setFcmId(String fcmId) {
            this.fcmId = fcmId;
        }

        public String getVerifikasiMsg() {
            return verifikasiMsg;
        }

        public void setVerifikasiMsg(String verifikasiMsg) {
            this.verifikasiMsg = verifikasiMsg;
        }

        public String getStatusVerifikasi() {
            return statusVerifikasi;
        }

        public void setStatusVerifikasi(String statusVerifikasi) {
            this.statusVerifikasi = statusVerifikasi;
        }

        public String getEmailKey() {
            return emailKey;
        }

        public void setEmailKey(String emailKey) {
            this.emailKey = emailKey;
        }

        public String getMyReferalCode() {
            return myReferalCode;
        }

        public void setMyReferalCode(String myReferalCode) {
            this.myReferalCode = myReferalCode;
        }

    }
}
