package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MajaCourierCost implements Serializable {

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("costs")
    @Expose
    private List<MajaCourierService> costs;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MajaCourierService> getCosts() {
        return costs;
    }

    public void setCosts(List<MajaCourierService> costs) {
        this.costs = costs;
    }
}
