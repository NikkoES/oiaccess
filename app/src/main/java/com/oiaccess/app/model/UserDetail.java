package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by don on 10/6/2017.
 */

public class UserDetail {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public Result result;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("fullname")
        @Expose
        public String fullname;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("no_hp")
        @Expose
        public String noHp;
        @SerializedName("vacc_number")
        @Expose
        public String vaccNumber;
        @SerializedName("status_verifikasi")
        @Expose
        public String statusVerifikasi;
        @SerializedName("verifikasi_msg")
        @Expose
        public String verifikasiMsg;

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNoHp() {
            return noHp;
        }

        public void setNoHp(String noHp) {
            this.noHp = noHp;
        }

        public String getVaccNumber() {
            return vaccNumber;
        }

        public void setVaccNumber(String vaccNumber) {
            this.vaccNumber = vaccNumber;
        }

        public String getStatusVerifikasi() {
            return statusVerifikasi;
        }

        public void setStatusVerifikasi(String statusVerifikasi) {
            this.statusVerifikasi = statusVerifikasi;
        }

        public String getVerifikasiMsg() {
            return verifikasiMsg;
        }

        public void setVerifikasiMsg(String verifikasiMsg) {
            this.verifikasiMsg = verifikasiMsg;
        }
    }
}
