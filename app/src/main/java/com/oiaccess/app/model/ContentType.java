package com.oiaccess.app.model;

public enum ContentType {

    HOME,
    TRANSACTION,
    HELP,
    ACCOUNT

}
