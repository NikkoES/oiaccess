package com.oiaccess.app.model;

/**
 * Created by 666 on 08-May-18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrxHisResult {

    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("trx_type")
    @Expose
    private String trxType;
    @SerializedName("trx_date")
    @Expose
    private String trxDate;
    @SerializedName("trx_desc")
    @Expose
    private String trxDesc;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("dk")
    @Expose
    private String dk;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("vacc_number")
    @Expose
    private String vaccNumber;
    @SerializedName("vacc_from")
    @Expose
    private String vaccFrom;
    @SerializedName("vacc_to")
    @Expose
    private String vaccTo;
    @SerializedName("cor_remark")
    @Expose
    private String corRemark;
    @SerializedName("referral")
    @Expose
    private String referral;
    @SerializedName("trx_id2")
    @Expose
    private String trxId2;
    @SerializedName("is_refund")
    @Expose
    private String isRefund;

    @SerializedName("type")
    @Expose
    public String type;

    public TrxHisResult(String load) {
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxDesc() {
        return trxDesc;
    }

    public void setTrxDesc(String trxDesc) {
        this.trxDesc = trxDesc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getVaccNumber() {
        return vaccNumber;
    }

    public void setVaccNumber(String vaccNumber) {
        this.vaccNumber = vaccNumber;
    }

    public String getVaccFrom() {
        return vaccFrom;
    }

    public void setVaccFrom(String vaccFrom) {
        this.vaccFrom = vaccFrom;
    }

    public String getVaccTo() {
        return vaccTo;
    }

    public void setVaccTo(String vaccTo) {
        this.vaccTo = vaccTo;
    }

    public String getCorRemark() {
        return corRemark;
    }

    public void setCorRemark(String corRemark) {
        this.corRemark = corRemark;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getTrxId2() {
        return trxId2;
    }

    public void setTrxId2(String trxId2) {
        this.trxId2 = trxId2;
    }

    public String getIsRefund() {
        return isRefund;
    }

    public void setIsRefund(String isRefund) {
        this.isRefund = isRefund;
    }
}
