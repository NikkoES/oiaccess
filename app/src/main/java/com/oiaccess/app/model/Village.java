package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Village {

    @SerializedName("result")
    private List<Result> result;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorcode() {
        return error_code;
    }

    public void setErrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrormesagge() {
        return error_message;
    }

    public void setErrormesagge(String error_message) {
        this.error_message = error_message;
    }

    public class Result {

        @SerializedName("village")
        @Expose
        private String villages;

        public String getVillages() {
            return villages;
        }

        public void setVillages(String villages) {
            this.villages = villages;
        }


    }

}
