package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaCartResponse implements Serializable {

    @SerializedName("id_cart")
    @Expose
    private Long id_cart;

    @SerializedName("id_user")
    @Expose
    private Long id_user;

    @SerializedName("id_product")
    @Expose
    private Long id_product;

    @SerializedName("jumlah")
    @Expose
    private int jumlah;

    public Long getId_cart() {
        return id_cart;
    }

    public void setId_cart(Long id_cart) {
        this.id_cart = id_cart;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
