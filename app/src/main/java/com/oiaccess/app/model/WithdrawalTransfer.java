package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by don on 10/12/2017.
 */

public class WithdrawalTransfer {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("result")
    @Expose
    public Result result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result{
        @SerializedName("agent_name")
        @Expose
        public String agentName;
        @SerializedName("amount")
        @Expose
        public String amount;
        @SerializedName("biaya_admin")
        @Expose
        public String biayaAdmin;
        @SerializedName("total_didebet_customer")
        @Expose
        public String totalDidebetCustomer;
        @SerializedName("total_cash_disetor_customer")
        @Expose
        public String totalCashDisetorCustomer;


        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBiayaAdmin() {
            return biayaAdmin;
        }

        public void setBiayaAdmin(String biayaAdmin) {
            this.biayaAdmin = biayaAdmin;
        }

        public String getTotalDidebetCustomer() {
            return totalDidebetCustomer;
        }

        public void setTotalDidebetCustomer(String totalDidebetCustomer) {
            this.totalDidebetCustomer = totalDidebetCustomer;
        }

        public String getTotalCashDisetorCustomer() {
            return totalCashDisetorCustomer;
        }

        public void setTotalCashDisetorCustomer(String totalCashDisetorCustomer) {
            this.totalCashDisetorCustomer = totalCashDisetorCustomer;
        }
    }
}
