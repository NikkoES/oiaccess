package com.oiaccess.app.model;

/**
 * Created by 666 on 29-Mar-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopupServiceResult {

    @SerializedName("response_msg")
    @Expose
    private String responseMsg;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }
}
