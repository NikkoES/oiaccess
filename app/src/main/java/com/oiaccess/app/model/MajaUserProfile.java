package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaUserProfile implements Serializable {

    @SerializedName("user_id")
    @Expose
    private Long user_id;

    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("pob")
    @Expose
    private String pob;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("job")
    @Expose
    private String job;

    @SerializedName("id_card_number")
    @Expose
    private String id_card_number;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("village")
    @Expose
    private String village;

    @SerializedName("districts")
    @Expose
    private String districts;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("postalcode")
    @Expose
    private String postalcode;

    @SerializedName("mom")
    @Expose
    private String mom;

    @SerializedName("gerai_name")
    @Expose
    private String gerai_name;

    @SerializedName("is_seller")
    @Expose
    private String is_seller;

    @SerializedName("is_confirmed_hp")
    @Expose
    private String is_confirmed_hp;

    @SerializedName("is_confirmed_email")
    @Expose
    private String is_confirmed_email;

    @SerializedName("is_active")
    @Expose
    private String is_active;

    @SerializedName("is_verified")
    @Expose
    private String is_verified;

    @SerializedName("referal_code")
    @Expose
    private String referal_code;

    @SerializedName("my_referal_code")
    @Expose
    private String my_referal_code;

    @SerializedName("fcm_id")
    @Expose
    private String fcm_id;

    @SerializedName("fcm_id_ios")
    @Expose
    private String fcm_id_ios;

    @SerializedName("verifikasi_msg")
    @Expose
    private String verifikasi_msg;

    @SerializedName("status_verifikasi")
    @Expose
    private String status_verifikasi;

    @SerializedName("id_card_number_photo")
    @Expose
    private String id_card_number_photo;

    @SerializedName("id_card_number_selfi")
    @Expose
    private String id_card_number_selfi;

    @SerializedName("profile_photo")
    @Expose
    private String profile_photo;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getId_card_number() {
        return id_card_number;
    }

    public void setId_card_number(String id_card_number) {
        this.id_card_number = id_card_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistricts() {
        return districts;
    }

    public void setDistricts(String districts) {
        this.districts = districts;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getMom() {
        return mom;
    }

    public void setMom(String mom) {
        this.mom = mom;
    }

    public String getGerai_name() {
        return gerai_name;
    }

    public void setGerai_name(String gerai_name) {
        this.gerai_name = gerai_name;
    }

    public String getIs_seller() {
        return is_seller;
    }

    public void setIs_seller(String is_seller) {
        this.is_seller = is_seller;
    }

    public String getIs_confirmed_hp() {
        return is_confirmed_hp;
    }

    public void setIs_confirmed_hp(String is_confirmed_hp) {
        this.is_confirmed_hp = is_confirmed_hp;
    }

    public String getIs_confirmed_email() {
        return is_confirmed_email;
    }

    public void setIs_confirmed_email(String is_confirmed_email) {
        this.is_confirmed_email = is_confirmed_email;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getReferal_code() {
        return referal_code;
    }

    public void setReferal_code(String referal_code) {
        this.referal_code = referal_code;
    }

    public String getMy_referal_code() {
        return my_referal_code;
    }

    public void setMy_referal_code(String my_referal_code) {
        this.my_referal_code = my_referal_code;
    }

    public String getFcm_id() {
        return fcm_id;
    }

    public void setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
    }

    public String getFcm_id_ios() {
        return fcm_id_ios;
    }

    public void setFcm_id_ios(String fcm_id_ios) {
        this.fcm_id_ios = fcm_id_ios;
    }

    public String getVerifikasi_msg() {
        return verifikasi_msg;
    }

    public void setVerifikasi_msg(String verifikasi_msg) {
        this.verifikasi_msg = verifikasi_msg;
    }

    public String getStatus_verifikasi() {
        return status_verifikasi;
    }

    public void setStatus_verifikasi(String status_verifikasi) {
        this.status_verifikasi = status_verifikasi;
    }

    public String getId_card_number_photo() {
        return id_card_number_photo;
    }

    public void setId_card_number_photo(String id_card_number_photo) {
        this.id_card_number_photo = id_card_number_photo;
    }

    public String getId_card_number_selfi() {
        return id_card_number_selfi;
    }

    public void setId_card_number_selfi(String id_card_number_selfi) {
        this.id_card_number_selfi = id_card_number_selfi;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }
}
