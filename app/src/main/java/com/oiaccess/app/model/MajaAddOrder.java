package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MajaAddOrder implements Serializable {

    @SerializedName("id_toko")
    @Expose
    private Long id_toko;

    @SerializedName("grand_total")
    @Expose
    private int grand_total;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("courier")
    @Expose
    private MajaCourier courier;

    @SerializedName("payment_method")
    @Expose
    private String payment_method;

    @SerializedName("items")
    @Expose
    private List<MajaItem> items;

    public Long getId_toko() {
        return id_toko;
    }

    public void setId_toko(Long id_toko) {
        this.id_toko = id_toko;
    }

    public int getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(int grand_total) {
        this.grand_total = grand_total;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MajaCourier getCourier() {
        return courier;
    }

    public void setCourier(MajaCourier courier) {
        this.courier = courier;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public List<MajaItem> getItems() {
        return items;
    }

    public void setItems(List<MajaItem> items) {
        this.items = items;
    }
}
