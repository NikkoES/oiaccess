package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OiAnggotaKelompok {

    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    @SerializedName("error_code")
    @Expose
    private String error_code;

    @SerializedName("error_message")
    @Expose
    private String error_message;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorcode() {
        return error_code;
    }

    public void setErrorcode(String error_code) {
        this.error_code = error_code;
    }

    public String getErrormesagge() {
        return error_message;
    }

    public void setErrormesagge(String error_message) {
        this.error_message = error_message;
    }

    public static class Result {
        /*
                public Profile(String load) {
                }
        */
        @SerializedName("fullname")
        @Expose
        private String fullname;

        public String getFullname() {
            return fullname;
        }
        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        @SerializedName("no_anggota")
        @Expose
        private String no_anggota;

        public String getNo_Anggota() {
            return no_anggota;
        }
        public void setNo_Anggota(String no_anggota) {
            this.no_anggota = no_anggota;
        }


    }

}
