package com.oiaccess.app.model;

/**
 * Created by 666 on 24-Apr-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopupHistory {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("result")
    @Expose
    private List<TopupHistoryResult> result = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<TopupHistoryResult> getResult() {
        return result;
    }

    public void setResult(List<TopupHistoryResult> result) {
        this.result = result;
    }
}
