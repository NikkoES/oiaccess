package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaCourier implements Serializable {

    @SerializedName("vendor")
    @Expose
    private String vendor;

    @SerializedName("cost")
    @Expose
    private int cost;

    @SerializedName("origin")
    @Expose
    private String origin;

    @SerializedName("originId")
    @Expose
    private String originId;

    @SerializedName("destination")
    @Expose
    private String destination;

    @SerializedName("destinationId")
    @Expose
    private String destinationId;

    @SerializedName("address")
    @Expose
    private String address;

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
