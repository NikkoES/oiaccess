package com.oiaccess.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MajaUser implements Serializable {

    @SerializedName("user_name")
    @Expose
    private String user_name;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("fcm_id")
    @Expose
    private String fcm_id;

    @SerializedName("is_verified")
    @Expose
    private String is_verified;

    @SerializedName("is_confirmed_hp")
    @Expose
    private String is_confirmed_hp;

    @SerializedName("is_confirmed_email")
    @Expose
    private String is_confirmed_email;

    @SerializedName("is_seller")
    @Expose
    private String is_seller;

    @SerializedName("gerai_name")
    @Expose
    private String gerai_name;

    @SerializedName("my_referal_code")
    @Expose
    private String my_referal_code;

    @SerializedName("status_verifikasi")
    @Expose
    private String status_verifikasi;

    @SerializedName("verifikasi_msg")
    @Expose
    private String verifikasi_msg;

    @SerializedName("access_token")
    @Expose
    private String access_token;

    @SerializedName("refresh_token")
    @Expose
    private String refresh_token;

    public MajaUser(String fullName, String email, String phoneNumber, String gender) {
        this.fullname = fullName;
        this.email = email;
        this.user_id = phoneNumber;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFcm_id() {
        return fcm_id;
    }

    public void setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getIs_confirmed_hp() {
        return is_confirmed_hp;
    }

    public void setIs_confirmed_hp(String is_confirmed_hp) {
        this.is_confirmed_hp = is_confirmed_hp;
    }

    public String getIs_confirmed_email() {
        return is_confirmed_email;
    }

    public void setIs_confirmed_email(String is_confirmed_email) {
        this.is_confirmed_email = is_confirmed_email;
    }

    public String getIs_seller() {
        return is_seller;
    }

    public void setIs_seller(String is_seller) {
        this.is_seller = is_seller;
    }

    public String getGerai_name() {
        return gerai_name;
    }

    public void setGerai_name(String gerai_name) {
        this.gerai_name = gerai_name;
    }

    public String getMy_referal_code() {
        return my_referal_code;
    }

    public void setMy_referal_code(String my_referal_code) {
        this.my_referal_code = my_referal_code;
    }

    public String getStatus_verifikasi() {
        return status_verifikasi;
    }

    public void setStatus_verifikasi(String status_verifikasi) {
        this.status_verifikasi = status_verifikasi;
    }

    public String getVerifikasi_msg() {
        return verifikasi_msg;
    }

    public void setVerifikasi_msg(String verifikasi_msg) {
        this.verifikasi_msg = verifikasi_msg;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }
}
