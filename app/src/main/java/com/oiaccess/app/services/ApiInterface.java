package com.oiaccess.app.services;


import android.graphics.Point;

import com.google.android.gms.common.internal.service.Common;
import com.oiaccess.app.model.ChangePassword;
import com.oiaccess.app.model.CheckVersion;
import com.oiaccess.app.model.City;
import com.oiaccess.app.model.CommonInput;
import com.oiaccess.app.model.CustomerService;
import com.oiaccess.app.model.District;
import com.oiaccess.app.model.GetForSpinner;
import com.oiaccess.app.model.Mutation;
import com.oiaccess.app.model.OiAnggotaKelompok;
import com.oiaccess.app.model.OiKelompok;
import com.oiaccess.app.model.OiNotifikasi;
import com.oiaccess.app.model.OiOrmas;
import com.oiaccess.app.model.PostalCode;
import com.oiaccess.app.model.Province;
import com.oiaccess.app.model.Referal;
import com.oiaccess.app.model.RegisterHP;
import com.oiaccess.app.model.RegisterUserHP;
import com.oiaccess.app.model.TopUp;
import com.oiaccess.app.model.TopUpModel;
import com.oiaccess.app.model.TopupManual;
import com.oiaccess.app.model.Transfer;
import com.oiaccess.app.model.TrxDetilPpob;
import com.oiaccess.app.model.TrxDetilTransfer;
import com.oiaccess.app.model.TrxHis;
import com.oiaccess.app.model.TukarPoin;
import com.oiaccess.app.model.UpdateProfile;
import com.oiaccess.app.model.UserDetail;
import com.oiaccess.app.model.Village;
import com.oiaccess.app.model.Voucher;
import com.oiaccess.app.model.VoucherData;
import com.oiaccess.app.model.WithdrawalTransfer;
import com.oiaccess.app.ui.model.auth.User;
import com.oiaccess.app.ui.model.auth.UserResponse;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInterface {

    //APK Ver
    @GET("oi_pub/apk_ver")
    Call<CheckVersion> getCheckVersion();



    //Saldo Point
    @FormUrlEncoded
    @POST("vacc/inquiry_vacc")
    Call<User> postInquiry(@Header("Authorization") String authorization,
                           @Field("vacc_number") String vacc_number);

    //CHANGE PASSWORD
    @FormUrlEncoded
    @POST("users/change_pass")
    Call<ChangePassword> postChangePass(@Header("Authorization") String authorization,
                                        @Field("user_id") String user_id,
                                        @Field("old_pass") String oldPass,
                                        @Field("new_pass") String newPass,
                                        @Field("retype_new_pass") String retypeNewPass);

    //CHANGE PIN
    @FormUrlEncoded
    @POST("users/change_pin")
    Call<ChangePassword> postChangePin(@Header("Authorization") String authorization,
                                       @Field("user_id") String user_id,
                                       @Field("old_pin") String oldPin,
                                       @Field("new_pin") String newPin,
                                       @Field("retype_new_pin") String retypeNewPin);

    //get profile
    @FormUrlEncoded
    @POST("oi/getuser")
    Call<UpdateProfile> getProfile(@Header("Authorization") String authorization,
                                   @Field("user_id") String userid);



    @FormUrlEncoded
    @POST("oi/nama_kelompok")
    Call<OiKelompok> postOikelompok(@Header("authorization") String authorization, @Field("keyword") String keyword, @Field("offset") int offset);

    @FormUrlEncoded
    @POST("oi/list_anggota")
    Call<OiAnggotaKelompok> postOiAnggotakelompok(@Header("authorization") String authorization, @Field("group_id") String group_id, @Field("offset") int offset);

    //Login
    @FormUrlEncoded
    @POST("oi_pub/login")
    Call<User> postLogin(@Field("no_hp") String phone,
                         @Field("pass") String pass,
                         @Field("gps_tag") String gps_tag);

    //Register HP
    @FormUrlEncoded
    @POST("oi_pub/cek_hp")
    Call<RegisterHP> postRegisterHP(@Field("no_hp") String phone);

    //Cek OTP
    @FormUrlEncoded
    @POST("oi_pub/confirmasi_otp")
    Call<RegisterHP> postCekOtp(@Field("no_hp") String no_hp, @Field("otp") String otp);

    //Register User HP
    @FormUrlEncoded
    @POST("oi_pub/register")
    Call<RegisterUserHP> postRegisterUserHP(@Field("no_hp") String no_hp, @Field("fullname") String fullname, @Field("email") String email, @Field("pass") String pass, @Field("retype_pass") String retype_pass, @Field("referral_code") String referral_code, @Field("fcm_id") String fcm_id, @Field("pin") String pin, @Field("retype_pin") String retype_pin);

    //@GET("pub_data/pekerjaan")
    @GET("pub_data/jobs")
    Call<GetForSpinner> getjobs();

    @GET("pub_data/education")
    Call<Common> geteducations();

    @GET("pub_data/province")
    Call<Province> getprovince();

    @FormUrlEncoded
    @POST("pub_data/city")
    Call<City> postcity(@Field("province") String province);

    @FormUrlEncoded
    @POST("pub_data/districts")
    Call<District> postdistrict(@Field("province") String province, @Field("city") String city);

    @FormUrlEncoded
    @POST("pub_data/village")
    Call<Village> postvillage(@Field("province") String province, @Field("city") String city, @Field("districts") String districts);

    @FormUrlEncoded
    @POST("pub_data/postalcode")
    Call<PostalCode> postpostalcode(@Field("province") String province, @Field("city") String city, @Field("districts") String districts, @Field("village") String village);

    /*
    @Multipart
    @POST("oi/upload_profile_foto")
    Call<UploadFoto> uploadImage(@Field("foto_profile") File berkas, @Header("authorization") String authorization);
    */

    @Multipart
    @POST("oi/upload_profile_foto")
    Call<ResponseBody> uploadFotoProfile(@Part MultipartBody.Part photo, @Header("authorization") String authorization);

    @Multipart
    @POST("oi/upload_foto_ktp")
    Call<ResponseBody> uploadFotoKTP(@Part MultipartBody.Part photo, @Header("authorization") String authorization);

    @Multipart
    @POST("oi/upload_foto_selfi")
    Call<ResponseBody> uploadFotoSelfiKTP(@Part MultipartBody.Part photo, @Header("authorization") String authorization);

    @FormUrlEncoded
    @POST("oi/data_diri")
    Call<CommonInput> postinputdatadiri(@Field("id_card_number") String id_card_number, @Field("fullname") String fullname,
                                        @Field("pob") String pob, @Field("dob") String dob, @Field("mom") String mom,
                                        @Field("gol_darah") String gol_darah, @Field("gender") String gender,
                                        @Field("status_nikah") String status_nikah, @Field("pendidikan") String pendidikan,
                                        @Field("gelar_sarjana") String gelar_sarjana, @Field("job") String job,
                                        @Field("keahlian_profesional1") String keahlian_profesional1,
                                        @Field("keahlian_profesional2") String keahlian_profesional2,
                                        @Field("address") String address, @Field("province") String province,
                                        @Field("city") String city,
                                        @Field("districts") String districts, @Field("village") String village,
                                        @Field("postalcode") String postalcode,
                                        @Field("th_masuk_anggota") String th_masuk_anggota,
                                        @Field("no_anggota") String no_anggota,
                                        @Field("bpw_oi") String bpw_oi,
                                        @Field("bpk_oi") String bpk_oi,
                                        @Field("oi_group_id") String oi_group_id,
                                        @Header("authorization") String authorization);


    //update 25/09/2018

    //topup request
    @FormUrlEncoded
    @POST("bni/topup_request")
    Call<TopUp> postTopupRequest(@Header("Authorization") String authorization,
                                 @Field("trx_amount") String trx_amount);

    //topup
    @FormUrlEncoded
    @POST("bni/topup")
    Call<TopUp> postTopup(@Header("Authorization") String authorization,
                          @Field("user_id") String user_id,
                          @Field("trx_amount") String trx_amount,
                          @Field("pin") String pin);


    //Refresh token
    @FormUrlEncoded
    @POST("users/refreshtoken")
    Call<User> postRefreshToken(@Field("refresh_token") String refreshToken);

    //PPOB HISTORY
    @FormUrlEncoded
    @POST("bni/topup_history")
    Call<TopUpModel> postTopUpHistory(@Header("Authorization") String authorization,
                                      @Field("user_id") String user_id,
                                      @Field("offset") int offset);

    @FormUrlEncoded
    @POST("topup/manual")
    Call<TopupManual> postTopupManual(@Header("Authorization") String authorization,
                                      @Field("bank") String bank,
                                      @Field("topup") String topup);

    //MUTATION HISTORY
    @FormUrlEncoded
    @POST("vacc/cek_mutasi")
    Call<Mutation> postMutationHistory(@Header("Authorization") String authorization,
                                       @Field("trx_type") String trx_type,
                                       @Field("vacc_number") String vacc_number,
                                       @Field("start_date") String startDate,
                                       @Field("end_date") String endDate,
                                       @Field("offset") int offset);

    //REQUEST CASH TRANSFER
    @FormUrlEncoded
    @POST("vacc/req_setor_tunai")
    Call<WithdrawalTransfer> postRequestCashTransfer(@Header("Authorization") String authorization,
                                                     @Field("agent_email_no_hp_vacc") String to_vacc_number,
                                                     @Field("amount") String amount);

    //REQUEST CASH TRANSFER SUBMIT
    @FormUrlEncoded
    @POST("vacc/req_setor_tunai_submit")
    Call<Transfer> postRequestCashTransferSubmit(@Header("Authorization") String authorization,
                                                 @Field("agent_email_no_hp_vacc") String to_vacc_number,
                                                 @Field("amount") String amount,
                                                 @Field("pin") String pin);

    //REQUEST CASH TRANSFER SUBMIT
    @FormUrlEncoded
    @POST("vacc/tarik_tunai_list")
    Call<Mutation> postWithdrawalList(@Header("Authorization") String authorization,
                                      @Field("vacc_number") String vacc_number,
                                      @Field("start_date") String startDate,
                                      @Field("end_date") String endDate,
                                      @Field("offset") int offset);


    //PROCESS CASH TRANSFER & WITHDRAWAL SUBMIT
    @FormUrlEncoded
    @POST("vacc/proses_tarik_setor_tunai")
    Call<Transfer> postProcessTransferWithdrawal(@Header("Authorization") String authorization,
                                                 @Field("trx_id") String trx_id,
                                                 @Field("pin") String pin);
    @FormUrlEncoded
    @POST("vacc/cancel_tarik_setor_tunai")
    Call<Transfer> postCancelTransferWithdrawal(@Header("Authorization") String authorization,
                                                @Field("trx_id") String trx_id,
                                                @Field("pin") String pin);


    //REQUEST CASH WITHDRAWAL
    @FormUrlEncoded
    @POST("vacc/req_tarik_tunai")
    Call<WithdrawalTransfer> postRequestCashWithdrawal(@Header("Authorization") String authorization,
                                                       @Field("agent_email_no_hp_vacc") String to_vacc_number,
                                                       @Field("amount") String amount);


    //REQUEST CASH WITHDRAWAL SUBMIT
    @FormUrlEncoded
    @POST("vacc/req_tarik_tunai_submit")
    Call<Transfer> postRequestCashWithdrawalSubmit(@Header("Authorization") String authorization,
                                                   @Field("agent_email_no_hp_vacc") String to_vacc_number,
                                                   @Field("amount") String amount,
                                                   @Field("pin") String pin);

    //CHECK INQUIRY NAME
    @FormUrlEncoded
    @POST("vacc/inquiry_name")
    Call<UserDetail> postInquiryName(@Header("Authorization") String authorization,
                                     @Field("email_no_hp_vacc_number") String user_id);



    //TRANSFER VACC
    @FormUrlEncoded
    @POST("vacc/transfer_vacc")
    Call<Transfer> postTransferVacc(@Header("Authorization") String authorization,
                                    @Field("to_vacc_number") String to_vacc_number,
                                    @Field("amount") String amount,
                                    @Field("trx_desc") String trx_desc,
                                    @Field("pin") String pin);

    @FormUrlEncoded
    @POST("vacc/trx_his")
    Call<TrxHis> postTrxHis(@Header("Authorization") String authorization,
                            @Field("offset") Integer offset);

    @FormUrlEncoded
    @POST("vacc/trx_detil")
    Call<TrxDetilPpob> postTrxDetilPpob(@Header("Authorization") String authorization,
                                        @Field("trx_id") String trx_id,
                                        @Field("trx_type") String trx_type);

    @FormUrlEncoded
    @POST("vacc/trx_detil")
    Call<TrxDetilTransfer> postTrxDetilTransfer(@Header("Authorization") String authorization,
                                                @Field("trx_id") String trx_id,
                                                @Field("trx_type") String trx_type);

    //REQUEST CASH TRANSFER SUBMIT
    @FormUrlEncoded
    @POST("vacc/setor_tunai_list")
    Call<Mutation> postTransferList(@Header("Authorization") String authorization,
                                    @Field("vacc_number") String vacc_number,
                                    @Field("start_date") String startDate,
                                    @Field("end_date") String endDate,
                                    @Field("offset") int offset);

    //------------------------


    //REFERAL AGENT
    @FormUrlEncoded
    @POST("users/referal_agent")
    Call<Referal> postReferalAgent(@Header("Authorization") String authorization,
                                   @Field("my_referal_code") String my_referal_code);


    //Customer Service
    @GET("oi_pub/customer_service")
    Call<CustomerService> getCustomerService();

    @FormUrlEncoded
    @POST("users/update_fcmid")
    Call<UserResponse> postUpdateFCMID(@Header("Authorization") String authorization,
                                       @Field("fcm_id") String trx_id);

    //HISTORY CLAIM REWARD POIN
    @GET("poin/history_claim_reward_poin")
    Call<Voucher> getHistoryClaimRewardPoin(@Header("Authorization") String authorization);

    //CHECK CLAIM POINT
    @FormUrlEncoded
    @POST("voucher/buy")
    Call<TukarPoin> postTukarPoin(@Header("Authorization") String authorization,
                                  @Field("voucher_id") String vacc_number,
                                  @Field("pin") String pin);

    //point
    @GET("pub_data/poin_reward_list")
    Call<Point> pointList();

    //point voucher
    @FormUrlEncoded
    @POST("voucher/list")
    Call<VoucherData> postVoucherList(@Header("Authorization") String authorization,
                                      @Field("offset") String offset,
                                      @Field("limit") String limit);


    @FormUrlEncoded
    @POST("oi/ormas_oi")
    Call<OiOrmas> postOiormas(@Header("authorization") String authorization, @Field("offset") int offset);

    @FormUrlEncoded
    @POST("common/notifikasi")
    Call<OiNotifikasi> postOinotifikasi(@Header("authorization") String authorization, @Field("offset") int offset);

}
