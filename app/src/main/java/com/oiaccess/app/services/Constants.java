package com.oiaccess.app.services;


public class Constants {

    public static final String APP_NAME = "Oi ACCESS";
    public static final String APK_VER = "1"; //changedate 07 OCT 2018 18:08
    public static final String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=com.oiaccess.app";
    public static final String REFERRAL_TEXT = "mengundangmu untuk menjadi bergabung  di OiAccess";

    public static final String GROUP_ID = "2";

    public static final String BASE_URL = "http://128.199.204.39:9000/restapi/";
    public static final String IMAGE_URL = "http://128.199.204.39:9000/";

    public static final String API_AUTH = "auth";

    public static final String API_PAGING = "page=";
    public static final String API_LIMIT = "&limit=10";

    public static final String CATEGORY_USER_MANAGE = "users/";
    public static final String API_USER_PROFILE = "me/";
    public static final String API_USER_PROFILE_WALLET = "wallet/";
    public static final String API_USER_STORE_REGISTER = "store/update";

    public static final String CATEGORY_PRODUCTS = BASE_URL + "products";
    public static final String API_PRODUCT_BEST_SELLER = CATEGORY_PRODUCTS + "/best-seller?page=1&limit=4";
    public static final String API_PRODUCT_BY_NAME = CATEGORY_PRODUCTS + "?keyword=";
    public static final String API_PRODUCT_BY_CATEGORY = CATEGORY_PRODUCTS + "/category/";
    public static final String API_PRODUCT_BY_STORE = CATEGORY_PRODUCTS + "/store/";
    public static final String API_PRODUCT_UPLOAD_IMAGE = CATEGORY_PRODUCTS + "/upload";

    public static final String CATEGORY_ORDER = "orders";
    public static final String API_ORDER_LIST = "?status=";

    public static final String CATEGORY_COURIER = "couriers/";
    public static final String API_COURIER_LIST = "list";
    public static final String API_COURIER_PROVINCE = "province";
    public static final String API_COURIER_CITY = "city?province=";
    public static final String API_COURIER_DISTRICT = "subdistrict?city=";
    public static final String API_COURIER_COST = "cost";

    public static final String BASE_VERSION = "v1/";

    public static final String IMG_FOLDER = "/oiaccess";

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static int currentPage = 0;
    public static int NUM_PAGES = 0;

}
