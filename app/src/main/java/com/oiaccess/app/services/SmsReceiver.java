package com.oiaccess.app.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by 666 on 1/3/2017.
 */

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        Object[] objectPDUS = (Object[]) data.get("pdus");

        try{
            for (int i = 0; i < objectPDUS.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) objectPDUS[i]);

                String sender = smsMessage.getDisplayOriginatingAddress();
                //You must check here if the sender is your provider and not another one with same text.

                String messageBody = smsMessage.getMessageBody();

                //Pass on the text to our listener.
                mListener.messageReceived(messageBody);
            }
        }catch (Exception e){
            String err = (e.getMessage()==null)?"SD Card failed":e.getMessage();
            Log.e("sdcard-err2:",err);
        }

    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}
