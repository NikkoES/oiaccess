package com.oiaccess.app.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.oiaccess.app.R;
import com.oiaccess.app.pref.PrefUtil;
import com.oiaccess.app.ui.activity.MainActivity;
import com.oiaccess.app.ui.model.auth.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    ApiClient apiClient;
    Context context;
    SharedPreferences sharedPreferences;

    int numMessages=0;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);
        sendRegistrationToServer(s);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "MESSAGE - ID: " + remoteMessage.getMessageId());
        Log.d(TAG, "MESSAGE - TYPE: " + remoteMessage.getMessageType());


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Log.d(TAG, "Message data payload: " + remoteMessage.getData().get("text"));
            sendNotification(remoteMessage.getData().get("text"));

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.v(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.v(TAG, "Click action Body: " + remoteMessage.getNotification().getClickAction());
            //Toast.makeText(getApplicationContext(), remoteMessage.getNotification().getBody(), Toast.LENGTH_LONG).show();
            sendNotification(remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getData().get(0));



        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent;
        PendingIntent pendingIntent;

        intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(500);
        String GROUP_KEY_WORK_EMAIL = "com.oiaccess.app.WORK_EMAIL";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("OiAccess")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                //.setGroup(GROUP_KEY_WORK_EMAIL)
                .setNumber(++numMessages)
                .setContentIntent(pendingIntent);
        //.setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
        //numMessages = 0;

// Start of a loop that processes data and then notifies the user

        //notificationBuilder.setContentText(messageBody).setNumber(++numMessages);
        //notificationBuilder.setContentText("tes2").setNumber(++numMessages);
        //notificationBuilder.setContentText("tes3").setNumber(++numMessages);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendRegistrationToServer(final String token) {
        // TODO: Implement this method to send token to your app server.

        if (getToken() != null && token != null) {
            apiClient = new ApiClient();
            Call<UserResponse> call = apiClient.getApiInterface().postUpdateFCMID("Bearer " + getToken(), token);
            call.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    if (response.isSuccessful()) {
                        UserResponse user = response.body();
                        if (user.getStatus().equals("1")) {
                            Log.d("FCM", "FIREBASE - TOKEN :"+token);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    Log.d("TAG", t.getMessage());
                }
            });
        }
    }

    public String getToken() {
        sharedPreferences = this.getSharedPreferences(PrefUtil.PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefUtil.PREF_USER_SIGNATURE, "");
    }


}
