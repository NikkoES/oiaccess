package com.oiaccess.app.services;

import com.oiaccess.app.model.User;
import com.oiaccess.app.ui.model.auth.UserResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static com.oiaccess.app.services.Constants.API_AUTH;

public interface ApiInterfaceOi {


    //Login
    @FormUrlEncoded
    @POST(API_AUTH)
    Call<UserResponse> postLogin(@Field("phone") String phone,
                                 @Field("password") String pass,
                                 @Field("gps_tag") String gps_tag);


}
