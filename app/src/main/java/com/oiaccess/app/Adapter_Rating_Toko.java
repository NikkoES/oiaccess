package com.oiaccess.app;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

public class Adapter_Rating_Toko extends RecyclerView.Adapter<Adapter_Rating_Toko.MyViewHolder> {

    Context mContext;
    List<Isi_Rating_Toko> mdata;

    public Adapter_Rating_Toko(Context mContext, List<Isi_Rating_Toko>mdata){
        this.mContext = mContext;
        this.mdata = mdata;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType){

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v =inflater.inflate(R.layout.item_rating_toko, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder( MyViewHolder myViewHolder,int position){

        myViewHolder.profilimg.setImageResource(mdata.get(position).getBackground());
        myViewHolder.namauser.setText(mdata.get(position).getProfilName());
        myViewHolder.isides.setText(mdata.get(position).getIsideskripsi());

    }

    @Override
    public int getItemCount(){
        return mdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        CircularImageView profilimg;
        TextView namauser, isides;

        public MyViewHolder( View itemView){
            super(itemView);
            profilimg = itemView.findViewById(R.id.profiluser);
            namauser = itemView.findViewById(R.id.nameuser);
            isides = itemView.findViewById(R.id.isideskripsi);

        }
    }
}
