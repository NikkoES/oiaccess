package com.oiaccess.app.ui.activity.seller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.order.DetailOrderActivity;
import com.oiaccess.app.ui.activity.order.KomplainListActivity;
import com.oiaccess.app.ui.adapter.recycler_view.KomplainListAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.list_order.ListOrder;
import com.oiaccess.app.ui.model.list_order.ListOrderResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;

public class ListKomplainActivity extends AppCompatActivity {

    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    KomplainListAdapter adapter;

    String status;

    List<ListOrder> listOrder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_komplain);
        ButterKnife.bind(this);

        status = getIntent().getStringExtra("status");
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        txtEmpty.setVisibility(View.VISIBLE);
        setHeaderToolbar();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new KomplainListAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new KomplainListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent i = new Intent(ListKomplainActivity.this, ChatKomplainActivity.class);
                i.putExtra("status", status);
                i.putExtra("id_order", "" + adapter.getItem(position).getIdOrder());
                startActivity(i);
            }
        });


        Log.e("TOKEN", "" + new Session(this).getUser().getAccessToken());

        loadItems();
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder<>(CATEGORY_ORDER);
        getRequestBuilder.addQueryParameter("status", getIntent().getStringExtra("status"));
        getRequestBuilder.addQueryParameter("seller", "true");
        getRequestBuilder
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ListOrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ListOrderResponse) {
                            DialogUtils.closeDialog();
                            ListOrderResponse response1 = (ListOrderResponse) response;
                            if (response1.getTotal() != 0) {
                                listOrder = response1.getResult();
                                txtEmpty.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                adapter.swap(listOrder);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(ListKomplainActivity.this, "Tidak ada data!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void setHeaderToolbar() {
        headerTitleTV.setText("Daftar Komplain");
        headerTitleTV.setTextSize(16);
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
