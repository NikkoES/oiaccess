package com.oiaccess.app.ui.activity.seller;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.oiaccess.app.R;
import com.oiaccess.app.Update_Stok;
import com.oiaccess.app.ui.BaseActivity;
import com.oiaccess.app.ui.activity.product.SearchProductActivity;
import com.oiaccess.app.ui.adapter.recycler_view.ImageAdapter;
import com.oiaccess.app.ui.adapter.recycler_view.KategoriAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.category.Kategori;
import com.oiaccess.app.ui.model.category.KategoriResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.model.create_product.CreateProductResponse;
import com.oiaccess.app.ui.model.post.PostProduct;
import com.oiaccess.app.ui.model.upload_product.UploadProductResponse;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.widget.LinearLayout.HORIZONTAL;
import static com.oiaccess.app.ui.data.Constants.*;

public class AddProductActivity extends BaseActivity {

    @BindView(R.id.productNameET)
    EditText productNameET;
    @BindView(R.id.categoryET)
    EditText categoryET;
    @BindView(R.id.kondisiET)
    EditText kondisiET;
    @BindView(R.id.priceET)
    EditText priceET;
    @BindView(R.id.descriptionET)
    EditText descriptionET;
    @BindView(R.id.stockET)
    EditText stockET;
    @BindView(R.id.weightET)
    EditText weightET;
    @BindView(R.id.minOrderET)
    EditText minOrderET;
    @BindView(R.id.valdo_recImage)
    RecyclerView valdoRecImage;
    @BindView(R.id.priceDiscountET)
    EditText priceDiscountET;
    @BindView(R.id.uploadET)
    EditText uploadET;

    ImageAdapter adapter;
    List<Kategori> listKategori = new ArrayList<>();
    Integer idKategori;
    Map<String, Image> images = new HashMap<String, Image>();

    Produk item;

    List<String> listImage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);

        StringUtils.setCurrency(priceET);

        loadCategory();
        initView();
        initKategori();
    }

    private void loadCategory() {
        AndroidNetworking.get(API_PRODUCT_BY_CATEGORY)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(KategoriResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof KategoriResponse) {
                            KategoriResponse response1 = (KategoriResponse) response;
                            listKategori.clear();
                            listKategori = response1.getResult();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(AddProductActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initView() {
        if (getIntent().getBooleanExtra("edit", false)) {
            item = (Produk) getIntent().getSerializableExtra("item");
            valdoRecImage.setVisibility(View.GONE);
            uploadET.setVisibility(View.GONE);

            productNameET.setText(item.getNamaProduct());
            categoryET.setText(item.getCategoryName());
            idKategori = Integer.parseInt(String.valueOf(item.getIdCategory()));
            kondisiET.setText(item.getKondisi());
            priceET.setText(String.valueOf(item.getHarga()));
            priceDiscountET.setText(String.valueOf(item.getHargaDiskon()));
            descriptionET.setText(item.getDescription());
            stockET.setText(String.valueOf(item.getStock()));
            minOrderET.setText(String.valueOf(item.getmMinOrder()));
            weightET.setText(String.valueOf(item.getWeight()));

        } else {
            adapter = new ImageAdapter(this);
            valdoRecImage.setLayoutManager(new LinearLayoutManager(this, HORIZONTAL, false));
            valdoRecImage.setAdapter(adapter);
            adapter.setOnItemClickListener(new ImageAdapter.OnItemClickListener() {
                @Override
                public void onClick(String item) {

                }

                @Override
                public void onDelete(String item) {
                    images.remove(item);
                    adapter.remove(item);
                }
            });
        }
    }

    @OnClick({R.id.backBtn, R.id.categoryET, R.id.kondisiET, R.id.uploadET, R.id.registerProductBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.categoryET:
                final String items[] = new String[listKategori.size()];
                for (int i = 0; i < listKategori.size(); i++) {
                    items[i] = listKategori.get(i).getCategoryName();
                }
                DialogUtils.dialogArray(this, items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        categoryET.setText(items[which]);
                        idKategori = listKategori.get(which).getIdCategory();
                    }
                });
                break;
            case R.id.kondisiET:
                final String kondisi[] = new String[]{"BARU", "BEKAS"};
                DialogUtils.dialogArray(this, kondisi, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        kondisiET.setText(kondisi[which]);
                    }
                });
                break;
            case R.id.uploadET:
                ImagePicker.create(this)
                        .returnMode(ReturnMode.ALL)
                        .single()
                        .start();
                break;
            case R.id.registerProductBtn:
                checkValidation();
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            String path = image.getPath();

            images.put(path, image);

            Log.e("PATH", "" + path);
            adapter.add(path);
            valdoRecImage.setAdapter(adapter);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkValidation() {
        ArrayList<View> list = new ArrayList<>();
        list.add(productNameET);
        list.add(descriptionET);
        list.add(categoryET);
        list.add(priceET);
        list.add(stockET);
        list.add(weightET);
        list.add(minOrderET);
        if (CommonUtil.validateEmptyEntries(list)) {
            if (getIntent().getBooleanExtra("edit", false)) {
                updateProduct();
            } else {
                if (adapter.getListItem().size() > 0) {
                    uploadFoto();
                } else {
                    Toast.makeText(this, "Gambar Produk Belum Dimasukkan !", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void updateProduct() {
        Log.e("id_product", "" + item.getIdProduct());
        Log.e("id_toko", "" + item.getIdToko());
        Log.e("id_category", "" + idKategori.toString());
        Log.e("nama_product", "" + productNameET.getText().toString());
        Log.e("harga", "" + priceET.getText().toString().replace(".", ""));
        Log.e("harga_diskon", "" + priceDiscountET.getText().toString());
        Log.e("description", "" + descriptionET.getText().toString());
        Log.e("weight", "" + weightET.getText().toString());
        Log.e("stock", "" + stockET.getText().toString());
        Log.e("kondisi", "" + kondisiET.getText().toString());
        Log.e("min_order", "" + minOrderET.getText().toString());

        JSONObject pj = new JSONObject();
        try {
            pj.put("id_toko", item.getIdToko());
            pj.put("id_category", idKategori.toString());
            pj.put("nama_product", productNameET.getText().toString());
            pj.put("harga", priceET.getText().toString().replace(".", ""));
            pj.put("harga_diskon", priceDiscountET.getText().toString());
            pj.put("description", descriptionET.getText().toString());
            pj.put("weight", weightET.getText().toString());
            pj.put("stock", stockET.getText().toString());
            pj.put("kondisi", kondisiET.getText().toString());
            pj.put("min_order", minOrderET.getText().toString());
        } catch (Exception e) {
            Log.e("ERROR BRAY", e.getMessage());
        }

        AndroidNetworking.put(CATEGORY_PRODUCTS + "/" + item.getIdProduct())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addJSONObjectBody(pj)
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(AddProductActivity.this, "Berhasil Mengubah Barang..", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(AddProductActivity.this, "Gagal Mengubah Barang !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(AddProductActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    private void uploadFoto() {
        DialogUtils.openDialog(this);
        listImage.clear();
        ANRequest.MultiPartBuilder postRequestBuilder = new ANRequest.MultiPartBuilder<>(API_PRODUCT_UPLOAD_IMAGE);
        for (int i = 0; i < adapter.getItemCount(); i++) {
            postRequestBuilder.addMultipartFile("image", new File(adapter.getItem(i)));
            postRequestBuilder
                    .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                    .build()
                    .getAsObject(UploadProductResponse.class, new ParsedRequestListener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response instanceof UploadProductResponse) {
                                UploadProductResponse response1 = ((UploadProductResponse) response);
                                if (response1.getStatus().equalsIgnoreCase("1")) {
                                    listImage.add(response1.getResult().getUrl());
                                } else {
                                    Toast.makeText(AddProductActivity.this, "Gagal Upload !", Toast.LENGTH_SHORT).show();
                                    DialogUtils.closeDialog();
                                }
                                Log.e("LIST IMAGE SIZE", "" + listImage.size());
                                Log.e("ADAPTER IMAGE SIZE", "" + adapter.getListItem().size());
                                if (listImage.size() == adapter.getListItem().size()) {
                                    addProduct();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            DialogUtils.closeDialog();
                            Log.e("", "onError: " + anError.getErrorDetail());
                            Log.e("", "onError: " + anError.getErrorBody());
                            Log.e("", "onError: " + anError.getErrorCode());
                        }
                    });
        }
    }

    private void initKategori() {
        AndroidNetworking.get(API_PRODUCT_BY_CATEGORY)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(KategoriResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        KategoriResponse response1 = (KategoriResponse) response;
                        listKategori.clear();
                        listKategori = response1.getResult();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addProduct() {
        Log.e("LIST IMAGE ARRAY", "" + Arrays.toString(listImage.toArray()).toString());
        JSONObject pj = new JSONObject();
        JSONArray jsArray = new JSONArray(listImage);
        try {
            pj.put("id_toko", new Session(this).getUser().getUserId());
            pj.put("id_category", idKategori.toString());
            pj.put("condition", kondisiET.getText().toString());
            pj.put("min_order", minOrderET.getText().toString());
            pj.put("nama_product", productNameET.getText().toString());
            pj.put("harga", priceET.getText().toString().replace(".", ""));
            pj.put("harga_diskon", priceDiscountET.getText().toString());
            pj.put("description", descriptionET.getText().toString());
            pj.put("stock", stockET.getText().toString());
            pj.put("weight", weightET.getText().toString());
            pj.put("images", jsArray);
        } catch (Exception e) {
            Log.e("ERROR BRAY", e.getMessage());
        }


        AndroidNetworking.post(CATEGORY_PRODUCTS)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addJSONObjectBody(pj)
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(AddProductActivity.this, "Tambah barang berhasil, silahkan tunggu verifikasi Admin", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(AddProductActivity.this, "Gagal Menambahkan Barang !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(AddProductActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }
}