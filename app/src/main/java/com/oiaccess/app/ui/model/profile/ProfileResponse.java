
package com.oiaccess.app.ui.model.profile;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProfileResponse {

    @SerializedName("result")
    private Profile mProfile;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public Profile getResult() {
        return mProfile;
    }

    public void setResult(Profile profile) {
        mProfile = profile;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
