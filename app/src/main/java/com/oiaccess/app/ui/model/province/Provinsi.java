
package com.oiaccess.app.ui.model.province;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Provinsi {

    @SerializedName("province")
    private String mProvince;
    @SerializedName("province_id")
    private String mProvinceId;

    public String getProvince() {
        return mProvince;
    }

    public void setProvince(String province) {
        mProvince = province;
    }

    public String getProvinceId() {
        return mProvinceId;
    }

    public void setProvinceId(String provinceId) {
        mProvinceId = provinceId;
    }

}
