
package com.oiaccess.app.ui.model.balance;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Balance {

    @SerializedName("balance")
    private Long mBalance;
    @SerializedName("poin")
    private Long mPoin;
    @SerializedName("user_id")
    private Long mUserId;
    @SerializedName("vacc_number")
    private String mVaccNumber;

    public Long getBalance() {
        return mBalance;
    }

    public void setBalance(Long balance) {
        mBalance = balance;
    }

    public Long getPoin() {
        return mPoin;
    }

    public void setPoin(Long poin) {
        mPoin = poin;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getVaccNumber() {
        return mVaccNumber;
    }

    public void setVaccNumber(String vaccNumber) {
        mVaccNumber = vaccNumber;
    }

}
