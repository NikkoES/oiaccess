package com.oiaccess.app.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oiaccess.app.R;
import com.oiaccess.app.ui.BaseActivity;
import com.oiaccess.app.ui.adapter.bottomnav.NoSwipePager;
import com.oiaccess.app.ui.adapter.viewpager.VpAdapter;
import com.oiaccess.app.ui.fragment.menu.AccountFragment;
import com.oiaccess.app.ui.fragment.menu.HelpFragment;
import com.oiaccess.app.ui.fragment.menu.HomeFragment;
import com.oiaccess.app.ui.fragment.menu.TransactionFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.view_pager_menu)
    NoSwipePager viewPagerMenu;
    @BindView(R.id.tab_layout_menu)
    TabLayout tabLayoutMenu;

    private VpAdapter vpAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initView();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        viewPagerMenu.setAdapter(vpAdapter);
//    }

    public void initView() {
        vpAdapter = new VpAdapter(getSupportFragmentManager());

        vpAdapter.addFragment(new HomeFragment(), "");
        vpAdapter.addFragment(new TransactionFragment(), "");
        vpAdapter.addFragment(new HelpFragment(), "");
        vpAdapter.addFragment(new AccountFragment(), "");

        viewPagerMenu.setAdapter(vpAdapter);
        viewPagerMenu.setOffscreenPageLimit(4);

        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setIcon(R.drawable.ic_house).setText("Beranda"));
        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setIcon(R.drawable.ic_transfer).setText("Transaksi"));
        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setIcon(R.drawable.ic_smartphone).setText("Bantuan"));
        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setIcon(R.drawable.ic_user).setText("Akun"));

        tabLayoutMenu.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerMenu.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        tabLayoutMenu.setupWithViewPager(viewPagerMenu);
        inflateDivider();
    }

    void inflateDivider() {
        for (int i = 0; i < tabLayoutMenu.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayoutMenu.getTabAt(i);
            RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.tab_layout, tabLayoutMenu, false);

            TextView tabTextView = (TextView) relativeLayout.findViewById(R.id.tab_title);
            ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.img_tab);
            tabTextView.setText(tab.getText());
            imageView.setImageDrawable(tab.getIcon());
            tab.setCustomView(relativeLayout);
//            tab.select();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Peringatan")
                .setMessage("Anda yakin akan keluar aplikasi?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.super.onBackPressed();
                        moveTaskToBack(true);
                        Log.d("LoginActivity", "Test exit 0");
                        finish();
                        System.exit(0);

                    }
                }).create().show();
    }

}
