package com.oiaccess.app.ui.activity.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.ProdukAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BEST_SELLER;
import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_CATEGORY;
import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_NAME;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_PRODUCTS;


public class SearchProductActivity extends AppCompatActivity {

    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.lyt_no_result)
    TextView lytNoResult;

    ProdukAdapter adapter;
    List<Produk> listProduk = new ArrayList<>();

    Integer idCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new ProdukAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new ProdukAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intentDetailProduct = new Intent(SearchProductActivity.this, ProductDetailActivity.class);
                intentDetailProduct.putExtra("item", adapter.getItem(position));
                startActivity(intentDetailProduct);
            }
        });

        if (getIntent().hasExtra("id_category")) {
            idCategory = getIntent().getIntExtra("id_category", 0);
            loadItems(API_PRODUCT_BY_CATEGORY + idCategory);
        } else {
//            loadItems(CATEGORY_PRODUCTS);
        }

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().equals("")) {
                    listProduk.clear();
                    adapter.swap(listProduk);
                    recyclerView.setVisibility(View.GONE);
                    lytNoResult.setVisibility(View.VISIBLE);
                }else {
                    loadItems(API_PRODUCT_BY_NAME + s.toString());
                }
            }
        });
    }

    private void loadItems(String url) {
        DialogUtils.openDialog(this);
        AndroidNetworking.get(url)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProductResponse) {
                            ProductResponse response1 = (ProductResponse) response;
                            listProduk.clear();
                            Long idUser = Long.parseLong(new Session(getApplicationContext()).getUser().getUserId());
                            for (Produk p : response1.getResult()) {
                                if (!p.getIdToko().equals(idUser)) {
                                    listProduk.add(p);
                                }
                            }

                            if (listProduk.size() > 0) {
                                recyclerView.setVisibility(View.VISIBLE);
                                lytNoResult.setVisibility(View.GONE);
                                adapter.swap(listProduk);
                            } else {
                                recyclerView.setVisibility(View.GONE);
                                lytNoResult.setVisibility(View.VISIBLE);
                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(SearchProductActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick({R.id.bt_backSearch, R.id.bt_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_backSearch:
                finish();
                break;
            case R.id.bt_clear:
                etSearch.setText("");
                break;
        }
    }
}
