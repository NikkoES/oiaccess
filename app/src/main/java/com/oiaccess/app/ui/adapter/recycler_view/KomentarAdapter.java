package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.oiaccess.app.R;

import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class KomentarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //List<Kategori> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public KomentarAdapter(Context ctx) {
        this.ctx = ctx;
        //listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komentar_product, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
//            Kategori item = listItem.get(position);
//
//            view.kategoriGambar.setImageDrawable(ctx.getResources().getDrawable(R.drawable.electronic));
//            view.kategoriNama.setText(item.getCategoryName());

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

//    public void add(Kategori item) {
//        listItem.add(item);
//        notifyItemInserted(listItem.size() + 1);
//    }
//
//    public void addAll(List<Kategori> listItem) {
//        for (Kategori item : listItem) {
//            add(item);
//        }
//    }
//
//    public void removeAll() {
//        listItem.clear();
//        notifyDataSetChanged();
//    }
//
//    public void swap(List<Kategori> datas) {
//        if (datas == null || datas.size() == 0) listItem.clear();
//        if (listItem != null && listItem.size() > 0)
//            listItem.clear();
//        listItem.addAll(datas);
//        notifyDataSetChanged();
//
//    }
//
//    public Kategori getItem(int pos) {
//        return listItem.get(pos);
//    }
//
//    public String showHourMinute(String hourMinute) {
//        String time = "";
//        time = hourMinute.substring(0, 5);
//        return time;
//    }
//
//    public void setFilter(List<Kategori> list) {
//        listItem = new ArrayList<>();
//        listItem.addAll(list);
//        notifyDataSetChanged();
//    }
//
//    public List<Kategori> getListItem() {
//        return listItem;
//    }
}
