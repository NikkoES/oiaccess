
package com.oiaccess.app.ui.model.detail_order;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Detail implements Serializable {

    @SerializedName("category_name")
    private String mCategoryName;
    @SerializedName("harga")
    private Long mHarga;
    @SerializedName("harga_diskon")
    private Long mHargaDiskon;
    @SerializedName("id_category")
    private Long mIdCategory;
    @SerializedName("id_order")
    private Long mIdOrder;
    @SerializedName("id_order_detail")
    private Long mIdOrderDetail;
    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("jumlah")
    private Long mJumlah;
    @SerializedName("nama_product")
    private String mNamaProduct;
    @SerializedName("thumbnail")
    private String mThumbnail;
    @SerializedName("total")
    private Long mTotal;
    @SerializedName("weight")
    private Long mWeight;
    @SerializedName("is_rating")
    private Long mIsRating;

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    public Long getHarga() {
        return mHarga;
    }

    public void setHarga(Long harga) {
        mHarga = harga;
    }

    public Long getHargaDiskon() {
        return mHargaDiskon;
    }

    public void setHargaDiskon(Long hargaDiskon) {
        mHargaDiskon = hargaDiskon;
    }

    public Long getIdCategory() {
        return mIdCategory;
    }

    public void setIdCategory(Long idCategory) {
        mIdCategory = idCategory;
    }

    public Long getIdOrder() {
        return mIdOrder;
    }

    public void setIdOrder(Long idOrder) {
        mIdOrder = idOrder;
    }

    public Long getIdOrderDetail() {
        return mIdOrderDetail;
    }

    public void setIdOrderDetail(Long idOrderDetail) {
        mIdOrderDetail = idOrderDetail;
    }

    public Long getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(Long idProduct) {
        mIdProduct = idProduct;
    }

    public Long getJumlah() {
        return mJumlah;
    }

    public void setJumlah(Long jumlah) {
        mJumlah = jumlah;
    }

    public String getNamaProduct() {
        return mNamaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        mNamaProduct = namaProduct;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

    public Long getWeight() {
        return mWeight;
    }

    public void setWeight(Long weight) {
        mWeight = weight;
    }

    public Long getmIsRating() {
        return mIsRating;
    }

    public void setmIsRating(Long mIsRating) {
        this.mIsRating = mIsRating;
    }
}
