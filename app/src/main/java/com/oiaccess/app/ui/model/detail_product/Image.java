
package com.oiaccess.app.ui.model.detail_product;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Image implements Serializable {

    @SerializedName("id_image")
    private Long mIdImage;
    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("image_name")
    private String mImageName;

    public Long getIdImage() {
        return mIdImage;
    }

    public void setIdImage(Long idImage) {
        mIdImage = idImage;
    }

    public Long getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(Long idProduct) {
        mIdProduct = idProduct;
    }

    public String getImageName() {
        return mImageName;
    }

    public void setImageName(String imageName) {
        mImageName = imageName;
    }

}
