package com.oiaccess.app.ui.model.entitiy_cart;

import com.google.gson.annotations.SerializedName;
import com.oiaccess.app.ui.model.post_checkout.Courier;
import com.oiaccess.app.ui.model.post_checkout.Item;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.annotation.Backlink;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToMany;

@Entity
public class EntityOrder {

    @Id
    public Long id;
    public Long mIdToko;
    public Long mIdCart;
    public Long mGrandTotal;
    public String mAddress;
    public String mNamaToko;
    public Long mCost;
    public Long mTotalWeight;
    public String mDestination;
    public String mOrigin;
    public String mVendor;
    public String mPaymentMethod;
    //    public ToMany<EntityItem> listItem;
    @Backlink()
    public ToMany<EntityItem> listItem;

}
