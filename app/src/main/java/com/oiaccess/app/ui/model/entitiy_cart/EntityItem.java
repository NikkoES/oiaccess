package com.oiaccess.app.ui.model.entitiy_cart;

import com.google.gson.annotations.SerializedName;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.relation.ToOne;

@Entity
public class EntityItem {

    @Id(assignable = true)
    public long id;

    public Long mIdProduct;
    public Long mJumlah;
    public Long mTotal;
    public Long mPrice;
    public Long mWeight;
    public String mImageProduct;
    public String mNamaProduct;
    public Long mStock;
    public Long mMinOrder;

    public ToOne<EntityOrder> orderToOne;

}
