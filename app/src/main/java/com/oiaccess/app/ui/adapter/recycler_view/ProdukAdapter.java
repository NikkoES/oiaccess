package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class ProdukAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    List<Produk> listItem;
    List<Produk> listItemFiltered;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ProdukAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
        listItemFiltered = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productImageIV)
        ImageView productImageIV;
        @BindView(R.id.productNameTV)
        TextView productNameTV;
        @BindView(R.id.priceTV)
        TextView priceTV;
        @BindView(R.id.discountPriceTV)
        TextView discountPriceTV;
        @BindView(R.id.discountTV)
        TextView discountTV;
        @BindView(R.id.cardImageHome1)
        CardView cardImageHome1;


        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            Produk item = listItemFiltered.get(position);

            Glide.with(ctx).load(item.getImageName()).into(view.productImageIV);
            view.productNameTV.setText(item.getNamaProduct());
            if (item.getHargaDiskon() > 0) {
                Long diskonPrice = (item.getHarga() * item.getHargaDiskon()) / 100;
                view.discountPriceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.getHarga())));
                view.priceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.getHarga() - diskonPrice)));
                view.discountTV.setText(item.getHargaDiskon().toString() + "%");
                view.discountPriceTV.setPaintFlags(view.priceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                view.priceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.getHarga())));
                view.discountTV.setVisibility(View.GONE);
                view.discountPriceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.getHarga())));
            }

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItemFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listItemFiltered = listItem;
                } else {
                    List<Produk> filteredList = new ArrayList<>();
                    for (Produk row : listItem) {

                        if (row.getNamaProduct().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    listItemFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listItemFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listItemFiltered = (ArrayList<Produk>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void add(Produk item) {
        listItemFiltered.add(item);
        notifyItemInserted(listItemFiltered.size() + 1);
    }

    public void addAll(List<Produk> listItemFiltered) {
        for (Produk item : listItemFiltered) {
            add(item);
        }
    }

    public void removeAll() {
        listItemFiltered.clear();
        notifyDataSetChanged();
    }

    public void swap(List<Produk> datas) {
        if (datas == null || datas.size() == 0) listItemFiltered.clear();
        if (listItemFiltered != null && listItemFiltered.size() > 0)
            listItemFiltered.clear();
        listItemFiltered.addAll(datas);
        listItem = datas;
        notifyDataSetChanged();

    }

    public Produk getItem(int pos) {
        return listItemFiltered.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<Produk> list) {
        listItemFiltered = new ArrayList<>();
        listItemFiltered.addAll(list);
        notifyDataSetChanged();
    }

    public List<Produk> getListItem() {
        return listItemFiltered;
    }
}
