package com.oiaccess.app.ui.activity.buyer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.seller.AddProductActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.create_product.CreateProductResponse;
import com.oiaccess.app.ui.model.report_bug.ReportBugResponse;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_PRODUCTS;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_REPORT_BUG;

public class ReportBugActivity extends AppCompatActivity {

    @BindView(R.id.subject)
    EditText subject;
    @BindView(R.id.content)
    EditText content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_bug);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_back, R.id.btn_kirim})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_kirim:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        ArrayList<View> list = new ArrayList<>();
        list.add(subject);
        list.add(content);
        if (CommonUtil.validateEmptyEntries(list)) {
            postReport();
        }
    }

    private void postReport() {
        AndroidNetworking.post(CATEGORY_REPORT_BUG)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("title", subject.getText().toString())
                .addBodyParameter("message", content.getText().toString())
                .build()
                .getAsObject(ReportBugResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ReportBugResponse) {
                            ReportBugResponse response1 = (ReportBugResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(ReportBugActivity.this, "Berhasil Report Bug..", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(ReportBugActivity.this, "Gagal Report Bug !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(ReportBugActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }
}