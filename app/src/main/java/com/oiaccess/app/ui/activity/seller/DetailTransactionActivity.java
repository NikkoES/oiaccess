package com.oiaccess.app.ui.activity.seller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.DetailOrderAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.create_product.CreateProductResponse;
import com.oiaccess.app.ui.model.detail_order.DetailOrder;
import com.oiaccess.app.ui.model.detail_order.DetailOrderResponse;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_UPDATE_ORDER_RESI;
import static com.oiaccess.app.ui.data.Constants.API_UPDATE_ORDER_STATUS;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_REJECTED;

public class DetailTransactionActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_status)
    ImageView imageStatus;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.id_order_invoice)
    TextView idOrderInvoice;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_nama_penjual)
    TextView txtNamaPenjual;
    @BindView(R.id.txt_nama_pembeli)
    TextView txtNamaPembeli;
    @BindView(R.id.txt_kurir_pengiriman)
    TextView txtKurirPengiriman;
    @BindView(R.id.txt_alamat_tujuan)
    TextView txtAlamatTujuan;
    @BindView(R.id.txt_jumlah_harga)
    TextView txtJumlahHarga;
    @BindView(R.id.txt_ongkos_kirim)
    TextView txtOngkosKirim;
    @BindView(R.id.txt_total_belanja)
    TextView txtTotalBelanja;
    @BindView(R.id.btn_terima_pesanan)
    Button btnTerimaPesanan;
    @BindView(R.id.btn_tolak_pesanan)
    Button btnTolakPesanan;
    @BindView(R.id.btn_kirim_pesanan)
    Button btnKirimPesanan;
    @BindView(R.id.et_resi)
    EditText etResi;
    @BindView(R.id.layout_resi)
    CardView layoutResi;

    String status, idOrder;
    DetailOrderAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaction);
        ButterKnife.bind(this);

        status = getIntent().getStringExtra("status");
        idOrder = getIntent().getStringExtra("id_order");

        Log.e("ID", "" + idOrder);

        initView();
    }

    private void initView() {
        DialogUtils.openDialog(this);
        checkStatus();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DetailOrderAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new DetailOrderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        });
        AndroidNetworking.get(CATEGORY_ORDER + "/" + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(DetailOrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        DetailOrderResponse detailOrderResponse = (DetailOrderResponse) response;
                        DetailOrder detailOrder = detailOrderResponse.getResult();
                        idOrderInvoice.setText("#" + detailOrder.getOrder().getIdOrder());
                        txtNamaPenjual.setText("" + detailOrder.getOrder().getStoreName());
                        txtNamaPembeli.setText("" + detailOrder.getOrder().getBuyerName());
                        txtKurirPengiriman.setText(detailOrder.getOrder().getCourierName());
                        txtAlamatTujuan.setText(detailOrder.getOrder().getAddress());
                        txtJumlahHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(detailOrder.getOrder().getTotalHarga())));
                        txtOngkosKirim.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(detailOrder.getOrder().getCourierCost())));
                        txtTotalBelanja.setText("Rp. " + StringUtils.priceFormatter(String.valueOf((long) (detailOrder.getOrder().getTotalHarga() + detailOrder.getOrder().getCourierCost()))));

                        adapter.swap(detailOrder.getDetails());
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(DetailTransactionActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void checkStatus() {
        switch (status) {
            case ORDER_STATUS_NEW:
                imageStatus.setImageDrawable(getResources().getDrawable(R.drawable.ordernew));
                txtStatus.setText("Menunggu Konfirmasi");
                btnTerimaPesanan.setVisibility(View.VISIBLE);
                btnTolakPesanan.setVisibility(View.VISIBLE);
                break;
            case ORDER_STATUS_ON_PROCESS:
                imageStatus.setImageDrawable(getResources().getDrawable(R.drawable.rr));
                txtStatus.setText("Sedang Diproses");
                layoutResi.setVisibility(View.VISIBLE);
                btnKirimPesanan.setVisibility(View.VISIBLE);
                break;
            case ORDER_STATUS_ON_SHIPPING:
                imageStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_shipped));
                txtStatus.setText("Sedang Dikirim");
                break;
            case ORDER_STATUS_COMPLETED:
                imageStatus.setImageDrawable(getResources().getDrawable(R.drawable.man));
                txtStatus.setText("Sudah Diterima");
                break;
        }
    }

    @OnClick({R.id.btn_back, R.id.btn_terima_pesanan, R.id.btn_tolak_pesanan, R.id.btn_kirim_pesanan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_terima_pesanan:
                updateStatus(ORDER_STATUS_ON_PROCESS);
                break;
            case R.id.btn_tolak_pesanan:
                updateStatus(ORDER_STATUS_REJECTED);
                break;
            case R.id.btn_kirim_pesanan:
                nomorResi();
                break;
        }
    }

    private void updateStatus(String status) {
        DialogUtils.openDialog(this);
        AndroidNetworking.put(API_UPDATE_ORDER_STATUS + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("status", status)
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(DetailTransactionActivity.this, "Berhasil Update Status Pesanan..", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(DetailTransactionActivity.this, "Gagal Update Status Pesanan !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(DetailTransactionActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    private void nomorResi() {
        if (TextUtils.isEmpty(etResi.getText().toString())) {
            Toast.makeText(this, "Nomor Resi Belum Diisi !", Toast.LENGTH_SHORT).show();
        } else {
            DialogUtils.openDialog(this);
            AndroidNetworking.put(API_UPDATE_ORDER_RESI + idOrder)
                    .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                    .addBodyParameter("resi", etResi.getText().toString())
                    .build()
                    .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response instanceof CreateProductResponse) {
                                CreateProductResponse response1 = (CreateProductResponse) response;
                                if (response1.getStatus().equalsIgnoreCase("1")) {
                                    DialogUtils.closeDialog();
                                    Toast.makeText(DetailTransactionActivity.this, "Berhasil Update Status Pesanan..", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    DialogUtils.closeDialog();
                                    Toast.makeText(DetailTransactionActivity.this, "Gagal Update Status Pesanan !", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            DialogUtils.closeDialog();
                            Toast.makeText(DetailTransactionActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                            Log.e("", "onError: " + anError.getErrorDetail());
                            Log.e("", "onError: " + anError.getErrorBody());
                            Log.e("", "onError: " + anError.getErrorCode());
                        }
                    });
        }
    }
}
