
package com.oiaccess.app.ui.model.list_order;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ListOrder {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("courier")
    private String mCourier;
    @SerializedName("courier_cost")
    private Long mCourierCost;
    @SerializedName("courier_name")
    private String mCourierName;
    @SerializedName("id_order")
    private Long mIdOrder;
    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("nama_toko")
    private String namaToko;
    @SerializedName("id_user")
    private Long mIdUser;
    @SerializedName("order_date")
    private String mOrderDate;
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("total_harga")
    private Long mTotalHarga;
    @SerializedName("buyer_name")
    private Long mBuyerName;

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCourier() {
        return mCourier;
    }

    public void setCourier(String courier) {
        mCourier = courier;
    }

    public Long getCourierCost() {
        return mCourierCost;
    }

    public void setCourierCost(Long courierCost) {
        mCourierCost = courierCost;
    }

    public String getCourierName() {
        return mCourierName;
    }

    public void setCourierName(String courierName) {
        mCourierName = courierName;
    }

    public Long getIdOrder() {
        return mIdOrder;
    }

    public void setIdOrder(Long idOrder) {
        mIdOrder = idOrder;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public Long getIdUser() {
        return mIdUser;
    }

    public void setIdUser(Long idUser) {
        mIdUser = idUser;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Long getTotalHarga() {
        return mTotalHarga;
    }

    public void setTotalHarga(Long totalHarga) {
        mTotalHarga = totalHarga;
    }

    public Long getmBuyerName() {
        return mBuyerName;
    }

    public void setmBuyerName(Long mBuyerName) {
        this.mBuyerName = mBuyerName;
    }
}
