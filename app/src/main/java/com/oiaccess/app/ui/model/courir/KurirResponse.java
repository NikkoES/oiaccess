
package com.oiaccess.app.ui.model.courir;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class KurirResponse {

    @SerializedName("result")
    private List<Kurir> mKurir;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Kurir> getResult() {
        return mKurir;
    }

    public void setResult(List<Kurir> kurir) {
        mKurir = kurir;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
