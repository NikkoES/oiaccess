package com.oiaccess.app.ui.model.category;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Kategori {

    @SerializedName("category_name")
    private String mCategoryName;
    @SerializedName("id_category")
    private Integer mIdCategory;
    @SerializedName("images")
    private String mImages;

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    public Integer getIdCategory() {
        return mIdCategory;
    }

    public void setIdCategory(Integer idCategory) {
        mIdCategory = idCategory;
    }

    public String getmImages() {
        return mImages;
    }

    public void setmImages(String mImages) {
        this.mImages = mImages;
    }
}
