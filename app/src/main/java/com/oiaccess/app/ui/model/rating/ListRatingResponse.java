package com.oiaccess.app.ui.model.rating;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListRatingResponse {

    @SerializedName("result")
    private List<ListRating> listRatingList;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;
    @SerializedName("total")
    private Long mTotal;

    public List<ListRating> getListRatingList() {
        return listRatingList;
    }

    public void setListRatingList(List<ListRating> listRatingList) {
        this.listRatingList = listRatingList;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmTimestamp() {
        return mTimestamp;
    }

    public void setmTimestamp(String mTimestamp) {
        this.mTimestamp = mTimestamp;
    }

    public Long getmTotal() {
        return mTotal;
    }

    public void setmTotal(Long mTotal) {
        this.mTotal = mTotal;
    }
}
