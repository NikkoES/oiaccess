package com.oiaccess.app.ui.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

class ImageHelperAsyncTaskLoader extends AsyncTask<Void, Void, Void> {

	private ImageView imageView;
	private String srcFilename;
	private ImageHelperListener listener;
	private Bitmap bitmap;
	private Context context;

	public ImageHelperAsyncTaskLoader(Context context, ImageView imageView, String srcFilename, ImageHelperListener listener) {
		this.context = context;
		this.imageView = imageView;
		this.srcFilename = srcFilename;
		this.listener = listener;
	}

	@Override
	protected void onPreExecute() {
		if (listener != null) {
			listener.onProcessStart();
		}
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		loadBitmapFromFile();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		imageView.post(new Runnable() {

			@Override
			public void run() {
				imageView.setImageBitmap(convert(bitmap, Config.RGB_565));
			}
		});

		if (listener != null) {
			listener.onProcessEnd(srcFilename);
		}

		super.onPostExecute(result);
	}

	private Bitmap convert(Bitmap bitmap, Config config) {
	    Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
	    Canvas canvas = new Canvas(convertedBitmap);
	    Paint paint = new Paint();
	    paint.setColor(Color.BLACK);
	    canvas.drawBitmap(bitmap, 0, 0, paint);
	    return convertedBitmap;
	}
	
	private void loadBitmapFromFile(){
		try{
			ContextWrapper cw = new ContextWrapper(imageView.getContext());
			File fileDir = cw.getDir(ImageHelper.folderName, Context.MODE_PRIVATE);
			File savedFile = new File(fileDir, srcFilename);
			
			try {
				FileInputStream fis = new FileInputStream(savedFile);
				for (int i = 0; i < 10; i++) {
					fis.read();
				}
//				bitmap = BitmapFactory.decodeStream(fis);
				
				BitmapFactory.Options options = new BitmapFactory.Options();
			    options.inJustDecodeBounds = true;
			    BitmapFactory.decodeStream(fis, null, options);

			    options.inSampleSize = calculateInSampleSize(options, 200, 200);

			    options.inJustDecodeBounds = false;
			    
			    fis = new FileInputStream(savedFile);
				for (int i = 0; i < 10; i++) {
					fis.read();
				}
//			    Bitmap bitmap = BitmapFactory.decodeStream(fis, null, options);
			    bitmap = BitmapFactory.decodeStream(fis, null, options);
			} catch (FileNotFoundException e) {
				
			} catch (IOException e) {
				
			}
		}catch(OutOfMemoryError e){
			e.printStackTrace();
			bitmap.recycle();
			bitmap = null;
		}
	}
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		int height = options.outHeight;
		int width = options.outWidth;
		int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {
	
	        int halfHeight = height;
	        int halfWidth = width;
	
	        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }

	    return inSampleSize;
	}
	
	public int dpToPx(int dp) {
	    DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
	    
	    return px;
	}
}
