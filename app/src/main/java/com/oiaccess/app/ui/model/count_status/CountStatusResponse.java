
package com.oiaccess.app.ui.model.count_status;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CountStatusResponse {

    @SerializedName("id_user")
    private String mIdUser;
    @SerializedName("result")
    private List<CountStatus> mCountStatus;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public String getIdUser() {
        return mIdUser;
    }

    public void setIdUser(String idUser) {
        mIdUser = idUser;
    }

    public List<CountStatus> getResult() {
        return mCountStatus;
    }

    public void setResult(List<CountStatus> countStatus) {
        mCountStatus = countStatus;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
