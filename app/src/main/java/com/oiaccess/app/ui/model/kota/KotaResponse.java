
package com.oiaccess.app.ui.model.kota;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class KotaResponse {

    @SerializedName("results")
    private List<Kota> mKotas;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Kota> getResults() {
        return mKotas;
    }

    public void setResults(List<Kota> kotas) {
        mKotas = kotas;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
