
package com.oiaccess.app.ui.model.kecamatan;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class KecamatanResponse {

    @SerializedName("results")
    private List<Kecamatan> mKecamatans;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Kecamatan> getResults() {
        return mKecamatans;
    }

    public void setResults(List<Kecamatan> kecamatans) {
        mKecamatans = kecamatans;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
