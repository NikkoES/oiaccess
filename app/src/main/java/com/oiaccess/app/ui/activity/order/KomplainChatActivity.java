package com.oiaccess.app.ui.activity.order;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.KomplainBuyerAdapter;
import com.oiaccess.app.ui.adapter.recycler_view.KomplainSellerAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.create_product.CreateProductResponse;
import com.oiaccess.app.ui.model.komplain.KomplainResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_UPDATE_ORDER_STATUS;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_COMPLAIN;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;

public class KomplainChatActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;

    String idOrder, status;

    Long idKomplain;

    KomplainBuyerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.komplain_chat);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        idOrder = getIntent().getStringExtra("id_order");
        status = getIntent().getStringExtra("status");

        setHeaderToolbar();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new KomplainBuyerAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new KomplainBuyerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        });

        loadItems();
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        etMessage.setText("");
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder<>(CATEGORY_COMPLAIN + idOrder);
        getRequestBuilder
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(KomplainResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof KomplainResponse) {
                            DialogUtils.closeDialog();
                            KomplainResponse response1 = (KomplainResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                adapter.swap(response1.getResult().getChats());
                                idKomplain = response1.getResult().getIdKomplain();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(KomplainChatActivity.this, "Anda belum menulis komplain !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setHeaderToolbar() {
        headerTitleTV.setText("Komplain");
    }

    @OnClick({R.id.btn_komplain_selesai, R.id.btn_image, R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_komplain_selesai:
                DialogUtils.dialogYesNo(this, "Anda yakin ingin mengakhiri komplain ?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateStatus(ORDER_STATUS_COMPLETED);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.btn_image:
                break;
            case R.id.btn_send:
                if (etMessage.getText().toString().trim().equals("")) {
                    Toast.makeText(KomplainChatActivity.this, "Pesan belum diisi !", Toast.LENGTH_SHORT).show();
                } else {
                    if (adapter.getItemCount() > 0) {
                        insertKomplain();
                    } else {
                        insertFirst();
                    }
                }
                break;
        }
    }

    private void insertFirst() {
        DialogUtils.openDialog(this);
        AndroidNetworking.post(CATEGORY_COMPLAIN + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("sender", "buyer")
                .addBodyParameter("text", etMessage.getText().toString())
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                loadItems();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(KomplainChatActivity.this, "Gagal Komplain Order !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(KomplainChatActivity.this, "Gagal Komplain Order !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    private void insertKomplain() {
        DialogUtils.openDialog(this);
        AndroidNetworking.post(CATEGORY_COMPLAIN + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addQueryParameter("chat", "true")
                .addQueryParameter("komplain", String.valueOf(idKomplain))
                .addBodyParameter("sender", "buyer")
                .addBodyParameter("text", etMessage.getText().toString())
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                loadItems();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(KomplainChatActivity.this, "Gagal Komplain Order !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(KomplainChatActivity.this, "Gagal Komplain Order !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    private void updateStatus(String status) {
        DialogUtils.openDialog(this);
        AndroidNetworking.put(API_UPDATE_ORDER_STATUS + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("status", status)
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(KomplainChatActivity.this, "Berhasil Update Status Pesanan..", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(KomplainChatActivity.this, "Gagal Update Status Pesanan !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(KomplainChatActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
