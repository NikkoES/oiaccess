package com.oiaccess.app.ui.activity.order;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.bumptech.glide.Glide;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.create_product.CreateProductResponse;
import com.oiaccess.app.ui.model.detail_order.Detail;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_UPDATE_ORDER_STATUS;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_RATINGS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_RATING;

public class UlasanProdukActivity extends AppCompatActivity {

    @BindView(R.id.image_produk)
    ImageView imageProduk;
    @BindView(R.id.txt_nama_produk)
    TextView txtNamaProduk;
    @BindView(R.id.rating_bar)
    AppCompatRatingBar ratingBar;
    @BindView(R.id.et_ulasan)
    EditText etUlasan;
    //    @BindView(R.id.et_nama)
//    EditText etNama;
    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;

    Detail item;
    String idToko;
    String idOrder;

    float totalRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ulasan_produk);
        ButterKnife.bind(this);

        item = (Detail) getIntent().getSerializableExtra("item");
        idToko = getIntent().getStringExtra("id_toko");
        idOrder = getIntent().getStringExtra("id_order");

        initView();
    }

    private void initView() {
        headerTitleTV.setText("Ulasan Produk");

        Glide.with(this).load(item.getThumbnail()).into(imageProduk);
        txtNamaProduk.setText(item.getNamaProduct());

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                totalRating = rating;
            }
        });
    }


    private void checkValidation() {
        ArrayList<View> list = new ArrayList<>();
        list.add(etUlasan);
        if (CommonUtil.validateEmptyEntries(list)) {
            postRating();
        }
    }

    private void postRating() {
        Log.e("id_toko", "" + idToko);
        Log.e("id_order", "" + idOrder);
        Log.e("rating", "" + String.valueOf(totalRating));
        Log.e("message", "" + etUlasan.getText().toString());
        DialogUtils.openDialog(this);
        AndroidNetworking.post(CATEGORY_RATINGS + item.getIdProduct())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("id_toko", idToko.toString())
                .addBodyParameter("id_order", idOrder.toString())
                .addBodyParameter("buyer_name", new Session(this).getUser().getFullname())
                .addBodyParameter("rating", String.valueOf(totalRating))
                .addBodyParameter("message", etUlasan.getText().toString())
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(UlasanProdukActivity.this, "Berhasil Mengulas Produk..", Toast.LENGTH_SHORT).show();
                                finish();
//                                updateStatus(ORDER_STATUS_RATING);
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(UlasanProdukActivity.this, "Gagal Mengulas Produk !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(UlasanProdukActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    private void updateStatus(String status) {
        DialogUtils.openDialog(this);
        AndroidNetworking.put(API_UPDATE_ORDER_STATUS + item.getIdOrder())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("status", status)
                .build()
                .getAsObject(CreateProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CreateProductResponse) {
                            CreateProductResponse response1 = (CreateProductResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                DialogUtils.closeDialog();
                                Toast.makeText(UlasanProdukActivity.this, "Berhasil Mengulas Produk..", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(UlasanProdukActivity.this, "Gagal Mengulas Produk !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(UlasanProdukActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        Log.e("", "onError: " + anError.getErrorDetail());
                        Log.e("", "onError: " + anError.getErrorBody());
                        Log.e("", "onError: " + anError.getErrorCode());
                    }
                });
    }

    @OnClick({R.id.rating_bar, R.id.btn_kirim_rating})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rating_bar:
                break;
            case R.id.btn_kirim_rating:
                checkValidation();
                break;
        }
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
