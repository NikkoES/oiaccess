
package com.oiaccess.app.ui.model.cart;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Cart {

    @SerializedName("details")
    private List<Detail> mDetails;
    @SerializedName("grandtotal")
    private Long mGrandtotal;
    @SerializedName("id_cart")
    private Long mIdCart;
    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("id_user")
    private Long mIdUser;
    @SerializedName("store_name")
    private String mStoreName;

    public List<Detail> getDetails() {
        return mDetails;
    }

    public void setDetails(List<Detail> details) {
        mDetails = details;
    }

    public Long getGrandtotal() {
        return mGrandtotal;
    }

    public void setGrandtotal(Long grandtotal) {
        mGrandtotal = grandtotal;
    }

    public Long getIdCart() {
        return mIdCart;
    }

    public void setIdCart(Long idCart) {
        mIdCart = idCart;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public Long getIdUser() {
        return mIdUser;
    }

    public void setIdUser(Long idUser) {
        mIdUser = idUser;
    }

    public String getStoreName() {
        return mStoreName;
    }

    public void setStoreName(String storeName) {
        mStoreName = storeName;
    }

}
