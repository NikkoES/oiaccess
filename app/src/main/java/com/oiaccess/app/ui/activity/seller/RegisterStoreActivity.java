package com.oiaccess.app.ui.activity.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.BaseActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.common.CommonResponse;
import com.oiaccess.app.ui.utils.CommonUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_USER_STORE_REGISTER;

public class RegisterStoreActivity extends BaseActivity {

    @BindView(R.id.storeNameET)
    EditText storeNameET;
//    @BindView(R.id.sloganET)
//    EditText sloganET;
//    @BindView(R.id.descriptionET)
//    EditText descriptionET;
//    @BindView(R.id.storePhotoIV)
//    ImageView storePhotoIV;
//
//    String pathImage;
//
//    int max_resolution_image = 800;
    CommonResponse commonResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_store);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.backBtn, R.id.openStoreBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
//            case R.id.storePhotoIV:
//                ImagePicker.create(this)
//                        .returnMode(ReturnMode.ALL)
//                        .single()
//                        .start();
//                break;
            case R.id.openStoreBtn:
                checkValidation();
//                registerStore();
                break;
        }
    }

    private void checkValidation() {
        ArrayList<View> list = new ArrayList<>();
//        list.add(descriptionET);
//        list.add(sloganET);
        list.add(storeNameET);
        if (CommonUtil.validateEmptyEntries(list)) {
            registerStore();
        }
    }

    private void registerStore() {
        showProgressDialog(RegisterStoreActivity.this, getString(R.string.progress_wait));
            AndroidNetworking.post(API_USER_STORE_REGISTER)
                    .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                    .addBodyParameter("name", storeNameET.getText().toString())
                    .build()
                    .getAsObject(CommonResponse.class, new ParsedRequestListener() {
                        @Override
                        public void onResponse(Object response) {
                            commonResponse = (CommonResponse) response;
                            Toast.makeText(getApplicationContext(), commonResponse.getmResult(), Toast.LENGTH_SHORT).show();
                            if(commonResponse.getmStatus().equals("1")){
                                finish();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(getApplicationContext(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                        }
                    });
            hideProgressDialog();
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            File file = new File(image.getPath());
//            pathImage = image.getPath();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//            Log.e("PATH", pathImage);

//            storePhotoIV.setVisibility(View.VISIBLE);
//            storePhotoIV.setImageBitmap(getResizedBitmap(bitmap, max_resolution_image));
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
