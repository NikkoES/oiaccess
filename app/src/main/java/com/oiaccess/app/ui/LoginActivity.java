package com.oiaccess.app.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import com.oiaccess.app.R;
import com.oiaccess.app.services.ApiClient;
import com.oiaccess.app.services.Constants;
import com.oiaccess.app.ui.activity.MainActivity;
import com.oiaccess.app.ui.activity.SampleActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.auth.User;
import com.oiaccess.app.ui.model.auth.UserResponse;

import org.json.JSONObject;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    Button btn_daftar, btn_masuk;
    EditText et_phone, et_pass;

    String phone, pass, group_id;
    private PermissionHandler PermissionHandlerLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_daftar = (Button) findViewById(R.id.btn_daftar);
        btn_masuk = (Button) findViewById(R.id.btn_masuk);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_pass = (EditText) findViewById(R.id.et_pass);
        group_id = Constants.GROUP_ID;

        PermissionHandlerLocation = new PermissionHandler() {
            @Override
            public void onGranted() {

                login();
            }
        };

        initUi();
    }

    private void initUi() {
        btn_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] permissions = {Manifest.permission.READ_SMS, Manifest.permission.ACCESS_COARSE_LOCATION};
                Permissions.check(LoginActivity.this, permissions, null, null, PermissionHandlerLocation);
            }
        });

        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(LoginActivity.this, RegisterHpActivity.class));

            }
        });
    }


    private void login() {
        phone = et_phone.getText().toString().trim();
        pass = et_pass.getText().toString().trim();

        showProgressDialog(LoginActivity.this, getString(R.string.progress_wait));
        apiClient = new ApiClient();
        Call<UserResponse> call = apiClient.getApiInterfaceOi().postLogin(phone, pass, "");
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    UserResponse login = response.body();

                    hideProgressDialog();
                    if (login.getStatus().equals("1")) {
                        hideProgressDialog();
                        User user = login.getResult();

                        new Session(LoginActivity.this).createLoginSession(user);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    hideProgressDialog();
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showErrorAlert(jObjError.getString("error_message"));

                    } catch (Exception e) {
                        showToast(LoginActivity.this, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                hideProgressDialog();
                Log.d("Login Failure", t.getMessage());
                showAlertNoInternet();
            }
        });
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setTitle("Peringatan")
                .setMessage("Anda yakin akan keluar aplikasi?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LoginActivity.super.onBackPressed();
                        moveTaskToBack(true);
                        Log.d("LoginActivity", "Test exit 0");
                        //android.os.Process.killProcess(android.os.Process.myPid());
                        //Log.d("LoginActivity","Test exit 1");
                        LoginActivity.this.finish();
                        System.exit(0);

                    }
                }).create().show();
    }


}
