
package com.oiaccess.app.ui.model.category;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class KategoriResponse {

    @SerializedName("result")
    private List<Kategori> mKategori;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Kategori> getResult() {
        return mKategori;
    }

    public void setResult(List<Kategori> kategori) {
        mKategori = kategori;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
