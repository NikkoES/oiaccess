
package com.oiaccess.app.ui.model.add_cart;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddCartResponse {

    @SerializedName("result")
    private AddCart mAddCart;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public AddCart getResult() {
        return mAddCart;
    }

    public void setResult(AddCart addCart) {
        mAddCart = addCart;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
