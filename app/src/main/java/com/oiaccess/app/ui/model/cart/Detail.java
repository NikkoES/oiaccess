
package com.oiaccess.app.ui.model.cart;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Detail {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("diskon")
    private String mDiskon;
    @SerializedName("harga")
    private Long mHarga;
    @SerializedName("harga_diskon")
    private Long mHargaDiskon;
    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("image_name")
    private String mImageName;
    @SerializedName("jumlah")
    private Long mJumlah;
    @SerializedName("kondisi")
    private String mKondisi;
    @SerializedName("min_order")
    private Long mMinOrder;
    @SerializedName("nama_product")
    private String mNamaProduct;
    @SerializedName("product_sold")
    private Long mProductSold;
    @SerializedName("stock")
    private Long mStock;
    @SerializedName("total")
    private Long mTotal;
    @SerializedName("weight")
    private Long mWeight;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDiskon() {
        return mDiskon;
    }

    public void setDiskon(String diskon) {
        mDiskon = diskon;
    }

    public Long getHarga() {
        return mHarga;
    }

    public void setHarga(Long harga) {
        mHarga = harga;
    }

    public Long getHargaDiskon() {
        return mHargaDiskon;
    }

    public void setHargaDiskon(Long hargaDiskon) {
        mHargaDiskon = hargaDiskon;
    }

    public Long getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(Long idProduct) {
        mIdProduct = idProduct;
    }

    public String getImageName() {
        return mImageName;
    }

    public void setImageName(String imageName) {
        mImageName = imageName;
    }

    public Long getJumlah() {
        return mJumlah;
    }

    public void setJumlah(Long jumlah) {
        mJumlah = jumlah;
    }

    public String getKondisi() {
        return mKondisi;
    }

    public void setKondisi(String kondisi) {
        mKondisi = kondisi;
    }

    public Long getMinOrder() {
        return mMinOrder;
    }

    public void setMinOrder(Long minOrder) {
        mMinOrder = minOrder;
    }

    public String getNamaProduct() {
        return mNamaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        mNamaProduct = namaProduct;
    }

    public Long getProductSold() {
        return mProductSold;
    }

    public void setProductSold(Long productSold) {
        mProductSold = productSold;
    }

    public Long getStock() {
        return mStock;
    }

    public void setStock(Long stock) {
        mStock = stock;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

    public Long getWeight() {
        return mWeight;
    }

    public void setWeight(Long weight) {
        mWeight = weight;
    }

}
