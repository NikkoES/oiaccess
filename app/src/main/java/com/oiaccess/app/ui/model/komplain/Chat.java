
package com.oiaccess.app.ui.model.komplain;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Chat {

    @SerializedName("chat_created")
    private String mChatCreated;
    @SerializedName("id_chats")
    private Long mIdChats;
    @SerializedName("id_order")
    private Long mIdOrder;
    @SerializedName("sender")
    private String mSender;
    @SerializedName("text")
    private String mText;

    public String getChatCreated() {
        return mChatCreated;
    }

    public void setChatCreated(String chatCreated) {
        mChatCreated = chatCreated;
    }

    public Long getIdChats() {
        return mIdChats;
    }

    public void setIdChats(Long idChats) {
        mIdChats = idChats;
    }

    public Long getIdOrder() {
        return mIdOrder;
    }

    public void setIdOrder(Long idOrder) {
        mIdOrder = idOrder;
    }

    public String getSender() {
        return mSender;
    }

    public void setSender(String sender) {
        mSender = sender;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

}
