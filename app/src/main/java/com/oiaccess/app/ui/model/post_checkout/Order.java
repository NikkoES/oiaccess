
package com.oiaccess.app.ui.model.post_checkout;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Order implements Serializable {

    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("grand_total")
    private Long mGrandTotal;
    @SerializedName("address")
    private String mAddress;
    @SerializedName("courier")
    private Courier mCourier;
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("items")
    private List<Item> mItems;

    private String mCatatan;
    private String mNamaToko;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCatatan() {
        return mCatatan;
    }

    public void setCatatan(String catatan) {
        mCatatan = catatan;
    }

    public Courier getCourier() {
        return mCourier;
    }

    public void setCourier(Courier courier) {
        mCourier = courier;
    }

    public Long getGrandTotal() {
        return mGrandTotal;
    }

    public void setGrandTotal(Long grandTotal) {
        mGrandTotal = grandTotal;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public List<Item> getItems() {
        return mItems;
    }

    public void setItems(List<Item> items) {
        mItems = items;
    }

    public String getNamaToko() {
        return mNamaToko;
    }

    public void setNamaToko(String namaToko) {
        mNamaToko = namaToko;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

}
