
package com.oiaccess.app.ui.model.delete_cart;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DeleteCartResponse {

    @SerializedName("result")
    private DeleteCart mDeleteCart;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public DeleteCart getResult() {
        return mDeleteCart;
    }

    public void setResult(DeleteCart deleteCart) {
        mDeleteCart = deleteCart;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
