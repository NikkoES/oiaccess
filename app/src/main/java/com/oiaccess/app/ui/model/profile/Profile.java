
package com.oiaccess.app.ui.model.profile;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Profile {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("districts")
    private String mDistricts;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("fcm_id")
    private String mFcmId;
    @SerializedName("fcm_id_ios")
    private String mFcmIdIos;
    @SerializedName("fullname")
    private String mFullname;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("gerai_name")
    private String mGeraiName;
    @SerializedName("id_card_number")
    private String mIdCardNumber;
    @SerializedName("id_card_number_photo")
    private String mIdCardNumberPhoto;
    @SerializedName("id_card_number_selfi")
    private String mIdCardNumberSelfi;
    @SerializedName("is_active")
    private String mIsActive;
    @SerializedName("is_confirmed_email")
    private String mIsConfirmedEmail;
    @SerializedName("is_confirmed_hp")
    private String mIsConfirmedHp;
    @SerializedName("is_seller")
    private String mIsSeller;
    @SerializedName("is_verified")
    private String mIsVerified;
    @SerializedName("job")
    private String mJob;
    @SerializedName("kta_photo")
    private String mKtaPhoto;
    @SerializedName("mom")
    private String mMom;
    @SerializedName("my_referal_code")
    private String mMyReferalCode;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("pob")
    private String mPob;
    @SerializedName("postalcode")
    private String mPostalcode;
    @SerializedName("profile_photo")
    private String mProfilePhoto;
    @SerializedName("province")
    private String mProvince;
    @SerializedName("referal_code")
    private String mReferalCode;
    @SerializedName("status_verifikasi")
    private String mStatusVerifikasi;
    @SerializedName("user_id")
    private Long mUserId;
    @SerializedName("verifikasi_msg")
    private String mVerifikasiMsg;
    @SerializedName("village")
    private String mVillage;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getDistricts() {
        return mDistricts;
    }

    public void setDistricts(String districts) {
        mDistricts = districts;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFcmId() {
        return mFcmId;
    }

    public void setFcmId(String fcmId) {
        mFcmId = fcmId;
    }

    public String getFcmIdIos() {
        return mFcmIdIos;
    }

    public void setFcmIdIos(String fcmIdIos) {
        mFcmIdIos = fcmIdIos;
    }

    public String getFullname() {
        return mFullname;
    }

    public void setFullname(String fullname) {
        mFullname = fullname;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getGeraiName() {
        return mGeraiName;
    }

    public void setGeraiName(String geraiName) {
        mGeraiName = geraiName;
    }

    public String getIdCardNumber() {
        return mIdCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        mIdCardNumber = idCardNumber;
    }

    public String getIdCardNumberPhoto() {
        return mIdCardNumberPhoto;
    }

    public void setIdCardNumberPhoto(String idCardNumberPhoto) {
        mIdCardNumberPhoto = idCardNumberPhoto;
    }

    public String getIdCardNumberSelfi() {
        return mIdCardNumberSelfi;
    }

    public void setIdCardNumberSelfi(String idCardNumberSelfi) {
        mIdCardNumberSelfi = idCardNumberSelfi;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String isActive) {
        mIsActive = isActive;
    }

    public String getIsConfirmedEmail() {
        return mIsConfirmedEmail;
    }

    public void setIsConfirmedEmail(String isConfirmedEmail) {
        mIsConfirmedEmail = isConfirmedEmail;
    }

    public String getIsConfirmedHp() {
        return mIsConfirmedHp;
    }

    public void setIsConfirmedHp(String isConfirmedHp) {
        mIsConfirmedHp = isConfirmedHp;
    }

    public String getIsSeller() {
        return mIsSeller;
    }

    public void setIsSeller(String isSeller) {
        mIsSeller = isSeller;
    }

    public String getIsVerified() {
        return mIsVerified;
    }

    public void setIsVerified(String isVerified) {
        mIsVerified = isVerified;
    }

    public String getJob() {
        return mJob;
    }

    public void setJob(String job) {
        mJob = job;
    }

    public String getKtaPhoto() {
        return mKtaPhoto;
    }

    public void setKtaPhoto(String ktaPhoto) {
        mKtaPhoto = ktaPhoto;
    }

    public String getMom() {
        return mMom;
    }

    public void setMom(String mom) {
        mMom = mom;
    }

    public String getMyReferalCode() {
        return mMyReferalCode;
    }

    public void setMyReferalCode(String myReferalCode) {
        mMyReferalCode = myReferalCode;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPob() {
        return mPob;
    }

    public void setPob(String pob) {
        mPob = pob;
    }

    public String getPostalcode() {
        return mPostalcode;
    }

    public void setPostalcode(String postalcode) {
        mPostalcode = postalcode;
    }

    public String getProfilePhoto() {
        return mProfilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        mProfilePhoto = profilePhoto;
    }

    public String getProvince() {
        return mProvince;
    }

    public void setProvince(String province) {
        mProvince = province;
    }

    public String getReferalCode() {
        return mReferalCode;
    }

    public void setReferalCode(String referalCode) {
        mReferalCode = referalCode;
    }

    public String getStatusVerifikasi() {
        return mStatusVerifikasi;
    }

    public void setStatusVerifikasi(String statusVerifikasi) {
        mStatusVerifikasi = statusVerifikasi;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getVerifikasiMsg() {
        return mVerifikasiMsg;
    }

    public void setVerifikasiMsg(String verifikasiMsg) {
        mVerifikasiMsg = verifikasiMsg;
    }

    public String getVillage() {
        return mVillage;
    }

    public void setVillage(String village) {
        mVillage = village;
    }

}
