package com.oiaccess.app.ui.model.rating;

import com.google.gson.annotations.SerializedName;

public class StoreRating {
    @SerializedName("total")
    private Long mTotal;
    @SerializedName("rating")
    private String mRating;

    public Long getmTotal() {
        return mTotal;
    }

    public void setmTotal(Long mTotal) {
        this.mTotal = mTotal;
    }

    public String getmRating() {
        return mRating;
    }

    public void setmRating(String mRating) {
        this.mRating = mRating;
    }
}
