package com.oiaccess.app.ui.application;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;
import com.oiaccess.app.ui.model.entitiy_cart.MyObjectBox;

import io.objectbox.BoxStore;


public class MyApplication extends  MultiDexApplication {

    private static BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidNetworking.initialize(getApplicationContext());
        //AndroidNetworking.enableLogging(com.androidnetworking.interceptors.HttpLoggingInterceptor.Level.BODY);
        //


        boxStore = MyObjectBox.builder().androidContext(this).build();
    }

    public static BoxStore getBoxStore() {
        return boxStore;
    }

}
