
package com.oiaccess.app.ui.model.count_status;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CountStatus {

    @SerializedName("complained")
    private Long mComplained;
    @SerializedName("completed")
    private Long mCompleted;
    @SerializedName("new")
    private Long mNew;
    @SerializedName("needrating")
    private Long mNeedrating;
    @SerializedName("onprocess")
    private Long mOnprocess;
    @SerializedName("onshipping")
    private Long mOnshipping;
    @SerializedName("paid")
    private Long mPaid;
    @SerializedName("rejected")
    private Long mRejected;
    @SerializedName("waitingpayment")
    private Long mWaitingpayment;

    public Long getmNew() {
        return mNew;
    }

    public void setmNew(Long mNew) {
        this.mNew = mNew;
    }

    public Long getComplained() {
        return mComplained;
    }

    public void setComplained(Long complained) {
        mComplained = complained;
    }

    public Long getCompleted() {
        return mCompleted;
    }

    public void setCompleted(Long completed) {
        mCompleted = completed;
    }

    public Long getNeedrating() {
        return mNeedrating;
    }

    public void setNeedrating(Long needrating) {
        mNeedrating = needrating;
    }

    public Long getOnprocess() {
        return mOnprocess;
    }

    public void setOnprocess(Long onprocess) {
        mOnprocess = onprocess;
    }

    public Long getOnshipping() {
        return mOnshipping;
    }

    public void setOnshipping(Long onshipping) {
        mOnshipping = onshipping;
    }

    public Long getPaid() {
        return mPaid;
    }

    public void setPaid(Long paid) {
        mPaid = paid;
    }

    public Long getRejected() {
        return mRejected;
    }

    public void setRejected(Long rejected) {
        mRejected = rejected;
    }

    public Long getWaitingpayment() {
        return mWaitingpayment;
    }

    public void setWaitingpayment(Long waitingpayment) {
        mWaitingpayment = waitingpayment;
    }

}
