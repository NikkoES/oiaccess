package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.oiaccess.app.ui.activity.order.UlasanProdukListActivity;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.list_order.ListOrder;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_RATING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_REJECTED;

/**
 * Created by Comp on 2/11/2018.
 */

public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ListOrder> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }


    public OrderListAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_prof)
        RoundedImageView iconProf;
        @BindView(R.id.order_id)
        TextView orderId;
        @BindView(R.id.tgl)
        TextView tgl;
        @BindView(R.id.toko_beli)
        TextView tokoBeli;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.total_harga)
        TextView totalHarga;
        @BindView(R.id.ulasbarang)
        LinearLayout ulasBarang;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_list, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            final ListOrder item = listItem.get(position);

//
//            Glide.with(ctx).load(item.getImageName()).into(view.productImageIV);
//            view.productNameTV.setText(item.getNamaProduct());
//            view.priceTV.setText(StringUtils.priceFormatter(String.valueOf(item.getHarga())));
//            view.discountPriceTV.setText(StringUtils.priceFormatter(String.valueOf(item.getHargaDiskon())));
//            view.discountTV.setText(item.getStock() + " pcs");

            view.ulasBarang.setVisibility(View.GONE);
            view.iconProf.setImageDrawable(ctx.getResources().getDrawable(R.drawable.noimage));
            view.orderId.setText("#" + item.getIdOrder());
            view.tgl.setText(CommonUtil.generalFormatDate(item.getOrderDate(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd/MM/yyyy"));
            view.tokoBeli.setText(item.getNamaToko());
            view.totalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf((long) (item.getTotalHarga() + item.getCourierCost()))));
            if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_NEW)) {
                view.status.setText("Menunggu Konfirmasi");
                view.ulasBarang.setVisibility(View.GONE);
            } else if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_ON_PROCESS)) {
                view.status.setText("Sedang Diproses");
                view.ulasBarang.setVisibility(View.GONE);
            } else if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_ON_SHIPPING)) {
                view.status.setText("Sedang Dikirim");
                view.ulasBarang.setVisibility(View.GONE);
            } else if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_RATING)) {
                view.status.setText("Belum Dirating");
                view.ulasBarang.setVisibility(View.VISIBLE);
            } else if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_COMPLETED)) {
                view.status.setText("Sampai Tujuan");
                view.ulasBarang.setVisibility(View.GONE);
            } else if (item.getStatus().equalsIgnoreCase(ORDER_STATUS_REJECTED)) {
                view.status.setText("Pesanan Ditolak");
                view.ulasBarang.setVisibility(View.GONE);
            }

            view.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });

            view.ulasBarang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), UlasanProdukListActivity.class);
                    i.putExtra("id_order", "" + item.getIdOrder());
                    v.getContext().startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(ListOrder item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<ListOrder> listItem) {
        for (ListOrder item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<ListOrder> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public ListOrder getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<ListOrder> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<ListOrder> getListItem() {
        return listItem;
    }
}
