package com.oiaccess.app.ui.model.rating;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoreRatingResponse {
    @SerializedName("result")
    private List<StoreRating> storeRatingList;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<StoreRating> getStoreRatingList() {
        return storeRatingList;
    }

    public void setStoreRatingList(List<StoreRating> storeRatingList) {
        this.storeRatingList = storeRatingList;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmTimestamp() {
        return mTimestamp;
    }

    public void setmTimestamp(String mTimestamp) {
        this.mTimestamp = mTimestamp;
    }
}
