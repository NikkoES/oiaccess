package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.detail_order.Detail;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class DetailOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<Detail> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public DetailOrderAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_item)
        ImageView imageItem;
        @BindView(R.id.txt_nama)
        TextView txtNama;
        @BindView(R.id.txt_jumlah)
        TextView txtJumlah;
        @BindView(R.id.txt_kategori)
        TextView txtKategori;
        @BindView(R.id.txt_harga)
        TextView txtHarga;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_order, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            Detail item = listItem.get(position);

            Glide.with(ctx).load(item.getThumbnail()).into(view.imageItem);
            view.txtNama.setText(item.getNamaProduct());
            Long harga = new Long("0");
            if (item.getHargaDiskon() > 0) {
                Long diskon = (item.getHarga() * item.getHargaDiskon()) / 100;
                harga = item.getHarga() - diskon;
            } else {
                harga = item.getHarga();
            }
            view.txtHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(harga)));
            view.txtKategori.setText(item.getCategoryName());
            view.txtJumlah.setText("Jumlah : " + item.getJumlah());

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(Detail item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<Detail> listItem) {
        for (Detail item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<Detail> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public Detail getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<Detail> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<Detail> getListItem() {
        return listItem;
    }
}
