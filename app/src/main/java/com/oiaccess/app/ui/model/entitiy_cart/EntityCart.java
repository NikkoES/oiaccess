package com.oiaccess.app.ui.model.entitiy_cart;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class EntityCart {

    @Id(assignable = true)
    public Long id;
    public String idUser;
    public String idProduk;
    public String idToko;
    public String jumlah;

}
