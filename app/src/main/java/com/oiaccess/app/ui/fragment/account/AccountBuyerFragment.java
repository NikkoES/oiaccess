package com.oiaccess.app.ui.fragment.account;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.buyer.ReportBugActivity;
import com.oiaccess.app.ui.activity.buyer.TermConditionActivity;
import com.oiaccess.app.ui.activity.seller.WithdrawActivity;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.fragment.BaseFragment;
import com.oiaccess.app.ui.model.balance.BalanceResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityOrder;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_USER_PROFILE_WALLET;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountBuyerFragment extends BaseFragment {


    @BindView(R.id.balanceTV)
    TextView balanceTV;
    Unbinder unbinder;
    BalanceResponse balanceResponse;

    BoxStore boxStore;
    Box<EntityOrder> orderBox;
    Box<EntityItem> itemBox;
    Box<EntityCart> cartBox;

    public AccountBuyerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account_buyer, container, false);
        unbinder = ButterKnife.bind(this, view);

        boxStore = MyApplication.getBoxStore();
        orderBox = boxStore.boxFor(EntityOrder.class);
        itemBox = boxStore.boxFor(EntityItem.class);
        cartBox = boxStore.boxFor(EntityCart.class);

        getBalance();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            getBalance();
        }
    }

    @OnClick({R.id.termBtn, R.id.contactBtn, R.id.reportBtn, R.id.logoutBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.termBtn:
                Intent term = new Intent(getActivity(), TermConditionActivity.class);
                startActivity(term);
                break;
            case R.id.contactBtn:
                break;
            case R.id.reportBtn:
                Intent report = new Intent(getActivity(), ReportBugActivity.class);
                startActivity(report);
                break;
            case R.id.logoutBtn:
                orderBox.removeAll();
                itemBox.removeAll();
                cartBox.removeAll();
                new Session(getActivity()).logoutUser();
                break;
        }
    }

    public void getBalance() {
        DialogUtils.openDialog(getActivity());
        AndroidNetworking.get(API_USER_PROFILE_WALLET)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .build()
                .getAsObject(BalanceResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        balanceResponse = (BalanceResponse) response;
                        balanceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(balanceResponse.getResult().getBalance())));
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
