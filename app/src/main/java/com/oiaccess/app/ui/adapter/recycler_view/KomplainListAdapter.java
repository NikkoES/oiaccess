package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.list_order.ListOrder;
import com.oiaccess.app.ui.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class KomplainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ListOrder> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }


    public KomplainListAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_id_order)
        TextView txtIdOrder;
        @BindView(R.id.txt_nama_toko)
        TextView txtNamaToko;
        @BindView(R.id.image)
        CircularImageView image;
        @BindView(R.id.txt_selesai_dalam)
        TextView txtSelesaiDalam;
        @BindView(R.id.txt_status_komplain)
        TextView txtStatusKomplain;
        @BindView(R.id.btn_lihat_komplain)
        Button btnLihatKomplain;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_komplain, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            ListOrder item = listItem.get(position);

            view.image.setImageDrawable(ctx.getResources().getDrawable(R.drawable.noimage));
            view.txtIdOrder.setText("#" + item.getIdOrder());
            view.txtSelesaiDalam.setText(CommonUtil.generalFormatDate(item.getOrderDate(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd/MM/yyyy"));
            view.txtNamaToko.setText("" + item.getNamaToko());
            view.txtStatusKomplain.setText(item.getStatus());

            view.btnLihatKomplain.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(ListOrder item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<ListOrder> listItem) {
        for (ListOrder item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<ListOrder> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public ListOrder getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<ListOrder> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<ListOrder> getListItem() {
        return listItem;
    }
}
