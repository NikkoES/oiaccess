
package com.oiaccess.app.ui.model.courir;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Kurir {

    @SerializedName("code")
    private String mCode;
    @SerializedName("logo")
    private String mLogo;
    @SerializedName("name")
    private String mName;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getLogo() {
        return mLogo;
    }

    public void setLogo(String logo) {
        mLogo = logo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
