package com.oiaccess.app.ui.model.post;

import com.google.gson.annotations.SerializedName;

public class PostLogin {

    @SerializedName("phone")
    String phone;
    @SerializedName("password")
    String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
