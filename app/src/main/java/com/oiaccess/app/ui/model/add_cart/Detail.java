
package com.oiaccess.app.ui.model.add_cart;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Detail {

    @SerializedName("diskon")
    private String mDiskon;
    @SerializedName("grandtotal")
    private Long mGrandtotal;
    @SerializedName("harga_diskon")
    private Long mHargaDiskon;
    @SerializedName("harga_product")
    private Long mHargaProduct;
    @SerializedName("id_product")
    private String mIdProduct;
    @SerializedName("id_toko")
    private String mIdToko;
    @SerializedName("id_user")
    private String mIdUser;
    @SerializedName("jumlah_beli")
    private Long mJumlahBeli;
    @SerializedName("total")
    private Long mTotal;

    public String getDiskon() {
        return mDiskon;
    }

    public void setDiskon(String diskon) {
        mDiskon = diskon;
    }

    public Long getGrandtotal() {
        return mGrandtotal;
    }

    public void setGrandtotal(Long grandtotal) {
        mGrandtotal = grandtotal;
    }

    public Long getHargaDiskon() {
        return mHargaDiskon;
    }

    public void setHargaDiskon(Long hargaDiskon) {
        mHargaDiskon = hargaDiskon;
    }

    public Long getHargaProduct() {
        return mHargaProduct;
    }

    public void setHargaProduct(Long hargaProduct) {
        mHargaProduct = hargaProduct;
    }

    public String getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(String idProduct) {
        mIdProduct = idProduct;
    }

    public String getIdToko() {
        return mIdToko;
    }

    public void setIdToko(String idToko) {
        mIdToko = idToko;
    }

    public String getIdUser() {
        return mIdUser;
    }

    public void setIdUser(String idUser) {
        mIdUser = idUser;
    }

    public Long getJumlahBeli() {
        return mJumlahBeli;
    }

    public void setJumlahBeli(Long jumlahBeli) {
        mJumlahBeli = jumlahBeli;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

}
