package com.oiaccess.app.ui.activity.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.CartAdapter;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.cart.Cart;
import com.oiaccess.app.ui.model.cart.CartResponse;
import com.oiaccess.app.ui.model.cart.Detail;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityOrder;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_CART;


public class CartActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_total_harga)
    TextView txtTotalHarga;

    BoxStore boxStore;
    Box<EntityCart> cartBox;
    Box<EntityOrder> orderBox;
    Box<EntityItem> itemBox;

    Long totalPrice = (long) 0;

    List<Cart> listCart = new ArrayList<>();

    CartAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        boxStore = MyApplication.getBoxStore();
        cartBox = boxStore.boxFor(EntityCart.class);
        orderBox = boxStore.boxFor(EntityOrder.class);
        itemBox = boxStore.boxFor(EntityItem.class);

        initView();
    }

    private void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CartAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new CartAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onUpdateItem(long price) {
                Log.e("subtotal", "" + price);
                totalPrice = price;
                txtTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(totalPrice)));
            }

            @Override
            public void onDeleteItem(int position) {
                loadItems();
            }
        });

        loadItems();
    }

    private void loadItems() {
        AndroidNetworking.get(CATEGORY_CART + new Session(this).getUser().getUserId())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(CartResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CartResponse) {
                            CartResponse response1 = (CartResponse) response;
                            if (response1.getResult().size() > 0) {
                                totalPrice = (long) 0;
                                listCart = response1.getResult();
                                for (int i = 0; i < listCart.size(); i++) {
                                    totalPrice = totalPrice + listCart.get(i).getGrandtotal();
                                }
                                txtTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(totalPrice)));
                                adapter.swap(listCart);
                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        txtTotalHarga.setText("Rp. 0");
                        Toast.makeText(CartActivity.this, "Anda belum mengisi keranjang !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick({R.id.closedetailbeli, R.id.btn_beli_sekarang})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.closedetailbeli:
                finish();
                break;
            case R.id.btn_beli_sekarang:
                if (listCart.size() > 0) {
                    orderBox.removeAll();
                    itemBox.removeAll();

                    List<Cart> listCartNew = adapter.getListItem();

                    for (int i = 0; i < listCartNew.size(); i++) {
                        Cart cart = listCartNew.get(i);

                        EntityOrder order = new EntityOrder();
                        order.mIdToko = cart.getIdToko();
                        order.mIdCart = cart.getIdCart();
                        order.mGrandTotal = cart.getGrandtotal();
                        order.mNamaToko = cart.getStoreName();

                        Long weight = (long) 0;

                        for (int j = 0; j < cart.getDetails().size(); j++) {
                            Detail detail = cart.getDetails().get(j);

                            EntityItem item = new EntityItem();
                            item.mIdProduct = detail.getIdProduct();
                            item.mJumlah = detail.getJumlah();
                            item.mTotal = detail.getTotal();
                            item.mPrice = detail.getHarga() - detail.getHargaDiskon();
                            item.mWeight = detail.getWeight();
                            item.mImageProduct = detail.getImageName();
                            item.mNamaProduct = detail.getNamaProduct();
                            weight = weight + detail.getWeight();

                            order.listItem.add(item);
                        }
                        order.mTotalWeight = weight;

                        orderBox.put(order);
                    }

                    List<EntityOrder> list = orderBox.getAll();

                    Log.e("LIST", "" + list.size());
                    Log.e("LIST ITEM", "" + list.get(0).listItem.size());

                    startActivity(new Intent(this, CheckoutActivity.class));
                } else {
                    Toast.makeText(this, "Anda belum mengisi keranjang !", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
