package com.oiaccess.app.ui.fragment.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oiaccess.app.R;
import com.oiaccess.app.ui.fragment.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment {

    View view;


    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_help, container, false);


        return view;
    }

}
