package com.oiaccess.app.ui.activity.transaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.Gson;
import com.oiaccess.app.Payment_Virtual_Account;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.MainActivity;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.balance.BalanceResponse;
import com.oiaccess.app.ui.model.delete_cart.DeleteCartResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityOrder;
import com.oiaccess.app.ui.model.order.OrderResponse;
import com.oiaccess.app.ui.model.post_checkout.Checkout;
import com.oiaccess.app.ui.model.post_checkout.Courier;
import com.oiaccess.app.ui.model.post_checkout.Item;
import com.oiaccess.app.ui.model.post_checkout.Order;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_USER_PROFILE_WALLET;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_CART;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;

public class PaymentMethodActivity extends AppCompatActivity {

    @BindView(R.id.txt_total_tagihan)
    TextView txtTotalTagihan;
    @BindView(R.id.txt_total_balance)
    TextView txtTotalBalance;

    BoxStore boxStore;
    Box<EntityOrder> orderBox;
    Box<EntityItem> itemBox;
    Box<EntityCart> cartBox;

    Long balance;

    List<Order> listOrder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        ButterKnife.bind(this);

        boxStore = MyApplication.getBoxStore();
        orderBox = boxStore.boxFor(EntityOrder.class);
        itemBox = boxStore.boxFor(EntityItem.class);
        cartBox = boxStore.boxFor(EntityCart.class);

        initView();
    }

    private void initView() {
        DialogUtils.openDialog(this);

        List<EntityOrder> list = orderBox.getAll();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                EntityOrder order = list.get(i);

                Log.e("mIdToko", "" + order.mIdToko);
                Log.e("mGrandTotal", "" + order.mGrandTotal);
                Log.e("mAddress", "" + order.mAddress);
                Log.e("mVendor", "" + order.mVendor);
                Log.e("mCost", "" + order.mCost);
                Log.e("mOrigin", "" + order.mOrigin);
                Log.e("mDestination", "" + order.mDestination);
                Log.e("mPaymentMethod", "" + order.mPaymentMethod);
                Log.e("mItem", "" + order.listItem.size());

                Order oOrder = new Order();
                oOrder.setIdToko(order.mIdToko);
                oOrder.setGrandTotal(order.mGrandTotal);
                oOrder.setAddress(order.mAddress);

                Courier courier = new Courier();
                courier.setVendor(order.mVendor);
                courier.setCost(order.mCost);
                courier.setOrigin(order.mOrigin);
                courier.setDestination(order.mDestination);
                oOrder.setCourier(courier);

                oOrder.setPaymentMethod(order.mPaymentMethod);

                List<Item> itemList = new ArrayList<>();
                for (int j = 0; j < order.listItem.size(); j++) {
                    EntityItem item = order.listItem.get(j);

                    Log.e("mIdProduct", "" + item.mIdProduct);
                    Log.e("mJumlah", "" + item.mJumlah);
                    Log.e("mTotal", "" + item.mTotal);

                    Item iItem = new Item();
                    iItem.setIdProduct(item.mIdProduct);
                    iItem.setJumlah(item.mJumlah);
                    iItem.setTotal(item.mTotal);
                    itemList.add(iItem);
                }
                oOrder.setItems(itemList);

                listOrder.add(oOrder);
            }
        }

        txtTotalTagihan.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(getIntent().getLongExtra("total", 0))));

        AndroidNetworking.get(API_USER_PROFILE_WALLET)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(BalanceResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof BalanceResponse) {
                            BalanceResponse response1 = (BalanceResponse) response;
                            balance = response1.getResult().getBalance();
                            txtTotalBalance.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(balance)));
                            DialogUtils.closeDialog();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(PaymentMethodActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick({R.id.btn_back, R.id.btn_bayar_dengan_wallet, R.id.btn_va})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.btn_bayar_dengan_wallet:
                DialogUtils.dialogYesNo(this, "Anda yakin akan bayar menggunakan wallet ?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (balance >= getIntent().getLongExtra("total", 0)) {
                            createOrderWallet();
                        } else {
                            Toast.makeText(PaymentMethodActivity.this, "Saldo wallet Anda tidak mencukupi !", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.btn_va:
                DialogUtils.dialogYesNo(this, "Anda yakin akan bayar menggunakan virtual account ?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //createOrderVa();
                        Toast.makeText(PaymentMethodActivity.this, "Sedang dalam pengembangan !", Toast.LENGTH_SHORT).show();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
        }
    }

    private void createOrderWallet() {
        DialogUtils.openDialog(this);

        for (int i = 0; i < listOrder.size(); i++) {
            Order order = listOrder.get(i);
            order.setPaymentMethod("Wallet");
            listOrder.set(i, order);
        }

        Checkout checkout = new Checkout();
        checkout.setOrders(listOrder);

        Gson gson = new Gson();
        String json = gson.toJson(checkout);

        Log.e("JSON", "" + json);

        AndroidNetworking.post(CATEGORY_ORDER)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addApplicationJsonBody(checkout)
                .build()
                .getAsObject(OrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof OrderResponse) {
                            OrderResponse response1 = (OrderResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                Toast.makeText(PaymentMethodActivity.this, "Order Selesai !", Toast.LENGTH_SHORT).show();
                                clearCart();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(PaymentMethodActivity.this, "Gagal Order Barang !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(PaymentMethodActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void createOrderVa() {
        DialogUtils.openDialog(this);

        Intent paymentbni = new Intent(PaymentMethodActivity.this, Payment_Virtual_Account.class);
        startActivity(paymentbni);

        for (int i = 0; i < listOrder.size(); i++) {
            Order order = listOrder.get(i);
            order.setPaymentMethod("va");
            listOrder.set(i, order);
        }

        Checkout checkout = new Checkout();
        checkout.setOrders(listOrder);

        Gson gson = new Gson();
        String json = gson.toJson(checkout);

        Log.e("JSON", "" + json);

        AndroidNetworking.post(CATEGORY_ORDER)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addApplicationJsonBody(checkout)
                .build()
                .getAsObject(OrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof OrderResponse) {
                            OrderResponse response1 = (OrderResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                clearCart();
                            } else {
                                DialogUtils.closeDialog();
                                Toast.makeText(PaymentMethodActivity.this, "Gagal Order Barang !", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(PaymentMethodActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void clearCart() {
        List<EntityOrder> list = orderBox.getAll();
        for (int i = 0; i < list.size(); i++) {
            EntityOrder order = list.get(i);
            long idCart = order.mIdCart;
            if (order.listItem.size() > 1) {
                for (int j = 0; j < order.listItem.size(); j++) {
                    deleteCart(idCart);
                }
            } else {
                deleteCart(idCart);
            }
        }

        if (list.size() == 0) {
            orderBox.removeAll();
            itemBox.removeAll();
            cartBox.removeAll();
        }
        Toast.makeText(PaymentMethodActivity.this, "Berhasil Order Barang..", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(PaymentMethodActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private void deleteCart(long idCart) {
        AndroidNetworking.delete(CATEGORY_CART + idCart)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(DeleteCartResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof DeleteCartResponse) {
                            DeleteCartResponse response1 = (DeleteCartResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {

                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                    }
                });

    }
}
