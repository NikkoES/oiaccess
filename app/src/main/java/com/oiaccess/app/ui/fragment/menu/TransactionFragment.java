package com.oiaccess.app.ui.fragment.menu;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.Menunggu_Pembayaran;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.order.KomplainListActivity;
import com.oiaccess.app.ui.activity.order.OrderListActivity;
import com.oiaccess.app.ui.activity.product.ProductDetailActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.count_status.CountStatus;
import com.oiaccess.app.ui.model.count_status.CountStatusResponse;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_CATEGORY;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLAIN;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_RATING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_REJECTED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_WAITING_PAYMENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionFragment extends Fragment {

    View view;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;

    @BindView(R.id.notif_waiting_payment)
    TextView notifWaitingPayment;
    @BindView(R.id.notif_waiting_confirmation)
    TextView notifWaitingConfirmation;
    @BindView(R.id.notif_on_process)
    TextView notifOnProcess;
    @BindView(R.id.notif_on_shipping)
    TextView notifOnShipping;
    @BindView(R.id.notif_need_rating)
    TextView notifNeedRating;
    @BindView(R.id.notif_complete)
    TextView notifComplete;
    @BindView(R.id.notif_rejected)
    TextView notifRejected;
    @BindView(R.id.notif_complained)
    TextView notifComplained;

    public TransactionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_transaction, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadItem();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            loadItem();
        }
    }

    private void loadItem() {
        AndroidNetworking.get(CATEGORY_ORDER)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .addQueryParameter("id_user", new Session(getActivity()).getUser().getUserId())
                .build()
                .getAsObject(CountStatusResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CountStatusResponse) {
                            CountStatusResponse response1 = (CountStatusResponse) response;
                            CountStatus countStatus = response1.getResult().get(0);
                            notifWaitingPayment.setVisibility(View.GONE);
                            notifWaitingConfirmation.setVisibility(View.GONE);
                            notifOnProcess.setVisibility(View.GONE);
                            notifOnShipping.setVisibility(View.GONE);
                            notifComplete.setVisibility(View.GONE);
                            notifRejected.setVisibility(View.GONE);
                            notifComplained.setVisibility(View.GONE);
                            notifNeedRating.setVisibility(View.GONE);
                            if (countStatus.getWaitingpayment() > 0) {
                                notifWaitingPayment.setText("" + countStatus.getWaitingpayment());
                                notifWaitingPayment.setVisibility(View.VISIBLE);
                            }
                            if (countStatus.getmNew() > 0) {
                                notifWaitingConfirmation.setText("" + countStatus.getmNew());
                                notifWaitingConfirmation.setVisibility(View.VISIBLE);
                            }
                            if (countStatus.getOnprocess() > 0) {
                                notifOnProcess.setText("" + countStatus.getOnprocess());
                                notifOnProcess.setVisibility(View.VISIBLE);
                            }
                            if (countStatus.getOnshipping() > 0) {
                                notifOnShipping.setText("" + countStatus.getOnshipping());
                                notifOnShipping.setVisibility(View.VISIBLE);
                            }
                            if (countStatus.getNeedrating() > 0) {
                                notifNeedRating.setText("" + countStatus.getNeedrating());
                                notifNeedRating.setVisibility(View.VISIBLE);
                            }
                            if (countStatus.getCompleted() > 0) {
                                notifComplete.setText("" + countStatus.getCompleted());
                                notifComplete.setVisibility(View.GONE);
                            }
                            if (countStatus.getRejected() > 0) {
                                notifRejected.setText("" + countStatus.getRejected());
                                notifRejected.setVisibility(View.GONE);
                            }
                            if (countStatus.getComplained() > 0) {
                                notifComplained.setText("" + countStatus.getComplained());
                                notifComplained.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.waitingForPaymentButton, R.id.waitingForConfirmationButton, R.id.orderProccessButton, R.id.orderDeliveredButton, R.id.orderNeedRatingButton, R.id.orderFinishedButton, R.id.rejectedOrderButton, R.id.transaksikomplain})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.waitingForPaymentButton:
                openActivitypayment(ORDER_STATUS_WAITING_PAYMENT);
                break;
            case R.id.waitingForConfirmationButton:
                openActivity(ORDER_STATUS_NEW);
                break;
            case R.id.orderProccessButton:
                openActivity(ORDER_STATUS_ON_PROCESS);
                break;
            case R.id.orderDeliveredButton:
                openActivity(ORDER_STATUS_ON_SHIPPING);
                break;
            case R.id.orderNeedRatingButton:
                openActivity(ORDER_STATUS_RATING);
                break;
            case R.id.orderFinishedButton:
                openActivity(ORDER_STATUS_COMPLETED);
                break;
            case R.id.rejectedOrderButton:
                openActivity(ORDER_STATUS_REJECTED);
                break;
            case R.id.transaksikomplain:
                openActivity2(ORDER_STATUS_COMPLAIN);
                break;
        }

    }

    private void openActivitypayment(String status) {
        Intent intent = new Intent(getActivity(), Menunggu_Pembayaran.class);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    private void openActivity(String status) {
        Intent intent = new Intent(getActivity(), OrderListActivity.class);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    private void openActivity2(String status) {
        Intent intent = new Intent(getActivity(), KomplainListActivity.class);
        intent.putExtra("status", status);
        startActivity(intent);
    }

}

