
package com.oiaccess.app.ui.model.detail_product;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unused")
public class DetailProduct implements Serializable {

    @SerializedName("category_name")
    private String mCategoryName;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("diskusi")
    private Object mDiskusi;
    @SerializedName("harga")
    private Long mHarga;
    @SerializedName("harga_diskon")
    private Long mHargaDiskon;
    @SerializedName("id_category")
    private Long mIdCategory;
    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("image_name")
    private String mImageName;
    @SerializedName("images")
    private List<Image> mImages;
    @SerializedName("nama_product")
    private String mNamaProduct;
    @SerializedName("nama_toko")
    private String mNamaToko;
    @SerializedName("product_sold")
    private Long mProductSold;
    @SerializedName("seen")
    private Long mSeen;
    @SerializedName("stock")
    private Long mStock;
    @SerializedName("weight")
    private Long mWeight;
    @SerializedName("kondisi")
    private String kondisi;
    @SerializedName("min_order")
    private Long mMinOrder;
    @SerializedName("ratings_product")
    private String mRatingsProduct;


    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String categoryName) {
        mCategoryName = categoryName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Object getDiskusi() {
        return mDiskusi;
    }

    public void setDiskusi(Object diskusi) {
        mDiskusi = diskusi;
    }

    public Long getHarga() {
        return mHarga;
    }

    public void setHarga(Long harga) {
        mHarga = harga;
    }

    public Long getHargaDiskon() {
        return mHargaDiskon;
    }

    public void setHargaDiskon(Long hargaDiskon) {
        mHargaDiskon = hargaDiskon;
    }

    public Long getIdCategory() {
        return mIdCategory;
    }

    public void setIdCategory(Long idCategory) {
        mIdCategory = idCategory;
    }

    public Long getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(Long idProduct) {
        mIdProduct = idProduct;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public String getImageName() {
        return mImageName;
    }

    public void setImageName(String imageName) {
        mImageName = imageName;
    }

    public List<Image> getImages() {
        return mImages;
    }

    public void setImages(List<Image> images) {
        mImages = images;
    }

    public String getNamaProduct() {
        return mNamaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        mNamaProduct = namaProduct;
    }

    public String getNamaToko() {
        return mNamaToko;
    }

    public void setNamaToko(String namaToko) {
        mNamaToko = namaToko;
    }

    public Long getProductSold() {
        return mProductSold;
    }

    public void setProductSold(Long productSold) {
        mProductSold = productSold;
    }

    public Long getSeen() {
        return mSeen;
    }

    public void setSeen(Long seen) {
        mSeen = seen;
    }

    public Long getStock() {
        return mStock;
    }

    public void setStock(Long stock) {
        mStock = stock;
    }

    public Long getWeight() {
        return mWeight;
    }

    public void setWeight(Long weight) {
        mWeight = weight;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public Long getmMinOrder() {
        return mMinOrder;
    }

    public void setmMinOrder(Long mMinOrder) {
        this.mMinOrder = mMinOrder;
    }

    public String getmRatingsProduct() {
        return mRatingsProduct;
    }

    public void setmRatingsProduct(String mRatingsProduct) {
        this.mRatingsProduct = mRatingsProduct;
    }
}
