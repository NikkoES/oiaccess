
package com.oiaccess.app.ui.model.detail_order;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DetailOrderResponse {

    @SerializedName("result")
    private DetailOrder mDetailOrder;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public DetailOrder getResult() {
        return mDetailOrder;
    }

    public void setResult(DetailOrder detailOrder) {
        mDetailOrder = detailOrder;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
