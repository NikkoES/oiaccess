
package com.oiaccess.app.ui.model.detail_order;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Order {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("courier")
    private String mCourier;
    @SerializedName("courier_cost")
    private Long mCourierCost;
    @SerializedName("courier_name")
    private String mCourierName;
    @SerializedName("destination")
    private String mDestination;
    @SerializedName("id_order")
    private Long mIdOrder;
    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("id_user")
    private Long mIdUser;
    @SerializedName("order_date")
    private String mOrderDate;
    @SerializedName("origin")
    private String mOrigin;
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("resi")
    private Object mResi;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("store_name")
    private String storeName;
    @SerializedName("buyer_name")
    private String buyerName;
    @SerializedName("total_harga")
    private Long mTotalHarga;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCourier() {
        return mCourier;
    }

    public void setCourier(String courier) {
        mCourier = courier;
    }

    public Long getCourierCost() {
        return mCourierCost;
    }

    public void setCourierCost(Long courierCost) {
        mCourierCost = courierCost;
    }

    public String getCourierName() {
        return mCourierName;
    }

    public void setCourierName(String courierName) {
        mCourierName = courierName;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public Long getIdOrder() {
        return mIdOrder;
    }

    public void setIdOrder(Long idOrder) {
        mIdOrder = idOrder;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public Long getIdUser() {
        return mIdUser;
    }

    public void setIdUser(Long idUser) {
        mIdUser = idUser;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String orderDate) {
        mOrderDate = orderDate;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public Object getResi() {
        return mResi;
    }

    public void setResi(Object resi) {
        mResi = resi;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Long getTotalHarga() {
        return mTotalHarga;
    }

    public void setTotalHarga(Long totalHarga) {
        mTotalHarga = totalHarga;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }
}
