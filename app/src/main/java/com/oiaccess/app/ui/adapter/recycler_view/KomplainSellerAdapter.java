package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.komplain.Chat;
import com.oiaccess.app.ui.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class KomplainSellerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    CardView tes;
    List<Chat> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }


    public KomplainSellerAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_him)
        CircularImageView imageHim;
        @BindView(R.id.txt_nama_him)
        TextView txtNamaHim;
        @BindView(R.id.txt_waktu_him)
        TextView txtWaktuHim;
        @BindView(R.id.txt_isi_him)
        TextView txtIsiHim;
        @BindView(R.id.him)
        LinearLayout him;
        @BindView(R.id.txt_nama_me)
        TextView txtNamaMe;
        @BindView(R.id.txt_waktu_me)
        TextView txtWaktuMe;
        @BindView(R.id.txt_isi_me)
        TextView txtIsiMe;
        @BindView(R.id.image_me)
        CircularImageView imageMe;
        @BindView(R.id.me)
        LinearLayout me;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            Chat item = listItem.get(position);

            if (item.getSender().equalsIgnoreCase("buyer")) {
                view.him.setVisibility(View.VISIBLE);
                Glide.with(ctx).load("").into(view.imageHim);
                view.txtNamaHim.setText(item.getSender());
                view.txtIsiHim.setText(item.getText());
                view.txtWaktuHim.setText(CommonUtil.generalFormatDate(item.getChatCreated(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd/MM/yyyy"));
            } else {
                view.me.setVisibility(View.VISIBLE);
                Glide.with(ctx).load("").into(view.imageMe);
                view.txtNamaMe.setText(item.getSender());
                view.txtIsiMe.setText(item.getText());
                view.txtWaktuMe.setText(CommonUtil.generalFormatDate(item.getChatCreated(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd MMM yyyy - HH:mm"));
            }

            view.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(Chat item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<Chat> listItem) {
        for (Chat item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<Chat> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public Chat getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<Chat> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<Chat> getListItem() {
        return listItem;
    }
}
