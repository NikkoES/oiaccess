package com.oiaccess.app.ui.model.rating;

import com.google.gson.annotations.SerializedName;

public class ListRating {
    @SerializedName("id_toko")
    private Long mIdStore;
    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("rating")
    private String mRating;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("buyer_name")
    private String mBuyerName;

    public Long getmIdStore() {
        return mIdStore;
    }

    public void setmIdStore(Long mIdStore) {
        this.mIdStore = mIdStore;
    }

    public Long getmIdProduct() {
        return mIdProduct;
    }

    public void setmIdProduct(Long mIdProduct) {
        this.mIdProduct = mIdProduct;
    }

    public String getmRating() {
        return mRating;
    }

    public void setmRating(String mRating) {
        this.mRating = mRating;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmBuyerName() {
        return mBuyerName;
    }

    public void setmBuyerName(String mBuyerName) {
        this.mBuyerName = mBuyerName;
    }
}

