package com.oiaccess.app.ui.utils;

import android.content.Context;
import android.util.Log;

import com.oiaccess.app.model.MajaKategori;

import java.io.OutputStreamWriter;

public class FileHelper {

    public static void saveKategoriModelStr(String kategoriModelStr,Context context){
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput("kategorimodel.txt", Context.MODE_PRIVATE));
            osw.write(kategoriModelStr);
            osw.close();
        }
        catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    public static void removeKategoriModelFile(Context context){
        try {
            context.deleteFile("kategorimodel.txt");
        }catch (Exception e){}
    }

    public static MajaKategori getKategoriModelFromFile(Context context){
        Log.d("Valdo Mobile","Get kategorimodel from file");
        Log.i("Valdo Mobile","Get kategorimodel from file");
        String ret = "{\"total\":8,\"data\":[{\"id_category\":1,\"category_name\":\"ELEKTRONIK\"},{\"id_category\":2,\"category_name\":\"FOOD & DRINK\"},{\"id_category\":3,\"category_name\":\"DIGITAL\"},{\"id_category\":4,\"category_name\":\"BEAUTY\"},{\"id_category\":5,\"category_name\":\"FASHION\"},{\"id_category\":6,\"category_name\":\"HOBBY\"},{\"id_category\":7,\"category_name\":\"LIVING\"},{\"id_category\":8,\"category_name\":\"KITCHEN\"}]}";

        Log.d("Valdo Mobile","kategorimodel:"+ret);
        Log.i("Valdo Mobile","kategorimodel:"+ret);
        if(ret!=null){
            return Util.mapResponseToKategoriModel(ret);
        }else {
            return null;
        }
    }
}
