package com.oiaccess.app.ui.activity.buyer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.product.SearchProductActivity;
import com.oiaccess.app.ui.activity.transaction.CartActivity;
import com.oiaccess.app.ui.adapter.SectionsPageAdapter;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.fragment.store.Tab_Info;
import com.oiaccess.app.ui.fragment.store.Tab_Produk;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_NAME;
import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_STORE;


public class ViewStoreActivity extends AppCompatActivity {
    private static final String TAG = "ViewStoreActivity";
    private SectionsPageAdapter mSectionsPageAdapter;
    private ViewPager nViewPager;
    @BindView(R.id.penjual)
    TextView penjual;
    @BindView(R.id.cartBtn)
    FrameLayout cartBtn;
    @BindView(R.id.cart_badge)
    TextView cartBadge;
    @BindView(R.id.searchET)
    EditText etSearch;


    BoxStore boxStore;
    Box<EntityCart> orderBox;

    int mCartItemCount = 0;

    private Tab_Produk tabProduk;

    List<Produk> listProduk = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__store);
        ButterKnife.bind(this);
        tabProduk = new Tab_Produk();
       // Log.d(TAG, "onCreate: Starting.");

//        NestedScrollView scrollView = (NestedScrollView) findViewById (R.id.nest_scrollview);
//        scrollView.setFillViewport (true);

        boxStore = MyApplication.getBoxStore();
        orderBox = boxStore.boxFor(EntityCart.class);
        setupBadge();
        loadProducts();

        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        nViewPager = (ViewPager)findViewById(R.id.container);
        setupViewPager(nViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.mytab);
        tabLayout.setupWithViewPager(nViewPager);

        final CircularImageView image = (CircularImageView) findViewById(R.id.image);
        String storeName = getIntent().getStringExtra("storeName");
        penjual.setText(storeName);

        final CollapsingToolbarLayout collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        ((AppBarLayout) findViewById(R.id.appbar)).addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout,int verticalOffset) {
                int min_height = ViewCompat.getMinimumHeight(collapsing_toolbar) * 2;
                float scale = (float) (min_height + verticalOffset) / min_height;
                image.setScaleX(scale >= 0 ? scale : 0);
                image.setScaleY(scale >= 0 ? scale : 0);

                penjual.setScaleX(scale >= 0 ? scale : 0);
                penjual.setScaleY(scale >= 0 ? scale : 0);

            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loadSearch(s.toString());
            }
        });

    }

    public void loadSearch(String q){
        if(q.equals("")){
            loadProducts();
        }else {
            List<Produk> search = new ArrayList<>();
            for (Produk p: listProduk
                 ) {
                if(p.getNamaProduct().toLowerCase().contains(q.toLowerCase())){
                    search.add(p);
                }
            }
            tabProduk.getProdukAdapter().swap(search);
        }
    }

    public void loadProducts(){
        Long idStore = getIntent().getLongExtra("idStore",0);

        AndroidNetworking.get(API_PRODUCT_BY_STORE+idStore.toString())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProductResponse) {
                            ProductResponse response1 = (ProductResponse) response;
                            listProduk.clear();
                            listProduk = response1.getResult();
                            tabProduk.getProdukAdapter().swap(listProduk);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(getApplicationContext(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(tabProduk,"Produk");
        adapter.addFragment(new Tab_Info(),"Info");
        viewPager.setAdapter(adapter);
    }

    private void setupBadge() {
        List<EntityCart> list = orderBox.getAll();
        mCartItemCount = list.size();

        if (cartBadge != null) {
            if (mCartItemCount == 0) {
                if (cartBadge.getVisibility() != View.GONE) {
                    cartBadge.setVisibility(View.GONE);
                }
            } else {
                Log.e("ITEM CART", "" + mCartItemCount);
                cartBadge.setText(String.valueOf(mCartItemCount));
                if (cartBadge.getVisibility() != View.VISIBLE) {
                    cartBadge.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @OnClick({ R.id.cartBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cartBtn:
                Intent intentCart = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intentCart);
                break;
        }
    }

    public void tes(View v) {
        finish();
    }
}


