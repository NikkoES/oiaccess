
package com.oiaccess.app.ui.model.product;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ProductResponse {

    @SerializedName("result")
    private List<Produk> mProduk;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;
    @SerializedName("total")
    private Long mTotal;

    public List<Produk> getResult() {
        return mProduk;
    }

    public void setResult(List<Produk> produk) {
        mProduk = produk;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

}
