package com.oiaccess.app.ui.fragment.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.Total_Rating_Toko;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.rating.StoreRating;
import com.oiaccess.app.ui.model.rating.StoreRatingResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_CATEGORY;
import static com.oiaccess.app.ui.data.Constants.LIST_RATINGS_BY_STORE;

public class Tab_Info extends Fragment {
    private static final String TAG = "Tab_Info";

    @BindView(R.id.progress_bar1)
    ProgressBar progressBar1;
    @BindView(R.id.txt1)
    TextView txt1;
    @BindView(R.id.progress_bar2)
    ProgressBar progressBar2;
    @BindView(R.id.txt2)
    TextView txt2;
    @BindView(R.id.progress_bar3)
    ProgressBar progressBar3;
    @BindView(R.id.txt3)
    TextView txt3;
    @BindView(R.id.progress_bar4)
    ProgressBar progressBar4;
    @BindView(R.id.txt4)
    TextView txt4;
    @BindView(R.id.progress_bar5)
    ProgressBar progressBar5;
    @BindView(R.id.txt5)
    TextView txt5;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.info_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        loadItems();
        return view;
    }

    private void loadItems() {
        Long idStore = getActivity().getIntent().getLongExtra("idStore",0);
        AndroidNetworking.get(LIST_RATINGS_BY_STORE + idStore)
                .addHeaders("X-AUTH-TOKEN", new Session(getContext()).getUser().getAccessToken())
                .build()
                .getAsObject(StoreRatingResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof StoreRatingResponse) {
                            StoreRatingResponse response1 = (StoreRatingResponse) response;
                            int max = 1;
                            for (StoreRating sr: response1.getStoreRatingList()
                                    ) {
                                if(sr.getmTotal()>max){
                                    max=(int)Integer.parseInt(sr.getmTotal().toString());
                                }
                                progressBar1.setMax(max);
                                progressBar2.setMax(max);
                                progressBar3.setMax(max);
                                progressBar4.setMax(max);
                                progressBar5.setMax(max);
                                if(sr.getmRating().equals("1")){
                                    txt1.setText(sr.getmTotal().toString());
                                    progressBar1.setProgress((int)Integer.parseInt(sr.getmTotal().toString()));
                                }else if(sr.getmRating().equals("2")){
                                    txt2.setText(sr.getmTotal().toString());
                                    progressBar2.setProgress((int)Integer.parseInt(sr.getmTotal().toString()));
                                }else if(sr.getmRating().equals("3")){
                                    txt3.setText(sr.getmTotal().toString());
                                    progressBar3.setProgress((int)Integer.parseInt(sr.getmTotal().toString()));
                                }else if(sr.getmRating().equals("4")){
                                    txt4.setText(sr.getmTotal().toString());
                                    progressBar4.setProgress((int)Integer.parseInt(sr.getmTotal().toString()));
                                }else if(sr.getmRating().equals("5")){
                                    txt5.setText(sr.getmTotal().toString());
                                    progressBar5.setProgress((int)Integer.parseInt(sr.getmTotal().toString()));
                                }

                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(getContext(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        TextView nameView = (TextView) view.findViewById(R.id.lihattotalrating);
//
//        nameView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), Total_Rating_Toko.class);
//                startActivity(intent);
//            }
//        });
//    }
}
