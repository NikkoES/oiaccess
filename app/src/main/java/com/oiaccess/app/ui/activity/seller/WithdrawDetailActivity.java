package com.oiaccess.app.ui.activity.seller;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.oiaccess.app.R;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WithdrawDetailActivity extends AppCompatActivity {

    @BindView(R.id.valdo_txtHistoryDetailAmountTotal)
    TextView valdoTxtHistoryDetailAmountTotal;
    @BindView(R.id.valdo_edtAmount)
    EditText valdoEdtAmount;
    @BindView(R.id.valdo_bank)
    EditText valdoBank;
    Long balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_detail);
        ButterKnife.bind(this);
        initView();
    }

    public void initView(){
        balance = getIntent().getLongExtra("balance",0);
        valdoTxtHistoryDetailAmountTotal.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(balance)));
    }

    @OnClick({R.id.backBtn, R.id.valdo_bank, R.id.withdrawNowBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.valdo_bank:
                final String[] items = new String[]{"Bank BRI", "Bank Mandiri", "Bank BCA"};
                DialogUtils.dialogArray(this, items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        valdoBank.setText(items[which]);
                    }
                });
                break;
            case R.id.withdrawNowBtn:
                final Dialog dialog = DialogUtils.dialogLayout(this, Window.FEATURE_NO_TITLE, R.layout.dialog_withdraw_status);
                Button buttonDialog = dialog.findViewById(R.id.valdo_btnWithdrawClose);
                buttonDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });
                break;
        }
    }
}
