
package com.oiaccess.app.ui.model.create_product;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CreateProductResponse {

    @SerializedName("result")
    private CreateProduct mCreateProduct;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public CreateProduct getResult() {
        return mCreateProduct;
    }

    public void setResult(CreateProduct createProduct) {
        mCreateProduct = createProduct;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
