
package com.oiaccess.app.ui.model.balance;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class BalanceResponse {

    @SerializedName("result")
    private Balance mBalance;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public Balance getResult() {
        return mBalance;
    }

    public void setResult(Balance balance) {
        mBalance = balance;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
