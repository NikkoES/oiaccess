package com.oiaccess.app.ui.utils;

import android.content.Context;
import android.widget.ImageView;

public class ImageHelper {
	
	public static final String folderName = "folder";
	
	public static void setImageViewFromCamera(Context context, String cameraFilePath, String dstFilename, int rotationOfImage){
		ImageHelperAsyncTaskSaver asynctask = new ImageHelperAsyncTaskSaver(context, cameraFilePath, dstFilename, rotationOfImage);
		asynctask.execute();
	}

	public static void setImageViewFromFile(Context context, ImageView imageView, String srcFilename, ImageHelperListener listener){
		ImageHelperAsyncTaskLoader asynctask = new ImageHelperAsyncTaskLoader(context, imageView, srcFilename, listener);
		asynctask.execute();
	}
}
