package com.oiaccess.app.ui.activity.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.oiaccess.app.R;
import com.oiaccess.app.ui.utils.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WithdrawActivity extends AppCompatActivity {

    @BindView(R.id.valdo_txtSaldo)
    TextView valdoTxtSaldo;
    @BindView(R.id.valdo_txtJumlahSaldo)
    TextView valdoTxtJumlahSaldo;
    @BindView(R.id.valdo_rv_history)
    RecyclerView valdoRvHistory;
    @BindView(R.id.withdrawBtn)
    Button buttonWithdraw;
    Long balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        balance = getIntent().getLongExtra("balance",0);
        if(balance > 0){
            valdoTxtJumlahSaldo.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(balance)));
        }else{
            buttonWithdraw.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.backBtn, R.id.withdrawBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.withdrawBtn:
                Intent i = new Intent(this, WithdrawDetailActivity.class);
                i.putExtra("balance", balance);
                startActivity(i);
                break;
        }
    }
}
