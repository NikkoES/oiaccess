package com.oiaccess.app.ui.utils;

import com.oiaccess.app.model.MajaDataKategori;
import com.oiaccess.app.model.MajaDataKategoriList;
import com.oiaccess.app.model.MajaKategori;
import com.oiaccess.app.services.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Util {

//    public static boolean isProductModelExpired() {
//
//        if (Constants.kategori == null ||
//                Constants.kategori.getDataKategoriList() == null) {
//            return true;
//        } else {
//            return false;
//        }
//    }

    public static MajaKategori mapResponseToKategoriModel(String data) {
        try {
            MajaKategori kategorimodel = new MajaKategori();
            MajaDataKategoriList dataKategoriList = new MajaDataKategoriList();
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            List<MajaDataKategori> listKategori = new ArrayList<MajaDataKategori>();
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject job = jsonArray.getJSONObject(x);
                MajaDataKategori kate = new MajaDataKategori();
                kate.setId_category(job.getInt("id_category"));
                kate.setCategory_name(job.getString("category_name"));
                listKategori.add(kate);
            }
            dataKategoriList.setKategoris(listKategori);
            kategorimodel.setDataKategoriList(dataKategoriList);
            return kategorimodel;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
