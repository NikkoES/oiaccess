package com.oiaccess.app.ui.activity.product;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;


import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.RatingListAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.IsiRatingProduct;
import com.oiaccess.app.ui.model.rating.ListRating;
import com.oiaccess.app.ui.model.rating.ListRatingResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import static com.oiaccess.app.ui.data.Constants.LIST_RATINGS_BY_PRODUCT;

import java.util.ArrayList;
import java.util.List;

public class RatingProductActivity extends AppCompatActivity {


    List<ListRating> listRatings;
    RatingListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_product);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ulasan Rating");
        setTitleColor(R.color.white);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        adapter = new RatingListAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.myrecyele);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        loadItems();
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        Long idProduct = getIntent().getLongExtra("idProduct", 0);
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder<>(LIST_RATINGS_BY_PRODUCT + idProduct);
        getRequestBuilder
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ListRatingResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ListRatingResponse) {
                            DialogUtils.closeDialog();
                            ListRatingResponse response1 = (ListRatingResponse) response;
                            if (response1.getmTotal() != 0) {
                                listRatings = response1.getListRatingList();
                                adapter.swap(listRatings);
                            }

                        }
                    }


                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(RatingProductActivity.this, "Ulasan kosong !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

