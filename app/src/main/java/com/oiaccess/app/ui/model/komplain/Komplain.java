
package com.oiaccess.app.ui.model.komplain;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Komplain {

    @SerializedName("chats")
    private List<Chat> mChats;
    @SerializedName("created")
    private String mCreated;
    @SerializedName("id_komplain")
    private Long mIdKomplain;
    @SerializedName("id_order")
    private Long mIdOrder;
    @SerializedName("status")
    private String mStatus;

    public List<Chat> getChats() {
        return mChats;
    }

    public void setChats(List<Chat> chats) {
        mChats = chats;
    }

    public String getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
        mCreated = created;
    }

    public Long getIdKomplain() {
        return mIdKomplain;
    }

    public void setIdKomplain(Long idKomplain) {
        mIdKomplain = idKomplain;
    }

    public Long getIdOrder() {
        return mIdOrder;
    }

    public void setIdOrder(Long idOrder) {
        mIdOrder = idOrder;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
