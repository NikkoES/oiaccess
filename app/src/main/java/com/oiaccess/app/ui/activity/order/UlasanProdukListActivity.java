package com.oiaccess.app.ui.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.UlasanAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.detail_order.Detail;
import com.oiaccess.app.ui.model.detail_order.DetailOrder;
import com.oiaccess.app.ui.model.detail_order.DetailOrderResponse;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;

public class UlasanProdukListActivity extends AppCompatActivity {

    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;
    @BindView(R.id.txt_tanggal)
    TextView txtTanggal;
    @BindView(R.id.txt_id_order)
    TextView txtIdOrder;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    UlasanAdapter adapter;

    DetailOrder detailOrder;

    String idOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_ulas_barang);
        ButterKnife.bind(this);

        idOrder = getIntent().getStringExtra("id_order");

        headerTitleTV.setText("Daftar Ulasan Produk");

//        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initView();
    }

    private void initView() {
        DialogUtils.openDialog(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UlasanAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new UlasanAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent i = new Intent(UlasanProdukListActivity.this, UlasanProdukActivity.class);
                i.putExtra("item", adapter.getItem(position));
                i.putExtra("id_toko", String.valueOf(detailOrder.getOrder().getIdToko()));
                i.putExtra("id_order", String.valueOf(detailOrder.getOrder().getIdOrder()));
                startActivity(i);
            }
        });
        AndroidNetworking.get(CATEGORY_ORDER + "/" + idOrder)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(DetailOrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        DetailOrderResponse detailOrderResponse = (DetailOrderResponse) response;
                        detailOrder = detailOrderResponse.getResult();
                        txtIdOrder.setText("#" + detailOrder.getOrder().getIdOrder());
                        txtTanggal.setText(CommonUtil.generalFormatDate(detailOrder.getOrder().getOrderDate(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd/MM/yyyy"));
                        List<Detail> ls = new ArrayList<>();
                        for (Detail d: detailOrder.getDetails()
                             ) {
                            if(d.getmIsRating().toString().equals("0")){
                                ls.add(d);
                            }

                        }
                        adapter.swap(ls);
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(UlasanProdukListActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
