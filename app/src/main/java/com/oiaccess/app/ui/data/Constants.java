package com.oiaccess.app.ui.data;

public class Constants {
    public static final String APP_NAME = "Oi ACCESS";
    public static final String APK_VER = "1"; //changedate 07 OCT 2018 18:08
    public static final String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=com.oiaccess.app";
    public static final String REFERRAL_TEXT = "mengundangmu untuk menjadi bergabung  di OiAccess";

    public static final String GROUP_ID = "2";

    public static final String BASE_URL = "http://128.199.204.39:9000/restapi/";
    public static final String IMAGE_URL = "http://128.199.204.39:9000/";

    public static final String API_AUTH = BASE_URL + "auth";

    public static final String API_PAGING = "page=";
    public static final String API_LIMIT = "&limit=10";

    public static final String CATEGORY_USER_MANAGE = "users/";
    public static final String API_USER_PROFILE = "me/";
    public static final String API_USER_PROFILE_WALLET = BASE_URL + "users/me/wallet/";
    public static final String API_USER_STORE_REGISTER = BASE_URL + "users/store/update";

    public static final String CATEGORY_PRODUCTS = BASE_URL + "products";
    public static final String API_PRODUCT_BEST_SELLER = CATEGORY_PRODUCTS + "/best-seller?page=1&limit=4";
    public static final String API_PRODUCT_BY_NAME = CATEGORY_PRODUCTS + "?keyword=";
    public static final String API_PRODUCT_BY_CATEGORY = CATEGORY_PRODUCTS + "/category/";
    public static final String API_PRODUCT_BY_STORE = CATEGORY_PRODUCTS + "/store/";
    public static final String API_PRODUCT_UPLOAD_IMAGE = CATEGORY_PRODUCTS + "/upload";

    public static final String CATEGORY_ORDER = BASE_URL + "orders";
    public static final String API_ORDER_LIST_SELLER = CATEGORY_ORDER + "?seller=true&";
    public static final String API_UPDATE_ORDER_STATUS = CATEGORY_ORDER + "/status/";
    public static final String API_UPDATE_ORDER_RESI = CATEGORY_ORDER + "/resi/";

    public static final String CATEGORY_COURIER = BASE_URL + "couriers";
    public static final String API_COURIER_LIST = CATEGORY_COURIER + "/list";
    public static final String API_COURIER_PROVINCE = CATEGORY_COURIER + "/province";
    public static final String API_COURIER_CITY = CATEGORY_COURIER + "/city?province=";
    public static final String API_COURIER_DISTRICT = CATEGORY_COURIER + "/subdistrict?city=";
    public static final String API_COURIER_COST = CATEGORY_COURIER + "/cost";

    public static final String CATEGORY_REPORT_BUG = BASE_URL + "bug-reports";

    public static final String CATEGORY_RATINGS = BASE_URL + "ratings/";
    public static final String LIST_RATINGS_BY_STORE = BASE_URL + "ratings/store/traffic/";
    public static final String LIST_RATINGS_BY_PRODUCT = CATEGORY_RATINGS + "product/list/";

    public static final String CATEGORY_CART = BASE_URL + "carts/";

    public static final String CATEGORY_COMPLAIN = BASE_URL + "complains/";
    public static final String API_RATING_PRODUCT = CATEGORY_RATINGS + "product/";

    public static final String BASE_VERSION = "v1/";

    public static final String IMG_FOLDER = "/oiaccess";

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    //status
    public static final String ORDER_STATUS_WAITING_PAYMENT = "waiting payment";
    public static final String ORDER_STATUS_NEW = "new";
    public static final String ORDER_STATUS_PAID = "paid";
    public static final String ORDER_STATUS_ON_PROCESS = "on process";
    public static final String ORDER_STATUS_ON_SHIPPING = "on shipping";
    public static final String ORDER_STATUS_COMPLETED = "completed";
    public static final String ORDER_STATUS_REJECTED = "rejected";
    public static final String ORDER_STATUS_COMPLAIN = "complained";
    public static final String ORDER_STATUS_RATING = "need rating";

    public static int currentPage = 0;
    public static int NUM_PAGES = 0;
}