package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.cart.Cart;
import com.oiaccess.app.ui.model.cart.Detail;
import com.oiaccess.app.ui.model.delete_cart.DeleteCartResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_CART;

/**
 * Created by Comp on 2/11/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Cart> listItem;
    List<Detail> listTokoItem;

    BoxStore boxStore;
    Box<EntityCart> cartBox;

    List<Long> subTotalHargaList;

    ItemCartAdapter adapter;

    long subTotalHarga = (long) 0;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onUpdateItem(long price);

        void onDeleteItem(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public CartAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
        listTokoItem = new ArrayList<>();
        subTotalHargaList = new ArrayList<>();

        boxStore = MyApplication.getBoxStore();
        cartBox = boxStore.boxFor(EntityCart.class);
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.txt_sub_total_harga)
        TextView txtSubTotalHarga;
        @BindView(R.id.txt_toko)
        TextView txtToko;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;

            final Cart item = listItem.get(position);

            view.txtToko.setText("" + item.getStoreName());
            view.txtSubTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.getGrandtotal())));
            view.recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
            adapter = new ItemCartAdapter(ctx);
            view.recyclerView.setAdapter(adapter);
            adapter.setOnItemClickListener(new ItemCartAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {

                }

                @Override
                public void onDeleteItem(int position) {
                    deleteCart(position, item, view);

                    mOnItemClickListener.onDeleteItem(position);
                }

                @Override
                public void onQuantityChange(long subTotal) {
                    subTotalHarga = (long) 0;

                    subTotalHargaList.set(position, subTotal);

                    for (int i = 0; i < listItem.size(); i++) {
                        subTotalHarga = subTotalHarga + subTotalHargaList.get(i);
                    }

                    Log.e("SUB TOTAL HARGA", "" + subTotalHarga);

                    view.txtSubTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(subTotal)));

                    mOnItemClickListener.onUpdateItem(subTotalHarga);
                }
            });

            listTokoItem = listItem.get(position).getDetails();
            adapter.swap(listTokoItem);

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });

        }
    }

    private void deleteCart(final int position, final Cart itemCart, final OriginalViewHolder view) {
        AndroidNetworking.delete(CATEGORY_CART + adapter.getItem(position).getIdProduct())
                .addHeaders("X-AUTH-TOKEN", new Session(ctx).getUser().getAccessToken())
                .build()
                .getAsObject(DeleteCartResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof DeleteCartResponse) {
                            DeleteCartResponse response1 = (DeleteCartResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {
                                Detail item = adapter.getItem(position);

                                cartBox.remove(item.getIdProduct());
                                adapter.remove(item);

                                adapter.notifyDataSetChanged();
                                if (adapter.getListItem().size() == 0) {
                                    remove(itemCart);
                                }
                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                    }
                });

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(Cart item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<Cart> listItem) {
        for (Cart item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void remove(Cart item) {
        listItem.remove(item);
        notifyDataSetChanged();
    }

    public void swap(List<Cart> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);

        for (int i = 0; i < listItem.size(); i++) {
            subTotalHargaList.add((listItem.get(i).getGrandtotal()));
        }
        notifyDataSetChanged();

    }

    public Cart getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<Cart> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<Cart> getListItem() {
        return listItem;
    }
}
