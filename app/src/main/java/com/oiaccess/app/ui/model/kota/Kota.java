
package com.oiaccess.app.ui.model.kota;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Kota {

    @SerializedName("city_id")
    private String mCityId;
    @SerializedName("city_name")
    private String mCityName;
    @SerializedName("postal_code")
    private String mPostalCode;
    @SerializedName("province")
    private String mProvince;
    @SerializedName("province_id")
    private String mProvinceId;
    @SerializedName("type")
    private String mType;

    public String getCityId() {
        return mCityId;
    }

    public void setCityId(String cityId) {
        mCityId = cityId;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(String postalCode) {
        mPostalCode = postalCode;
    }

    public String getProvince() {
        return mProvince;
    }

    public void setProvince(String province) {
        mProvince = province;
    }

    public String getProvinceId() {
        return mProvinceId;
    }

    public void setProvinceId(String provinceId) {
        mProvinceId = provinceId;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

}
