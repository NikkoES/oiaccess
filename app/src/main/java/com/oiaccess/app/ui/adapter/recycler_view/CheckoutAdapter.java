package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.transaction.CheckoutActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.courir_cost.Cost;
import com.oiaccess.app.ui.model.courir_cost.CourirCost;
import com.oiaccess.app.ui.model.courir_cost.CourirCostResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityOrder;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.API_COURIER_COST;

/**
 * Created by Comp on 2/11/2018.
 */

public class CheckoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<EntityOrder> listItem;
    List<CourirCost> listKurir;
    List<EntityItem> listTokoItem;

    String origin, destination, sOrigin, sDestination, address;

    ItemCheckoutAdapter adapter;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    @OnClick(R.id.et_kurir)
    public void onViewClicked() {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onKurirSelected(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public CheckoutAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
        listKurir = new ArrayList<>();
        listTokoItem = new ArrayList<>();
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_nama_toko)
        TextView txtNamaToko;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.et_kurir)
        EditText etKurir;
        @BindView(R.id.txt_sub_total_harga)
        TextView txtSubTotalHarga;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout_product, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            final EntityOrder item = listItem.get(position);

            view.recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
            adapter = new ItemCheckoutAdapter(ctx);
            view.recyclerView.setAdapter(adapter);
            adapter.setOnItemClickListener(new ItemCheckoutAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {

                }
            });

            listTokoItem = listItem.get(position).listItem;

            loadKurir(item.mTotalWeight);

            Log.e("KURIR", "" + listKurir.size());
            adapter.swap(listTokoItem);

            view.txtNamaToko.setText("" + item.mNamaToko);
            view.txtSubTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.mGrandTotal)));
            view.etKurir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<String> listItem = new ArrayList<>();
                    List<String> listNama = new ArrayList<>();
                    List<String> listService = new ArrayList<>();
                    List<String> listPrice = new ArrayList<>();
                    int count = 0;
                    for (int i = 0; i < listKurir.size(); i++) {
                        CourirCost courirCost = listKurir.get(i);
                        for (int j = 0; j < courirCost.getCosts().size(); j++) {
                            Cost cost = courirCost.getCosts().get(j);
                            listNama.add(courirCost.getName());
                            listService.add(cost.getService());
                            listPrice.add(String.valueOf(cost.getCost().get(0).getValue()));
                            listItem.add(listNama.get(count) + " - " + listService.get(count) + " - Rp. " + StringUtils.priceFormatter(listPrice.get(count)));
                            count++;
                        }
                    }
                    final String[] items = new String[listItem.size()];
                    final String[] nama = new String[listItem.size()];
                    final String[] service = new String[listItem.size()];
                    final String[] price = new String[listItem.size()];
                    for (int i = 0; i < listItem.size(); i++) {
                        nama[i] = listNama.get(i);
                        service[i] = listService.get(i);
                        price[i] = String.valueOf(listPrice.get(i));
                        items[i] = listItem.get(i);
                    }
                    DialogUtils.dialogArray(ctx, items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            view.etKurir.setText(items[which]);
                            item.mCost = Long.parseLong(price[which]);
                            item.mDestination = sDestination;
                            item.mOrigin = sOrigin;
                            item.mVendor = nama[which] + " - " + service[which];
                            item.mAddress = address;
                            mOnItemClickListener.onKurirSelected(position);
                        }
                    });
                }
            });

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    public void loadKurir(Long weight) {
        AndroidNetworking.post(API_COURIER_COST)
                .addHeaders("X-AUTH-TOKEN", new Session(ctx).getUser().getAccessToken())
                .addBodyParameter("origin", origin)
                .addBodyParameter("originType", "city")
                .addBodyParameter("destination", destination)
                .addBodyParameter("destinationType", "city")
                .addBodyParameter("weight", String.valueOf(weight))
                .addBodyParameter("courier", "jne:tiki:jnt:pos")
                .build()
                .getAsObject(CourirCostResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CourirCostResponse) {
                            CourirCostResponse response1 = (CourirCostResponse) response;
                            listKurir = response1.getResults();
                            Log.e("KURIR LIST", "" + listKurir.size());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ctx, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(EntityOrder item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addCity(String idOrigin, String idDestination) {
        origin = idOrigin;
        destination = idDestination;
    }

    public void addData(String origin, String destination, String alamat) {
        address = alamat;
        sOrigin = origin;
        sDestination = destination;
    }

    public void addItem(EntityItem item) {
        listTokoItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<EntityOrder> listItem) {
        for (EntityOrder item : listItem) {
            add(item);
        }
    }

    public void addAllItem(List<EntityItem> listItem) {
        for (EntityItem item : listItem) {
            addItem(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<EntityOrder> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public EntityOrder getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<EntityOrder> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<EntityOrder> getListItem() {
        return listItem;
    }
}
