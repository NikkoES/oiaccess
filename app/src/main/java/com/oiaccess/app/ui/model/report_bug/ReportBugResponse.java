
package com.oiaccess.app.ui.model.report_bug;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ReportBugResponse {

    @SerializedName("result")
    private ReportBug mReportBug;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public ReportBug getResult() {
        return mReportBug;
    }

    public void setResult(ReportBug reportBug) {
        mReportBug = reportBug;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
