package com.oiaccess.app.ui.adapter.viewpager;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oiaccess.app.R;

import java.util.ArrayList;

public class ImageSliderPager extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    //    private ArrayList<dataImageSlider> dataImageSliders;
    private ArrayList<Integer> dataImageSliders;

    public ImageSliderPager(Context context, ArrayList<Integer> dataImageSliders) {
        this.context = context;
        this.dataImageSliders = dataImageSliders;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return dataImageSliders.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.item_slider, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.ValdoImageSlider);
        imageView.setImageResource(dataImageSliders.get(position));

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
