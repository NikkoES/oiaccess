package com.oiaccess.app.ui.fragment.menu;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.Handphone;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.product.ProductDetailActivity;
import com.oiaccess.app.ui.activity.product.SearchProductActivity;
import com.oiaccess.app.ui.activity.transaction.CartActivity;
import com.oiaccess.app.ui.adapter.recycler_view.KategoriAdapter;
import com.oiaccess.app.ui.adapter.recycler_view.ProdukAdapter;
import com.oiaccess.app.ui.adapter.viewpager.ImageSliderPager;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Constants;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.category.Kategori;
import com.oiaccess.app.ui.model.category.KategoriResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.synnapps.carouselview.CarouselView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BEST_SELLER;
import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_CATEGORY;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    View view;

    @BindView(R.id.searchET)
    TextView searchET;
    @BindView(R.id.cartBtn)
    FrameLayout cartBtn;
    @BindView(R.id.headerPanel)
    LinearLayout headerPanel;
    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.layoutKategori)
    LinearLayout layoutKategori;
    @BindView(R.id.textBest)
    TextView textBest;
    @BindView(R.id.homeScrollView)
    NestedScrollView homeScrollView;
    @BindView(R.id.swip)
    SwipeRefreshLayout swip;
    @BindView(R.id.gridview_kategori)
    RecyclerView gridviewKategori;
    @BindView(R.id.gridview_best_seller)
    RecyclerView gridviewBestSeller;
    @BindView(R.id.cart_badge)
    TextView cartBadge;
    @BindView(R.id.handphone)
    FloatingActionButton handphone;

    Unbinder unbinder;

    private ArrayList<Integer> listimageSliders = new ArrayList<Integer>();
    private int[] myImageList = new int[]{R.drawable.image1, R.drawable.image2, R.drawable.image4, R.drawable.image5};

    KategoriAdapter adapter;
    ProdukAdapter adapter2;

    List<Kategori> listKategori = new ArrayList<>();
    List<Produk> listProduk = new ArrayList<>();

    BoxStore boxStore;
    Box<EntityCart> orderBox;

    int mCartItemCount = 0;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        boxStore = MyApplication.getBoxStore();
        orderBox = boxStore.boxFor(EntityCart.class);

        initView();

        swip.setOnRefreshListener(this);
        swip.setColorSchemeResources(R.color.bg_screen1);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        setupBadge();
    }

    private void setupBadge() {
        List<EntityCart> list = orderBox.getAll();
        mCartItemCount = list.size();

        if (cartBadge != null) {
            if (mCartItemCount == 0) {
                if (cartBadge.getVisibility() != View.GONE) {
                    cartBadge.setVisibility(View.GONE);
                }
            } else {
                Log.e("ITEM CART", "" + mCartItemCount);
                cartBadge.setText(String.valueOf(mCartItemCount));
                if (cartBadge.getVisibility() != View.VISIBLE) {
                    cartBadge.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void initView() {
        initSlider();
        initKategori();
        initBestSeller();
    }

    private void initBestSeller() {
        gridviewBestSeller.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter2 = new ProdukAdapter(getActivity());
        gridviewBestSeller.setAdapter(adapter2);
        adapter2.setOnItemClickListener(new ProdukAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intentDetailProduct = new Intent(getActivity(), ProductDetailActivity.class);
                intentDetailProduct.putExtra("item", adapter2.getItem(position));
                startActivity(intentDetailProduct);
            }
        });

        Log.e("TOKEN", "" + new Session(getActivity()).getUser().getAccessToken());

        AndroidNetworking.get(API_PRODUCT_BEST_SELLER)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .build()
                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProductResponse) {
                            ProductResponse response1 = (ProductResponse) response;
                            listProduk.clear();
                            Long idUser = Long.parseLong(new Session(getActivity()).getUser().getUserId());
                            for (Produk p : response1.getResult()
                            ) {
                                if (!p.getIdToko().equals(idUser)) {
                                    listProduk.add(p);
                                }
                            }

                            adapter2.swap(listProduk);
                        }
//                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
//                        DialogUtils.closeDialog();
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initKategori() {
        gridviewKategori.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new KategoriAdapter(getActivity());
        gridviewKategori.setAdapter(adapter);
        adapter.setOnItemClickListener(new KategoriAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent i = new Intent(getActivity(), SearchProductActivity.class);
                i.putExtra("id_category", adapter.getItem(position).getIdCategory());
                Log.e("ID KATEGORI", "" + adapter.getItem(position).getIdCategory());
                startActivity(i);
            }
        });


        handphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hanphone = new Intent(getActivity(), Handphone.class);
                startActivity(hanphone);
            }
        });


        AndroidNetworking.get(API_PRODUCT_BY_CATEGORY)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .build()
                .getAsObject(KategoriResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof KategoriResponse) {
                            KategoriResponse response1 = (KategoriResponse) response;
                            listKategori = response1.getResult();

                            adapter.swap(listKategori);
                        }
//                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
//                        DialogUtils.closeDialog();
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initSlider() {
        for (int i = 0; i < myImageList.length; i++) {
            listimageSliders.add(myImageList[i]);
        }

        viewPager.setAdapter(new ImageSliderPager(getActivity(), listimageSliders));
        indicator.setViewPager(viewPager);
        final float denstity = getResources().getDisplayMetrics().density;
        indicator.setRadius(4 * denstity);
        Constants.NUM_PAGES = listimageSliders.size();

        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (Constants.currentPage == Constants.NUM_PAGES) {
                    Constants.currentPage = 0;
                }
                viewPager.setCurrentItem(Constants.currentPage++, true);

            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 3000, 4000);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Constants.currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initView();
                swip.setRefreshing(false);

            }
        }, 500);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        initView();
        unbinder.unbind();
    }

    @OnClick({R.id.searchET, R.id.cartBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.searchET:
                startActivity(new Intent(getActivity(), SearchProductActivity.class));
                break;
            case R.id.cartBtn:
                Intent intentCart = new Intent(getActivity(), CartActivity.class);
                startActivity(intentCart);
                break;
        }
    }
}
