
package com.oiaccess.app.ui.model.komplain;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class KomplainResponse {

    @SerializedName("result")
    private Komplain mKomplain;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public Komplain getResult() {
        return mKomplain;
    }

    public void setResult(Komplain komplain) {
        mKomplain = komplain;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
