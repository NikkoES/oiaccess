
package com.oiaccess.app.ui.model.post_checkout;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Checkout implements Serializable {

    @SerializedName("orders")
    private List<Order> mOrders;

    public List<Order> getOrders() {
        return mOrders;
    }

    public void setOrders(List<Order> orders) {
        mOrders = orders;
    }

}
