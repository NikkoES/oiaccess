
package com.oiaccess.app.ui.model.upload_product;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UploadProduct {

    @SerializedName("encoding")
    private String mEncoding;
    @SerializedName("fieldname")
    private String mFieldname;
    @SerializedName("filename")
    private String mFilename;
    @SerializedName("mimetype")
    private String mMimetype;
    @SerializedName("originalname")
    private String mOriginalname;
    @SerializedName("size")
    private Long mSize;
    @SerializedName("url")
    private String mUrl;

    public String getEncoding() {
        return mEncoding;
    }

    public void setEncoding(String encoding) {
        mEncoding = encoding;
    }

    public String getFieldname() {
        return mFieldname;
    }

    public void setFieldname(String fieldname) {
        mFieldname = fieldname;
    }

    public String getFilename() {
        return mFilename;
    }

    public void setFilename(String filename) {
        mFilename = filename;
    }

    public String getMimetype() {
        return mMimetype;
    }

    public void setMimetype(String mimetype) {
        mMimetype = mimetype;
    }

    public String getOriginalname() {
        return mOriginalname;
    }

    public void setOriginalname(String originalname) {
        mOriginalname = originalname;
    }

    public Long getSize() {
        return mSize;
    }

    public void setSize(Long size) {
        mSize = size;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

}
