package com.oiaccess.app.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.oiaccess.app.R;
import com.oiaccess.app.pref.PrefUtil;
import com.oiaccess.app.services.ApiClient;
import com.oiaccess.app.services.Constants;

import org.json.JSONObject;

import retrofit2.Response;


public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;
    ApiClient apiClient;

    public void showProgressDialog(Context context, String message) {
        // TODO: Change Progress dialog color
        progressDialog = new ProgressDialog(context);
        if (progressDialog != null) {
            if (message != null) {
                progressDialog.setMessage(message);
                Log.d("P-D",context.getClass().getSimpleName());
            }
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } else {
            showErrorAlert("Progress dialog is not set or null");
        }

    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        } else {
            showErrorAlert("Progress dialog is null");
        }
    }

    public void showErrorAlert(final String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.alert_title)
                .setMessage(message)
                .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (message.contains("404")){
                            //logout();
                        }else {
                            dialog.dismiss();
                        }
                    }
                })
                .create();
        alertDialog.show();

        //set dialog button color
        //get color
        int buttonColor = getResources().getColor(R.color.colorAccent);
        //get the positive button
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        //set the color to the buttton
        pbutton.setTextColor(buttonColor);
    }

    protected void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showAlertNoInternet() {
       // Log.d("showAlertNoInternet=>","showAlertNoInternet");

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(  getString(R.string.no_internet_connection))
                .setCancelable(false)
                .setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //recreate();
                        //Intent newIntent = new Intent(, CaptureActivity.class);
                        //startActivity();
                    }
                })
                .create();
        alertDialog.show();
        //set dialog button color
        //get color
        int buttonColor = getResources().getColor(R.color.colorAccent);
        //get the positive button
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        //set the color to the buttton
        pbutton.setTextColor(buttonColor);
    }

    public void showAlertUpdate(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Intent viewIntent =
                                    new Intent("android.intent.action.VIEW",
                                            Uri.parse(Constants.PLAYSTORE_URL));
                            startActivity(viewIntent);
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Unable to Connect Try Again...",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        finish();
                    }
                })
                .create();
        alertDialog.show();


        //set dialog button color
        //get color
        int buttonColor = getResources().getColor(R.color.colorAccent);
        //get the positive button
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        //set the color to the buttton
        pbutton.setTextColor(buttonColor);
    }

    public void getJsonErrorMessage(Context context, Response response) {
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            if (jObjError.getString("error_code").equals("72")) {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
                //generateNewToken(context);
            } else if (jObjError.getString("error_code").equals("48")) {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            } else if(response.code()==400 || response.code() ==401) {
                showErrorAlert(jObjError.getString("error_message"));
            }else if(jObjError.getString("error_code").equals("404")) {
                showErrorAlert(jObjError.getString("error_message"));
            }else{
                showErrorAlert(jObjError.getString("error_message"));
            }
        } catch (Exception e) {
            showToast(context, e.getMessage());
        }
    }



    public String getIsOTPDone() {
        sharedPreferences = this.getSharedPreferences(PrefUtil.PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefUtil.PREF_USER_OTP_FINISH, "");
    }

    public void setIsOTPDone(String type) {
        SharedPreferences.Editor editor = this.getSharedPreferences(PrefUtil.PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(PrefUtil.PREF_USER_OTP_FINISH, type);
        editor.apply();
    }

    public String getLatLong() {
        sharedPreferences = this.getSharedPreferences(PrefUtil.PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefUtil.PREF_USER_LATLONG, "");
    }

    public void setLatLong(String type) {
        SharedPreferences.Editor editor = this.getSharedPreferences(PrefUtil.PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(PrefUtil.PREF_USER_LATLONG, type);
        editor.apply();
    }

    public void setPlayInstallReferrer(String referrer) {
        SharedPreferences.Editor editor = getSharedPreferences(PrefUtil.PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(PrefUtil.PREF_PLAY_INSTALL_REFERRER, referrer);
        editor.apply();
    }

    public String getPlayInstallReferrer() {
        sharedPreferences = this.getSharedPreferences(PrefUtil.PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PrefUtil.PREF_PLAY_INSTALL_REFERRER, "");
    }



}
