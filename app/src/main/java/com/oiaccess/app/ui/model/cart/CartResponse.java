
package com.oiaccess.app.ui.model.cart;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CartResponse {

    @SerializedName("result")
    private List<Cart> mCart;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Cart> getResult() {
        return mCart;
    }

    public void setResult(List<Cart> cart) {
        mCart = cart;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
