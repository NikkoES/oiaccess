
package com.oiaccess.app.ui.model.order;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Order {

    @SerializedName("result")
    private String mResult;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
