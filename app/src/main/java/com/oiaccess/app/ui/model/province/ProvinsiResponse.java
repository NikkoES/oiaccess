
package com.oiaccess.app.ui.model.province;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProvinsiResponse {

    @SerializedName("results")
    private List<Provinsi> mProvinsis;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Provinsi> getResults() {
        return mProvinsis;
    }

    public void setResults(List<Provinsi> provinsis) {
        mProvinsis = provinsis;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
