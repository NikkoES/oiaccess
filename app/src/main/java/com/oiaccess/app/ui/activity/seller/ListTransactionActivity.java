package com.oiaccess.app.ui.activity.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.TransactionAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.list_order.ListOrder;
import com.oiaccess.app.ui.model.list_order.ListOrderResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_PAID;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_REJECTED;

public class ListTransactionActivity extends AppCompatActivity {

    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;
    @BindView(R.id.orderListLV)
    RecyclerView orderListLV;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    TransactionAdapter adapter;

    String status;

    List<ListOrder> listTransaction = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_transaction);
        ButterKnife.bind(this);

        status = getIntent().getStringExtra("status");
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        txtEmpty.setVisibility(View.VISIBLE);
        setHeaderToolbar();
        orderListLV.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TransactionAdapter(this);
        orderListLV.setAdapter(adapter);
        adapter.setOnItemClickListener(new TransactionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent i = new Intent(ListTransactionActivity.this, DetailTransactionActivity.class);
                i.putExtra("status", status);
                i.putExtra("id_order", "" + adapter.getItem(position).getIdOrder());
                startActivity(i);
            }
        });

        Log.e("TOKEN", "" + new Session(this).getUser().getAccessToken());

        loadItems();
    }

    public void setHeaderToolbar() {
        switch (status) {
            case ORDER_STATUS_NEW:
                headerTitleTV.setText("Orderan Baru");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_ON_PROCESS:
                headerTitleTV.setText("Pesanan Dikirim");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_ON_SHIPPING:
                headerTitleTV.setText("Sedang dalam Pengiriman");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_COMPLETED:
                headerTitleTV.setText("Sampai Tujuan");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_REJECTED:
                headerTitleTV.setText("Pesanan Ditolak");
                headerTitleTV.setTextSize(16);
                break;
        }
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder<>(CATEGORY_ORDER);
        getRequestBuilder.addQueryParameter("status", getIntent().getStringExtra("status"));
        getRequestBuilder.addQueryParameter("seller", "true");
        getRequestBuilder
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ListOrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ListOrderResponse) {
                            DialogUtils.closeDialog();
                            ListOrderResponse response1 = (ListOrderResponse) response;
                            if (response1.getTotal() != 0) {
                                listTransaction = response1.getResult();
                                txtEmpty.setVisibility(View.GONE);
                                orderListLV.setVisibility(View.VISIBLE);
                                adapter.swap(listTransaction);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(ListTransactionActivity.this, "Tidak ada data!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
