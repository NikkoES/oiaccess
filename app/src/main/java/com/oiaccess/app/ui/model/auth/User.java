
package com.oiaccess.app.ui.model.auth;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class User {

    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("fcm_id")
    private String mFcmId;
    @SerializedName("fullname")
    private String mFullname;
    @SerializedName("gerai_name")
    private String mGeraiName;
    @SerializedName("is_confirmed_email")
    private String mIsConfirmedEmail;
    @SerializedName("is_confirmed_hp")
    private String mIsConfirmedHp;
    @SerializedName("is_seller")
    private String mIsSeller;
    @SerializedName("is_verified")
    private String mIsVerified;
    @SerializedName("my_referal_code")
    private String mMyReferalCode;
    @SerializedName("nama_kelompok")
    private String mNamaKelompok;
    @SerializedName("no_anggota")
    private String mNoAnggota;
    @SerializedName("oi_group_id")
    private String mOiGroupId;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("refresh_token")
    private String mRefreshToken;
    @SerializedName("status_verifikasi")
    private String mStatusVerifikasi;
    @SerializedName("user_id")
    private String mUserId;
    @SerializedName("vacc_number")
    private String mVaccNumber;
    @SerializedName("verifikasi_msg")
    private String mVerifikasiMsg;

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFcmId() {
        return mFcmId;
    }

    public void setFcmId(String fcmId) {
        mFcmId = fcmId;
    }

    public String getFullname() {
        return mFullname;
    }

    public void setFullname(String fullname) {
        mFullname = fullname;
    }

    public String getGeraiName() {
        return mGeraiName;
    }

    public void setGeraiName(String geraiName) {
        mGeraiName = geraiName;
    }

    public String getIsConfirmedEmail() {
        return mIsConfirmedEmail;
    }

    public void setIsConfirmedEmail(String isConfirmedEmail) {
        mIsConfirmedEmail = isConfirmedEmail;
    }

    public String getIsConfirmedHp() {
        return mIsConfirmedHp;
    }

    public void setIsConfirmedHp(String isConfirmedHp) {
        mIsConfirmedHp = isConfirmedHp;
    }

    public String getIsSeller() {
        return mIsSeller;
    }

    public void setIsSeller(String isSeller) {
        mIsSeller = isSeller;
    }

    public String getIsVerified() {
        return mIsVerified;
    }

    public void setIsVerified(String isVerified) {
        mIsVerified = isVerified;
    }

    public String getMyReferalCode() {
        return mMyReferalCode;
    }

    public void setMyReferalCode(String myReferalCode) {
        mMyReferalCode = myReferalCode;
    }

    public String getNamaKelompok() {
        return mNamaKelompok;
    }

    public void setNamaKelompok(String namaKelompok) {
        mNamaKelompok = namaKelompok;
    }

    public String getNoAnggota() {
        return mNoAnggota;
    }

    public void setNoAnggota(String noAnggota) {
        mNoAnggota = noAnggota;
    }

    public String getOiGroupId() {
        return mOiGroupId;
    }

    public void setOiGroupId(String oiGroupId) {
        mOiGroupId = oiGroupId;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        mRefreshToken = refreshToken;
    }

    public String getStatusVerifikasi() {
        return mStatusVerifikasi;
    }

    public void setStatusVerifikasi(String statusVerifikasi) {
        mStatusVerifikasi = statusVerifikasi;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getVaccNumber() {
        return mVaccNumber;
    }

    public void setVaccNumber(String vaccNumber) {
        mVaccNumber = vaccNumber;
    }

    public String getVerifikasiMsg() {
        return mVerifikasiMsg;
    }

    public void setVerifikasiMsg(String verifikasiMsg) {
        mVerifikasiMsg = verifikasiMsg;
    }

}
