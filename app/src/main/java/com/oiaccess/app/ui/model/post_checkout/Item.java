
package com.oiaccess.app.ui.model.post_checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Item implements Serializable {

    @SerializedName("id_product")
    private Long mIdProduct;
    @SerializedName("jumlah")
    private Long mJumlah;
    @SerializedName("total")
    private Long mTotal;

    private String mImageProduct;
    private String mNamaProduct;
    private Long mStock;

    public Long getIdProduct() {
        return mIdProduct;
    }

    public void setIdProduct(Long idProduct) {
        mIdProduct = idProduct;
    }

    public String getImageProduct() {
        return mImageProduct;
    }

    public void setImageProduct(String imageProduct) {
        mImageProduct = imageProduct;
    }

    public Long getJumlah() {
        return mJumlah;
    }

    public void setJumlah(Long jumlah) {
        mJumlah = jumlah;
    }

    public String getNamaProduct() {
        return mNamaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        mNamaProduct = namaProduct;
    }

    public Long getStock() {
        return mStock;
    }

    public void setStock(Long stock) {
        mStock = stock;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

}
