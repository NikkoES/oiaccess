
package com.oiaccess.app.ui.model.detail_order;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DetailOrder {

    @SerializedName("details")
    private List<Detail> mDetails;
    @SerializedName("order")
    private Order mOrder;

    public List<Detail> getDetails() {
        return mDetails;
    }

    public void setDetails(List<Detail> details) {
        mDetails = details;
    }

    public Order getOrder() {
        return mOrder;
    }

    public void setOrder(Order order) {
        mOrder = order;
    }

}
