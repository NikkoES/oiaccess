
package com.oiaccess.app.ui.model.list_order;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ListOrderResponse {

    @SerializedName("result")
    private List<ListOrder> mListOrder;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;
    @SerializedName("total")
    private Long mTotal;

    public List<ListOrder> getResult() {
        return mListOrder;
    }

    public void setResult(List<ListOrder> listOrder) {
        mListOrder = listOrder;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

}
