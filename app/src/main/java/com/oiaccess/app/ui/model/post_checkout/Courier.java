
package com.oiaccess.app.ui.model.post_checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Courier implements Serializable {

    @SerializedName("cost")
    private Long mCost;
    @SerializedName("destination")
    private String mDestination;
    @SerializedName("origin")
    private String mOrigin;
    @SerializedName("vendor")
    private String mVendor;

    public Long getCost() {
        return mCost;
    }

    public void setCost(Long cost) {
        mCost = cost;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getVendor() {
        return mVendor;
    }

    public void setVendor(String vendor) {
        mVendor = vendor;
    }

}
