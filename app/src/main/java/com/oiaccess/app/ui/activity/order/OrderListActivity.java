package com.oiaccess.app.ui.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.OrderListAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.list_order.ListOrder;
import com.oiaccess.app.ui.model.list_order.ListOrderResponse;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_RATING;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_REJECTED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_WAITING_PAYMENT;

public class OrderListActivity extends AppCompatActivity {

    @BindView(R.id.headerTitleTV)
    TextView headerTitleTV;
    @BindView(R.id.orderListLV)
    RecyclerView orderListLV;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;

    OrderListAdapter adapter;

    String status;

    List<ListOrder> listOrder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        status = getIntent().getStringExtra("status");
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void initView() {
        txtEmpty.setVisibility(View.VISIBLE);
        setHeaderToolbar();
        orderListLV.setLayoutManager(new LinearLayoutManager(this));
        adapter = new OrderListAdapter(this);
        orderListLV.setAdapter(adapter);
        adapter.setOnItemClickListener(new OrderListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent i = new Intent(OrderListActivity.this, DetailOrderActivity.class);
                i.putExtra("status", status);
                i.putExtra("id_order", "" + adapter.getItem(position).getIdOrder());
                startActivity(i);
            }
        });

        loadItems();
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        ANRequest.GetRequestBuilder getRequestBuilder = new ANRequest.GetRequestBuilder<>(CATEGORY_ORDER);
        getRequestBuilder.addQueryParameter("status", getIntent().getStringExtra("status"));
        getRequestBuilder
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ListOrderResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ListOrderResponse) {
                            DialogUtils.closeDialog();
                            ListOrderResponse response1 = (ListOrderResponse) response;
                            if (response1.getTotal() != 0) {
                                listOrder = response1.getResult();
                                txtEmpty.setVisibility(View.GONE);
                                orderListLV.setVisibility(View.VISIBLE);
                                adapter.swap(listOrder);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(OrderListActivity.this, "Tidak ada data!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void setHeaderToolbar() {
        switch (status) {
            case ORDER_STATUS_WAITING_PAYMENT:
                headerTitleTV.setText("Menunggu Pembayaran");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_NEW:
                headerTitleTV.setText("Menunggu Konfirmasi");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_ON_PROCESS:
                headerTitleTV.setText("Pesanan Diproses");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_ON_SHIPPING:
                headerTitleTV.setText("Sedang Dikirim");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_RATING:
                headerTitleTV.setText("Pesanan Belum Dirating");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_COMPLETED:
                headerTitleTV.setText("Pesanan Selesai");
                headerTitleTV.setTextSize(16);
                break;
            case ORDER_STATUS_REJECTED:
                headerTitleTV.setText("Pesanan Ditolak");
                headerTitleTV.setTextSize(16);
                break;
        }
    }

    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        finish();
    }
}
