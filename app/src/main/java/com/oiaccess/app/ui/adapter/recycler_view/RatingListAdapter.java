package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mikhaellopez.circularimageview.CircularImageView;
import com.oiaccess.app.ui.model.IsiRatingProduct;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.rating.ListRating;
import com.oiaccess.app.ui.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RatingListAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;

    List<ListRating> listItem;

    public RatingListAdapter(Context mContext) {
        this.mContext = mContext;
        listItem = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating_product, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            final ListRating item = listItem.get(position);
            view.txtTanggal.setText(CommonUtil.generalFormatDate(item.getmCreatedAt().toString(), "yyyy-MM-dd'T'HH:mm:ss.Z", "dd/MM/yyyy"));
            view.ratingBar.setRating((float)Float.parseFloat(item.getmRating().toString()));
            view.txtDesc.setText(item.getmMessage().toString());
            view.txtNameUser.setText(item.getmBuyerName().toString());
        }

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(ListRating item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<ListRating> listItem) {
        for (ListRating item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<ListRating> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public ListRating getItem(int pos) {
        return listItem.get(pos);
    }

    public List<ListRating> getListItem() {
        return listItem;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_tanggal)
        TextView txtTanggal;
        @BindView(R.id.nameuser)
        TextView txtNameUser;
        @BindView(R.id.isideskripsi)
        TextView txtDesc;
        @BindView(R.id.ratingBar)
        AppCompatRatingBar ratingBar;

        public OriginalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
