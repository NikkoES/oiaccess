package com.oiaccess.app.ui.activity.product;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.buyer.ViewStoreActivity;
import com.oiaccess.app.ui.activity.transaction.CartActivity;
import com.oiaccess.app.ui.adapter.recycler_view.KomentarAdapter;
import com.oiaccess.app.ui.adapter.recycler_view.RelatedProductAdapter;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.add_cart.AddCartResponse;
import com.oiaccess.app.ui.model.detail_product.DetailProduct;
import com.oiaccess.app.ui.model.detail_product.DetailProductResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityCart;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_PRODUCT_BY_CATEGORY;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_CART;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_PRODUCTS;


public class ProductDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbarproduk)
    Toolbar toolbarproduk;
    @BindView(R.id.productImageIV)
    CarouselView productImageIV;
    @BindView(R.id.productNameTV)
    TextView productNameTV;
    @BindView(R.id.discountTV)
    TextView discountTV;
    @BindView(R.id.discountPriceTV)
    TextView discountPriceTV;
    @BindView(R.id.priceTV)
    TextView priceTV;
    //    @BindView(R.id.ratingBar)
//    AppCompatRatingBar ratingBar;
    @BindView(R.id.stockTV)
    TextView stockTV;
    @BindView(R.id.soldTV)
    TextView soldTV;
    @BindView(R.id.seenTV)
    TextView seenTV;
    @BindView(R.id.detailProductTV)
    TextView detailProductTV;
    @BindView(R.id.produkdetaildialog)
    CardView produkdetaildialog;
    @BindView(R.id.ratingBar)
    AppCompatRatingBar ratingBar;
    @BindView(R.id.txt_ratingBar)
    TextView txtRatingBar;
    @BindView(R.id.produklain)
    CardView produklain;
    @BindView(R.id.atas)
    LinearLayout atas;
    @BindView(R.id.footer1)
    LinearLayout footer1;
    @BindView(R.id.list_komentar)
    RecyclerView recyclerKomenter;
    @BindView(R.id.list_related_product)
    RecyclerView recyclerRelatedProduct;
    @BindView(R.id.txt_kategori)
    TextView txtKategori;
    @BindView(R.id.txt_kondisi)
    TextView txtKondisi;
    @BindView(R.id.txt_minimum_order)
    TextView txtMinimumOrder;
    @BindView(R.id.store_name)
    TextView txtStoreName;
    @BindView(R.id.discountLayout)
    LinearLayout discountLayout;
    @BindView(R.id.txt_jumlah_item)
    ElegantNumberButton txtJumlahItem;


    TextView textCartItemCount;

    Produk product;
    DetailProduct detailProduct;

    List<Produk> listRelatedProduct = new ArrayList<>();

    KomentarAdapter adapter;
    RelatedProductAdapter adapter2;

    BoxStore boxStore;
    Box<EntityCart> cartBox;
    Box<EntityItem> itemBox;

    int jumlahBarang;

    int mCartItemCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        product = (Produk) getIntent().getSerializableExtra("item");
        boxStore = MyApplication.getBoxStore();
        cartBox = boxStore.boxFor(EntityCart.class);
        itemBox = boxStore.boxFor(EntityItem.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSupportActionBar(toolbarproduk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Product");
        toolbarproduk.setNavigationIcon(R.drawable.ic_arrow_white_24dp);

        initDetail();
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Glide.with(ProductDetailActivity.this).load(detailProduct.getImages().get(position).getImageName()).into(imageView);
        }
    };

    private void initDetail() {
        AndroidNetworking.get(CATEGORY_PRODUCTS + "/" + product.getIdProduct())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(DetailProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof DetailProductResponse) {
                            DetailProductResponse response1 = (DetailProductResponse) response;
                            detailProduct = response1.getResult();

                            productImageIV.setImageListener(imageListener);
                            productImageIV.setPageCount(detailProduct.getImages().size());
                            productImageIV.setImageClickListener(new ImageClickListener() {
                                @Override
                                public void onClick(int positions) {
                                    Intent intent = new Intent(getApplicationContext(), ViewDetailProduk.class);
                                    intent.putExtra("images", detailProduct.getImages().get(positions).getImageName());
                                    startActivity(intent);
                                }
                            });

                            if (detailProduct.getHargaDiskon() > 0) {
                                Long diskonPrice = (detailProduct.getHarga() * detailProduct.getHargaDiskon()) / 100;
                                discountPriceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(detailProduct.getHarga())));
                                priceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(detailProduct.getHarga() - diskonPrice)));
                                discountTV.setText(detailProduct.getHargaDiskon().toString() + "%");
                                discountPriceTV.setPaintFlags(priceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            } else {
                                priceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(detailProduct.getHarga())));
//                                discountTV.setVisibility(View.GONE);
                                discountPriceTV.setVisibility(View.GONE);
                                discountLayout.setVisibility(View.GONE);
                            }

                            productNameTV.setText(detailProduct.getNamaProduct());
                            stockTV.setText("" + detailProduct.getStock());
                            soldTV.setText("" + detailProduct.getProductSold());
                            seenTV.setText("" + detailProduct.getSeen());
                            detailProductTV.setText(detailProduct.getDescription());
                            txtKategori.setText(detailProduct.getCategoryName());
                            txtKondisi.setText(detailProduct.getKondisi());
                            txtMinimumOrder.setText(detailProduct.getmMinOrder().toString());
                            txtStoreName.setText(detailProduct.getNamaToko());
                            txtRatingBar.setText(detailProduct.getmRatingsProduct());
                            ratingBar.setRating((float) Float.parseFloat(detailProduct.getmRatingsProduct()));
                            initView();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ProductDetailActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });

        toolbarproduk.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initView() {
        jumlahBarang = Integer.parseInt(String.valueOf(product.getmMinOrder()));
        txtJumlahItem.setRange(Integer.parseInt(String.valueOf(product.getmMinOrder())), Integer.parseInt(String.valueOf(product.getStock())));
        txtJumlahItem.setNumber(String.valueOf(product.getmMinOrder()));
        txtJumlahItem.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                jumlahBarang = newValue;
            }
        });


        recyclerKomenter.setLayoutManager(new LinearLayoutManager(this));
        adapter = new KomentarAdapter(this);
        recyclerKomenter.setAdapter(adapter);
        adapter.setOnItemClickListener(new KomentarAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Toast.makeText(ProductDetailActivity.this, "" + adapter.getItem(position).getCategoryName(), Toast.LENGTH_SHORT).show();
            }
        });

        recyclerRelatedProduct.setLayoutManager(new GridLayoutManager(this, 2));
        adapter2 = new RelatedProductAdapter(this);
        recyclerRelatedProduct.setAdapter(adapter2);
        adapter2.setOnItemClickListener(new RelatedProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                finish();
                Intent intentDetailProduct = new Intent(ProductDetailActivity.this, ProductDetailActivity.class);
                intentDetailProduct.putExtra("item", adapter2.getItem(position));
                startActivity(intentDetailProduct);
            }
        });


        loadItems();
    }

    private void loadItems() {
        DialogUtils.openDialog(this);
        AndroidNetworking.get(API_PRODUCT_BY_CATEGORY + detailProduct.getIdCategory())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProductResponse) {
                            ProductResponse response1 = (ProductResponse) response;
                            listRelatedProduct.clear();
                            Long idUser = Long.parseLong(new Session(getApplicationContext()).getUser().getUserId());
                            for (Produk dp : response1.getResult()) {
                                if (!dp.getIdProduct().equals(detailProduct.getIdProduct())) {
                                    listRelatedProduct.add(dp);
                                }
                            }

                            adapter2.swap(listRelatedProduct);
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(ProductDetailActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.seller_toolbarproduk, menu);

        final MenuItem menuItem = menu.findItem(R.id.cartproduk);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.cart_badge);

        setupBadge();

        Log.e("ITEM CART", "" + mCartItemCount);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    private void setupBadge() {
        List<EntityCart> list = cartBox.getAll();
        mCartItemCount = list.size();

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.cartproduk:
                startActivity(new Intent(ProductDetailActivity.this, CartActivity.class));
                break;
        }
        return true;
    }

    @OnClick({R.id.btn_selengkapnya, R.id.visit_store, R.id.valdo_btnAddToCart, R.id.rating})
    public void onViewClicked(View view) {
        Dialog dialog;
        ImageView btnClosed;
        switch (view.getId()) {
//            case R.id.discussionBtn:
//                break;
            case R.id.visit_store:
                Intent intent = new Intent(this, ViewStoreActivity.class);
                intent.putExtra("idStore", detailProduct.getIdToko());
                intent.putExtra("storeName", detailProduct.getNamaToko());
                startActivity(intent);
                break;
            case R.id.btn_selengkapnya:
                dialog = new Dialog(ProductDetailActivity.this, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
                dialog.setContentView(R.layout.dialog_detail_product);
                TextView title = dialog.findViewById(R.id.detailProductTV);
                title.setText(detailProduct.getDescription());
                btnClosed = dialog.findViewById(R.id.btnclosedialog);
                final Dialog finalDialog = dialog;
                btnClosed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalDialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.valdo_btnAddToCart:
                addToCart();
                break;
            case R.id.rating:
                Intent rating = new Intent(this, RatingProductActivity.class);
                rating.putExtra("idProduct", detailProduct.getIdProduct());
                startActivity(rating);
                break;
        }
    }

    private void addToCart() {
        DialogUtils.openDialog(this);
        AndroidNetworking.post(CATEGORY_CART)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addBodyParameter("id_user", new Session(this).getUser().getUserId())
                .addBodyParameter("id_product", String.valueOf(product.getIdProduct()))
                .addBodyParameter("id_toko", String.valueOf(product.getIdToko()))
                .addBodyParameter("jumlah", String.valueOf(jumlahBarang))
                .build()
                .getAsObject(AddCartResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof AddCartResponse) {
                            AddCartResponse response1 = (AddCartResponse) response;
                            if (response1.getStatus().equalsIgnoreCase("1")) {

                                EntityCart cart = new EntityCart();

                                cart.id = Long.parseLong(response1.getResult().getDetail().getIdProduct());
                                cart.idProduk = response1.getResult().getDetail().getIdProduct();
                                cart.idToko = response1.getResult().getDetail().getIdToko();
                                cart.idUser = response1.getResult().getDetail().getIdUser();
                                cart.jumlah = String.valueOf(response1.getResult().getDetail().getJumlahBeli());

                                cartBox.put(cart);

                                setupBadge();

                                Toast.makeText(ProductDetailActivity.this, "Produk berhasil ditambahkan ke Keranjang..", Toast.LENGTH_SHORT).show();
                            }
                        }
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(ProductDetailActivity.this, "Produk gagal ditambahkan ke Keranjang !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
