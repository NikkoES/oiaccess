package com.oiaccess.app.ui.fragment.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TabWidget;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.fragment.BaseFragment;
import com.oiaccess.app.ui.fragment.account.AccountBuyerFragment;
import com.oiaccess.app.ui.fragment.account.AccountSellerFragment;
import com.oiaccess.app.ui.model.auth.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AccountFragment extends Fragment {


    @BindView(R.id.userImageIV)
    RoundedImageView userImageIV;
    @BindView(R.id.fullNameTV)
    TextView fullNameTV;
    @BindView(android.R.id.tabs)
    TabWidget tabs;
    @BindView(android.R.id.tabcontent)
    FrameLayout tabcontent;
    @BindView(R.id.tabHostUser)
    FragmentTabHost fragmentTabHost;
    Unbinder unbinder;

    View view;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadView();
        return view;
    }

    private void loadView() {
        User user = new Session(getActivity()).getUser();
        //Glide.with(getActivity()).load("").into(userImageIV);
        fullNameTV.setText(user.getFullname());

        fragmentTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("Pembeli").setIndicator("Pembeli"), AccountBuyerFragment.class, null);
        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("Penjual").setIndicator("Penjual"), AccountSellerFragment.class, null);
        fragmentTabHost.setPadding(0, 0, 0, 0);
        for (int i = 0; i < fragmentTabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = fragmentTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setAllCaps(false);
            tv.setTextSize(14);
        }
        fragmentTabHost.setCurrentTab(0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
