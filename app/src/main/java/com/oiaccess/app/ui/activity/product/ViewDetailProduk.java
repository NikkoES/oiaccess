package com.oiaccess.app.ui.activity.product;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.oiaccess.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewDetailProduk extends AppCompatActivity {

    @BindView(R.id.viewdetailproduk)
    ImageView viewdetailproduk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail_produk);
        ButterKnife.bind(this);

        Glide.with(this).load(getIntent().getStringExtra("images")).into(viewdetailproduk);
    }

    @OnClick(R.id.btncloseviewprodukdetail)
    public void onViewClicked() {
        finish();
    }
}
