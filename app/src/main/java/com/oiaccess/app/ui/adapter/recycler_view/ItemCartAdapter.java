package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.cart.Detail;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class ItemCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Detail> listItem;
    List<Long> harga;

    long jumlahHarga = (long) 0;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteItem(int position);

        void onQuantityChange(long harga);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ItemCartAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
        harga = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_produk)
        ImageView imageProduk;
        @BindView(R.id.nama_produk)
        TextView namaProduk;
        @BindView(R.id.harga_produk)
        TextView hargaProduk;
        @BindView(R.id.txt_harga)
        TextView totalHarga;
        @BindView(R.id.txt_jumlah_item)
        ElegantNumberButton txtJumlahItem;
        @BindView(R.id.btn_delete)
        ImageView btnDelete;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_item, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            final OriginalViewHolder view = (OriginalViewHolder) holder;
            final Detail item = listItem.get(position);

            final long hargaItem = item.getHarga() - item.getHargaDiskon();

            Glide.with(ctx).load(item.getImageName()).into(view.imageProduk);
            view.namaProduk.setText("" + item.getNamaProduct());
            view.hargaProduk.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(hargaItem)));
            view.totalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(hargaItem * item.getJumlah())));

            view.txtJumlahItem.setRange(Integer.parseInt(String.valueOf(item.getMinOrder())), Integer.parseInt(String.valueOf(item.getStock())));
            view.txtJumlahItem.setNumber(String.valueOf(item.getJumlah()));
            view.txtJumlahItem.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
                @Override
                public void onValueChange(ElegantNumberButton v, int oldValue, int newValue) {
                    long subHarga = hargaItem * newValue;

                    jumlahHarga = (long) 0;

                    harga.set(position, subHarga);

                    for (int i = 0; i < listItem.size(); i++) {
                        jumlahHarga = jumlahHarga + harga.get(i);
                    }

                    view.totalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(subHarga)));

                    mOnItemClickListener.onQuantityChange(jumlahHarga);
                }
            });

            view.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onDeleteItem(position);
                }
            });
            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void remove(Detail item) {
        listItem.remove(item);
        notifyDataSetChanged();
    }

    public void add(Detail item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<Detail> listItem) {
        for (Detail item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<Detail> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);

        for (int i = 0; i < listItem.size(); i++) {
            harga.add((listItem.get(i).getTotal()));
        }
        notifyDataSetChanged();

    }

    public Detail getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<Detail> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<Detail> getListItem() {
        return listItem;
    }
}
