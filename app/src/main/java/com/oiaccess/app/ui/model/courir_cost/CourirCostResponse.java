
package com.oiaccess.app.ui.model.courir_cost;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CourirCostResponse {

    @SerializedName("results")
    private List<CourirCost> mCourirCosts;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<CourirCost> getResults() {
        return mCourirCosts;
    }

    public void setResults(List<CourirCost> courirCosts) {
        mCourirCosts = courirCosts;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
