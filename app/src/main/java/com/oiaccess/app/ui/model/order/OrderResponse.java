
package com.oiaccess.app.ui.model.order;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OrderResponse {

    @SerializedName("results")
    private List<Order> mOrders;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public List<Order> getResults() {
        return mOrders;
    }

    public void setResults(List<Order> orders) {
        mOrders = orders;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
