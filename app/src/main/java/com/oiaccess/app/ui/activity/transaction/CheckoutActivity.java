package com.oiaccess.app.ui.activity.transaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.adapter.recycler_view.CheckoutAdapter;
import com.oiaccess.app.ui.application.MyApplication;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.courir.Kurir;
import com.oiaccess.app.ui.model.courir.KurirResponse;
import com.oiaccess.app.ui.model.courir_cost.CourirCost;
import com.oiaccess.app.ui.model.courir_cost.CourirCostResponse;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.entitiy_cart.EntityOrder;
import com.oiaccess.app.ui.model.kecamatan.Kecamatan;
import com.oiaccess.app.ui.model.kecamatan.KecamatanResponse;
import com.oiaccess.app.ui.model.kota.Kota;
import com.oiaccess.app.ui.model.kota.KotaResponse;
import com.oiaccess.app.ui.model.post_checkout.Checkout;
import com.oiaccess.app.ui.model.post_checkout.Item;
import com.oiaccess.app.ui.model.post_checkout.Order;
import com.oiaccess.app.ui.model.province.Provinsi;
import com.oiaccess.app.ui.model.province.ProvinsiResponse;
import com.oiaccess.app.ui.utils.CommonUtil;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.objectbox.Box;
import io.objectbox.BoxStore;

import static com.oiaccess.app.ui.data.Constants.API_COURIER_CITY;
import static com.oiaccess.app.ui.data.Constants.API_COURIER_COST;
import static com.oiaccess.app.ui.data.Constants.API_COURIER_DISTRICT;
import static com.oiaccess.app.ui.data.Constants.API_COURIER_PROVINCE;

public class CheckoutActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbarlayout)
    AppBarLayout appbarlayout;
    @BindView(R.id.et_nama_penerima)
    EditText etNamaPenerima;
    @BindView(R.id.et_provinsi)
    EditText etProvinsi;
    @BindView(R.id.et_kota)
    EditText etKota;
    @BindView(R.id.et_kecamatan)
    EditText etKecamatan;
    @BindView(R.id.et_alamat)
    EditText etAlamat;
    @BindView(R.id.et_kode_pos)
    EditText etKodePos;
    @BindView(R.id.et_no_hp)
    EditText etNoHp;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_total_harga)
    TextView txtTotalHarga;
    @BindView(R.id.txt_ongkos_kirim)
    TextView txtOngkosKirim;
    @BindView(R.id.txt_total_belanja)
    TextView txtTotalBelanja;

    List<Provinsi> listProvinsi;
    List<Kota> listKota;
    List<Kecamatan> listKecamatan;
    List<CourirCost> list = new ArrayList<>();

    BoxStore boxStore;
    Box<EntityOrder> orderBox;
    Box<EntityItem> itemBox;

    String idProvinsi, idKota, idKecamatan;

    Long ongkosKirim = (long) 0;
    Long totalHarga = (long) 0;
    Long totalBelanja = (long) 0;

    CheckoutAdapter adapter;

    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        session = new Session(this);

        boxStore = MyApplication.getBoxStore();
        orderBox = boxStore.boxFor(EntityOrder.class);
        itemBox = boxStore.boxFor(EntityItem.class);

        loadMasterData();
    }

    private void loadMasterData() {
        etNamaPenerima.setText(session.getUser().getFullname());
        etNoHp.setText(session.getUser().getPhone());
        AndroidNetworking.get(API_COURIER_PROVINCE)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(ProvinsiResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProvinsiResponse) {
                            ProvinsiResponse response1 = (ProvinsiResponse) response;
                            listProvinsi = response1.getResults();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(CheckoutActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initView() {
        final List<EntityOrder> list = orderBox.getAll();

        final Long[] aOngkosKirim = new Long[list.size()];
        final Long[] aTotalHarga = new Long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aOngkosKirim[i] = (long) 0;
            aTotalHarga[i] = (long) 0;
        }

        final Long[] aTotalBelanja = new Long[list.size()];

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CheckoutAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.addCity(idKota, idKota);
        adapter.setOnItemClickListener(new CheckoutAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onKurirSelected(int position) {
                EntityOrder item = adapter.getListItem().get(position);

                ongkosKirim = (long) 0;
                totalHarga = (long) 0;

                aOngkosKirim[position] = item.mCost;
                aTotalHarga[position] = item.mGrandTotal;

                for (int i = 0; i < list.size(); i++) {
                    ongkosKirim = ongkosKirim + aOngkosKirim[i];
                    totalHarga = totalHarga + aTotalHarga[i];
                }
                totalBelanja = ongkosKirim + totalHarga;

                txtOngkosKirim.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(ongkosKirim)));
                txtTotalHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(totalHarga)));
                txtTotalBelanja.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(totalBelanja)));
            }
        });
        loadItems();
    }

    private void loadItems() {
        List<EntityOrder> list = orderBox.getAll();

        if (list.size() > 0) {
            adapter.swap(list);
        }
    }

    @OnClick({R.id.btn_back, R.id.et_provinsi, R.id.et_kota, R.id.et_kecamatan, R.id.btn_bayar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            case R.id.et_provinsi:
                if (listProvinsi != null) {
                    final String[] provinsi = new String[listProvinsi.size()];
                    for (int i = 0; i < listProvinsi.size(); i++) {
                        provinsi[i] = listProvinsi.get(i).getProvince();
                    }
                    DialogUtils.dialogArray(this, provinsi, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            etProvinsi.setText(provinsi[which]);
                            for (int i = 0; i < listProvinsi.size(); i++) {
                                if (provinsi[which].equalsIgnoreCase(listProvinsi.get(i).getProvince())) {
                                    idProvinsi = listProvinsi.get(i).getProvinceId();
                                    loadKota(idProvinsi);
                                    break;
                                }
                            }
                        }
                    });
                } else {
                    Toast.makeText(this, "Mohon tunggu sebentar !", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.et_kota:
                if (listKota != null) {
                    final String[] kota = new String[listKota.size()];
                    for (int i = 0; i < listKota.size(); i++) {
                        kota[i] = listKota.get(i).getCityName();
                    }
                    DialogUtils.dialogArray(this, kota, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            etKota.setText(kota[which]);
                            for (int i = 0; i < listKota.size(); i++) {
                                if (kota[which].equalsIgnoreCase(listKota.get(i).getCityName())) {
                                    idKota = listKota.get(i).getCityId();
                                    loadKecamatan(idKota);
                                    break;
                                }
                            }
                        }
                    });
                } else {
                    Toast.makeText(this, "Mohon tunggu sebentar !", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.et_kecamatan:
                if (listKecamatan != null) {
                    final String[] kecamatan = new String[listKecamatan.size()];
                    for (int i = 0; i < listKecamatan.size(); i++) {
                        kecamatan[i] = listKecamatan.get(i).getSubdistrictName();
                    }
                    DialogUtils.dialogArray(this, kecamatan, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            etKecamatan.setText(kecamatan[which]);
                            for (int i = 0; i < listKecamatan.size(); i++) {
                                if (kecamatan[which].equalsIgnoreCase(listKecamatan.get(i).getSubdistrictName())) {
                                    idKecamatan = listKecamatan.get(i).getSubdistrictId();
                                    loadKecamatan(idKecamatan);
                                    break;
                                }
                            }
                        }
                    });
                } else {
                    Toast.makeText(this, "Mohon tunggu sebentar !", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_bayar:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        ArrayList<View> listView = new ArrayList<>();
        listView.add(etNamaPenerima);
        listView.add(etProvinsi);
        listView.add(etKota);
        listView.add(etKecamatan);
        listView.add(etAlamat);
        listView.add(etKodePos);
        listView.add(etNoHp);
        if (CommonUtil.validateEmptyEntries(listView)) {
            boolean isEmpty = false;
            List<EntityOrder> listOrder = adapter.getListItem();
            orderBox.removeAll();
            itemBox.removeAll();
            Log.e("LIST", "" + listOrder.size());
            for (int i = 0; i < listOrder.size(); i++) {
                EntityOrder order = listOrder.get(i);

                EntityOrder entityOrder = new EntityOrder();

                entityOrder.mIdToko = order.mIdToko;
                entityOrder.mIdCart = order.mIdCart;
                entityOrder.mGrandTotal = order.mGrandTotal;
                entityOrder.mAddress = etAlamat.getText().toString() + ", " + etKecamatan.getText().toString() + ", " + etKota.getText().toString() + ", " + etProvinsi.getText().toString() + ", " + etKodePos.getText().toString();
                entityOrder.mNamaToko = order.mNamaToko;
                entityOrder.mTotalWeight = order.mTotalWeight;
                for (int j = 0; j < order.listItem.size(); j++) {
                    EntityItem item = order.listItem.get(j);
                    EntityItem entityItem = new EntityItem();
                    entityItem.mIdProduct = item.mIdProduct;
                    entityItem.mJumlah = item.mJumlah;
                    entityItem.mTotal = item.mTotal;
                    entityItem.mWeight = item.mWeight;
                    entityItem.mImageProduct = item.mImageProduct;
                    entityItem.mNamaProduct = item.mNamaProduct;
                    entityItem.mStock = item.mStock;
                    entityOrder.listItem.add(entityItem);
                }
                Log.e("COST", "" + order.mCost);
                if (order.mCost == 0) {
                    isEmpty = true;
                    orderBox.put(entityOrder);
                    break;
                } else {
                    entityOrder.mCost = order.mCost;
                    entityOrder.mDestination = etKota.getText().toString();
                    entityOrder.mOrigin = etKota.getText().toString();
                    entityOrder.mVendor = order.mVendor;
                    entityOrder.mPaymentMethod = "Wallet";
                    orderBox.put(entityOrder);
                }
            }
            if (isEmpty) {
                Toast.makeText(this, "Anda belum memilih kurir !", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(this, PaymentMethodActivity.class);
                intent.putExtra("total", totalBelanja);
                startActivity(intent);
            }
        }
    }

    private void loadKecamatan(String idKota) {
        AndroidNetworking.get(API_COURIER_DISTRICT + idKota)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(KecamatanResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof KecamatanResponse) {
                            KecamatanResponse response1 = (KecamatanResponse) response;
                            listKecamatan = response1.getResults();
                            initView();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(CheckoutActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadKota(String idProvinsi) {
        AndroidNetworking.get(API_COURIER_CITY + idProvinsi)
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .build()
                .getAsObject(KotaResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof KotaResponse) {
                            KotaResponse response1 = (KotaResponse) response;
                            listKota = response1.getResults();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(CheckoutActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
