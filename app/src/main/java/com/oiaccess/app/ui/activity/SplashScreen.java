package com.oiaccess.app.ui.activity;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.oiaccess.app.R;
import com.oiaccess.app.services.ApiClient;
import com.oiaccess.app.ui.BaseActivity;
import com.oiaccess.app.ui.LoginActivity;
import com.oiaccess.app.ui.data.Session;

import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends BaseActivity {
    InstallReferrerClient mReferrerClient;

    ApiClient apiClient;
    String isVerified;
    String TAG = SplashScreen.class.getSimpleName();

    String versionCode;
    Double doubleApkVersion;
    Double doubleApiVersion;
    TextView tvVersion;

    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    private int warningwindowrunning = 0;
    private int checkfirsttime = 0;
/*
    String[] permissions = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
*/

    String[] permissions = new String[]{
            // Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            //Manifest.permission.READ_EXTERNAL_STORAGE,
            //Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            //Manifest.permission.READ_CONTACTS
    };
    private PermissionHandler PermissionHandlerLocationSMS;

    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        checkfirsttime = 0;

        session = new Session(this);

        tvVersion = (TextView) findViewById(R.id.tvVersion);
        warningwindowrunning = 0;

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setIsOTPDone("1");
        //get version code
        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = pinfo.versionName;
            if (!versionCode.equals("")) {
                if (getIsOTPDone().isEmpty() || getIsOTPDone().equals("1")) {
                    checkVersion();
                } else {
                    //Intent intent = new Intent(SplashScreen.this, OtpActivity.class);
                    //startActivity(intent);
                    finish();
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mReferrerClient = InstallReferrerClient.newBuilder(this).build();
        mReferrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Connection established
                        Log.w(TAG, "OK");
                        try {
                            Log.v(TAG, "InstallReferrer conneceted");
                            ReferrerDetails response = mReferrerClient.getInstallReferrer();
                            Log.d("Response_referrer=>", response.getInstallReferrer());

                            //Toast.makeText(getApplicationContext(),response.getInstallReferrer(),Toast.LENGTH_SHORT).show();
                            String reff_string = response.getInstallReferrer().toString().trim();
                            Log.d("reff_string", reff_string);
                            Log.d("reff_string", reff_string.substring(0, 3));
                            if (reff_string.substring(0, 3).equals("utm")) {
                                setPlayInstallReferrer("");
                                Log.d("cek", "kosong");

                            } else {
                                setPlayInstallReferrer(reff_string);
                                Log.d("cek", "isi");

                            }

                            mReferrerClient.endConnection();
                            Log.d("pref_reff", getPlayInstallReferrer());


                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app
                        Log.w(TAG, "FEATURE_NOT_SUPPORTED");
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection could not be established
                        Log.w(TAG, "SERVICE_UNAVAILABLE");
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.w(TAG, "onInstallReferrerServiceDisconnected");
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (warningwindowrunning == 1)
            MyWarningwindows();
    }

    public void MyWarningwindows() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if (checkPermission()) {
                        getLastLocation();
//                      runThread();
                    }
                } else {
                    finish();
                }
            }
        };

        new AlertDialog.Builder(this).setTitle("Warning")
                .setMessage("Anda Harus Mengizinkan Akses ke Location dan SMS untuk dapat memakai Aplikasi ini")
                .setPositiveButton(android.R.string.ok, listener)
                .setNegativeButton(android.R.string.cancel, listener)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                }).create().show();
    }

    private boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void checkVersion() {

        getLastLocation();
        runThread();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            setLatLong(String.valueOf(mLastLocation.getLatitude()) + ";" + String.valueOf(mLastLocation.getLongitude()));
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                    warningwindowrunning = 0;
                    runThread();
                    getLastLocation();
                } else {
                    warningwindowrunning = 1;
                }
//                return;
            }
        }
    }

    public void runThread() {
        Thread background = new Thread() {
            public void run() {

                try {
                    // Thread will sleep for 3 seconds
                    sleep(3 * 1000);
                    if(session.isLoggedIn()){
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                    else{
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    }

                    finish();

                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }
            }
        };

        // start thread
        background.start();
    }

    @Override
    public void onBackPressed() {

        if (checkfirsttime == 1) {
            super.onBackPressed();

            moveTaskToBack(true);
            //Log.d("LoginActivity","Test exit 0");
            SplashScreen.this.finish();
            System.exit(0);

        }
        //else Toast.makeText(getApplicationContext(), "Back press disabled!", Toast.LENGTH_SHORT).show();
        //Write your code here
        //
    }
}