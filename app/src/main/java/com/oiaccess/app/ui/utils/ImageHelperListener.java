package com.oiaccess.app.ui.utils;

public interface ImageHelperListener {
	
	public void onProcessStart();
	public void onProcessEnd(String finalFileName);
	
}
