
package com.oiaccess.app.ui.model.detail_product;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DetailProductResponse {

    @SerializedName("result")
    private DetailProduct mDetailProduct;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public DetailProduct getResult() {
        return mDetailProduct;
    }

    public void setResult(DetailProduct detailProduct) {
        mDetailProduct = detailProduct;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
