package com.oiaccess.app.ui.adapter.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.model.entitiy_cart.EntityItem;
import com.oiaccess.app.ui.model.post_checkout.Item;
import com.oiaccess.app.ui.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Comp on 2/11/2018.
 */

public class ItemCheckoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<EntityItem> listItem;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ItemCheckoutAdapter(Context ctx) {
        this.ctx = ctx;
        listItem = new ArrayList<>();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_nama_barang)
        TextView txtNamaBarang;
        @BindView(R.id.txt_jumlah_item)
        TextView txtJumlahItem;
        @BindView(R.id.txt_harga)
        TextView txtHarga;
        @BindView(R.id.image_item)
        ImageView imageItem;

        public OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checkout_store, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            EntityItem item = listItem.get(position);

            Glide.with(ctx).load(item.mImageProduct).into(view.imageItem);
            view.txtNamaBarang.setText(item.mNamaProduct);
            view.txtHarga.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(item.mTotal)));
            view.txtJumlahItem.setText("" + item.mJumlah);

            view.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void add(EntityItem item) {
        listItem.add(item);
        notifyItemInserted(listItem.size() + 1);
    }

    public void addAll(List<EntityItem> listItem) {
        for (EntityItem item : listItem) {
            add(item);
        }
    }

    public void removeAll() {
        listItem.clear();
        notifyDataSetChanged();
    }

    public void swap(List<EntityItem> datas) {
        if (datas == null || datas.size() == 0) listItem.clear();
        if (listItem != null && listItem.size() > 0)
            listItem.clear();
        listItem.addAll(datas);
        notifyDataSetChanged();

    }

    public EntityItem getItem(int pos) {
        return listItem.get(pos);
    }

    public String showHourMinute(String hourMinute) {
        String time = "";
        time = hourMinute.substring(0, 5);
        return time;
    }

    public void setFilter(List<EntityItem> list) {
        listItem = new ArrayList<>();
        listItem.addAll(list);
        notifyDataSetChanged();
    }

    public List<EntityItem> getListItem() {
        return listItem;
    }
}
