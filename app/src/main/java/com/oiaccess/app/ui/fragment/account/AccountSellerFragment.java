package com.oiaccess.app.ui.fragment.account;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.seller.AddProductActivity;
import com.oiaccess.app.ui.activity.seller.ListKomplainActivity;
import com.oiaccess.app.ui.activity.seller.ListTransactionActivity;
import com.oiaccess.app.ui.activity.seller.ManagementStoreActivity;
import com.oiaccess.app.ui.activity.seller.RegisterStoreActivity;
import com.oiaccess.app.ui.activity.seller.WithdrawActivity;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.fragment.BaseFragment;
import com.oiaccess.app.ui.model.balance.BalanceResponse;
import com.oiaccess.app.ui.model.count_status.CountStatus;
import com.oiaccess.app.ui.model.count_status.CountStatusResponse;
import com.oiaccess.app.ui.utils.DialogUtils;
import com.oiaccess.app.ui.utils.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.View.GONE;
import static com.oiaccess.app.ui.data.Constants.API_USER_PROFILE_WALLET;
import static com.oiaccess.app.ui.data.Constants.CATEGORY_ORDER;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLAIN;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_COMPLETED;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_NEW;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_PROCESS;
import static com.oiaccess.app.ui.data.Constants.ORDER_STATUS_ON_SHIPPING;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountSellerFragment extends BaseFragment {


    @BindView(R.id.noStoreLayout)
    LinearLayout noStoreLayout;
    @BindView(R.id.storeLogoIV)
    ImageView storeLogoIV;
    @BindView(R.id.storeNameTV)
    TextView storeNameTV;
    @BindView(R.id.storeStatusTV)
    TextView storeStatusTV;
    @BindView(R.id.balanceTV)
    TextView balanceTV;
    @BindView(R.id.storeLayout)
    LinearLayout storeLayout;

    @BindView(R.id.notif_waiting_confirmation)
    TextView notifWaitingConfirmation;
    @BindView(R.id.notif_on_process)
    TextView notifOnProcess;
    @BindView(R.id.notif_on_shipping)
    TextView notifOnShipping;
    @BindView(R.id.notif_complained)
    TextView notifComplained;
    Unbinder unbinder;
    BalanceResponse balanceResponse;

    String statusSeller, statusVerfikasi;

    Session session;

    public AccountSellerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account_seller, container, false);
        unbinder = ButterKnife.bind(this, view);

        session = new Session(getActivity());

        initView();
        getBalance();
        loadNotif();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
            loadNotif();
        }
    }

    private void initView() {
        statusSeller = session.getUser().getIsSeller();
        if (statusSeller.equalsIgnoreCase("0")) {
            noStoreLayout.setVisibility(View.VISIBLE);
            storeLayout.setVisibility(GONE);
        } else {
            noStoreLayout.setVisibility(View.GONE);
            storeLayout.setVisibility(View.VISIBLE);
            String storeName = session.getUser().getGeraiName();
            String isVerified = session.getUser().getIsVerified();
            if (!storeName.equals("")) {
                storeNameTV.setText(session.getUser().getGeraiName());
            }
            if (isVerified.equals("0")) {
                storeStatusTV.setText("Belum Diverikasi");
                storeStatusTV.setTextColor(getResources().getColor(R.color.red_900));
                storeStatusTV.setCompoundDrawables(getContext().getResources().getDrawable(R.drawable.cancel), null, null, null);
            }

            //Glide.with(getActivity()).load()
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.registerSellerBtn, R.id.newOrderBtn, R.id.shippingBtn, R.id.readyToSendBtn, R.id.arriveBtn, R.id.komplain, R.id.addProductBtn, R.id.managementStoreBtn})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.registerSellerBtn:
                intent = new Intent(getActivity(), RegisterStoreActivity.class);
                startActivity(intent);
                break;
            case R.id.newOrderBtn:
                openActivity(ORDER_STATUS_NEW);
                break;
            case R.id.readyToSendBtn:
                openActivity(ORDER_STATUS_ON_PROCESS);
                break;
            case R.id.shippingBtn:
                openActivity(ORDER_STATUS_ON_SHIPPING);
                break;
            case R.id.arriveBtn:
                openActivity(ORDER_STATUS_COMPLETED);
                break;
            case R.id.komplain:
                openActivity2(ORDER_STATUS_COMPLAIN);
                break;
            case R.id.addProductBtn:
                intent = new Intent(getActivity(), AddProductActivity.class);
                startActivity(intent);
                break;
            case R.id.managementStoreBtn:
                intent = new Intent(getActivity(), ManagementStoreActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void openActivity(String status) {
        Intent intent = new Intent(getActivity(), ListTransactionActivity.class);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    private void openActivity2(String status) {
        Intent intent = new Intent(getActivity(), ListKomplainActivity.class);
        intent.putExtra("status", status);
        startActivity(intent);
    }

    public void getBalance() {
        DialogUtils.openDialog(getActivity());
        AndroidNetworking.get(API_USER_PROFILE_WALLET)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .build()
                .getAsObject(BalanceResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        balanceResponse = (BalanceResponse) response;
                        balanceTV.setText("Rp. " + StringUtils.priceFormatter(String.valueOf(balanceResponse.getResult().getBalance())));
                        DialogUtils.closeDialog();
                    }

                    @Override
                    public void onError(ANError anError) {
                        DialogUtils.closeDialog();
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadNotif() {
        AndroidNetworking.get(CATEGORY_ORDER)
                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
                .addQueryParameter("id_toko", new Session(getActivity()).getUser().getUserId())
                .build()
                .getAsObject(CountStatusResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof CountStatusResponse) {
                            CountStatusResponse response1 = (CountStatusResponse) response;
                            CountStatus countStatus = response1.getResult().get(0);

                            if(countStatus.getmNew()>0){
                                notifWaitingConfirmation.setText("" + countStatus.getmNew());
                                notifWaitingConfirmation.setVisibility(View.VISIBLE);
                            }
                            if(countStatus.getOnprocess()>0){
                                notifOnProcess.setText("" + countStatus.getOnprocess());
                                notifOnProcess.setVisibility(View.VISIBLE);
                            }
                            if(countStatus.getOnshipping()>0){
                                notifOnShipping.setText("" + countStatus.getOnshipping());
                                notifOnShipping.setVisibility(View.VISIBLE);
                            }
                            if(countStatus.getComplained()>0){
                                notifComplained.setText("" + countStatus.getComplained());
                                notifComplained.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
