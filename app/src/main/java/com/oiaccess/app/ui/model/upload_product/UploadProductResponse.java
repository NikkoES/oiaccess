
package com.oiaccess.app.ui.model.upload_product;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UploadProductResponse {

    @SerializedName("result")
    private UploadProduct mUploadProduct;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;

    public UploadProduct getResult() {
        return mUploadProduct;
    }

    public void setResult(UploadProduct uploadProduct) {
        mUploadProduct = uploadProduct;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

}
