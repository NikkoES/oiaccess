package com.oiaccess.app.ui.model.common;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("status")
    private String mStatus;
    @SerializedName("timestamp")
    private String mTimestamp;
    @SerializedName("result")
    private String mResult;

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmTimestamp() {
        return mTimestamp;
    }

    public void setmTimestamp(String mTimestamp) {
        this.mTimestamp = mTimestamp;
    }

    public String getmResult() {
        return mResult;
    }

    public void setmResult(String mResult) {
        this.mResult = mResult;
    }
}
