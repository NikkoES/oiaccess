
package com.oiaccess.app.ui.model.post;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PostProduct {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("harga")
    private Long mHarga;
    @SerializedName("harga_diskon")
    private Long mHargaDiskon;
    @SerializedName("id_category")
    private Integer mIdCategory;
    @SerializedName("id_toko")
    private Long mIdToko;
    @SerializedName("images")
    private List<String> mImages;
    @SerializedName("nama_product")
    private String mNamaProduct;
    @SerializedName("stock")
    private Long mStock;
    @SerializedName("weight")
    private Long mWeight;
    @SerializedName("condition")
    private String condition;
    @SerializedName("min_order")
    private Long mMinOrder;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getHarga() {
        return mHarga;
    }

    public void setHarga(Long harga) {
        mHarga = harga;
    }

    public Long getHargaDiskon() {
        return mHargaDiskon;
    }

    public void setHargaDiskon(Long hargaDiskon) {
        mHargaDiskon = hargaDiskon;
    }

    public Integer getIdCategory() {
        return mIdCategory;
    }

    public void setIdCategory(Integer idCategory) {
        mIdCategory = idCategory;
    }

    public Long getIdToko() {
        return mIdToko;
    }

    public void setIdToko(Long idToko) {
        mIdToko = idToko;
    }

    public List<String> getImages() {
        return mImages;
    }

    public void setImages(List<String> images) {
        mImages = images;
    }

    public String getNamaProduct() {
        return mNamaProduct;
    }

    public void setNamaProduct(String namaProduct) {
        mNamaProduct = namaProduct;
    }

    public Long getStock() {
        return mStock;
    }

    public void setStock(Long stock) {
        mStock = stock;
    }

    public Long getWeight() {
        return mWeight;
    }

    public void setWeight(Long weight) {
        mWeight = weight;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Long getmMinOrder() {
        return mMinOrder;
    }

    public void setmMinOrder(Long mMinOrder) {
        this.mMinOrder = mMinOrder;
    }
}
