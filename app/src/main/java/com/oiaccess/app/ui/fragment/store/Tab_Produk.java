package com.oiaccess.app.ui.fragment.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.product.ProductDetailActivity;
import com.oiaccess.app.ui.adapter.recycler_view.ProdukAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.oiaccess.app.ui.data.Constants.*;

public class Tab_Produk extends Fragment {
    private static final String TAG = "Tab_Produk";
    Unbinder unbinder;
    @BindView(R.id.storeProductList)
    RecyclerView storeProductList;
    ProdukAdapter produkAdapter;
    List<Produk> listProduk = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.produk_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();

        return view;
    }

    private void initView() {

        DialogUtils.openDialog(getActivity());
        storeProductList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        produkAdapter = new ProdukAdapter(getActivity());
        storeProductList.setAdapter(produkAdapter);
        produkAdapter.setOnItemClickListener(new ProdukAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intentDetailProduct = new Intent(getActivity(), ProductDetailActivity.class);
                intentDetailProduct.putExtra("item", produkAdapter.getItem(position));
                startActivity(intentDetailProduct);
            }
        });

//        Log.e("TOKEN", "" + new Session(getActivity()).getUser().getAccessToken());
//        Long idStore = getActivity().getIntent().getLongExtra("idStore",0);
//
//        AndroidNetworking.get(API_PRODUCT_BY_STORE+idStore.toString())
//                .addHeaders("X-AUTH-TOKEN", new Session(getActivity()).getUser().getAccessToken())
//                .build()
//                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
//                    @Override
//                    public void onResponse(Object response) {
//                        if (response instanceof ProductResponse) {
//                            ProductResponse response1 = (ProductResponse) response;
//                            listProduk = response1.getResult();
//
//                            produkAdapter.swap(listProduk);
//                        }
//                        DialogUtils.closeDialog();
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        DialogUtils.closeDialog();
//                        Toast.makeText(getActivity(), "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
//                    }
//                });
    }

    public ProdukAdapter getProdukAdapter() {
        return produkAdapter;
    }
}
