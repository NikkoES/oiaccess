package com.oiaccess.app.ui.activity.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.oiaccess.app.R;
import com.oiaccess.app.ui.activity.product.ProductDetailActivity;
import com.oiaccess.app.ui.activity.product.SearchProductActivity;
import com.oiaccess.app.ui.adapter.recycler_view.ProdukAdapter;
import com.oiaccess.app.ui.data.Session;
import com.oiaccess.app.ui.model.product.ProductResponse;
import com.oiaccess.app.ui.model.product.Produk;
import com.oiaccess.app.ui.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.oiaccess.app.ui.data.Constants.*;

public class ManagementStoreActivity extends AppCompatActivity {

    @BindView(R.id.et_search_product)
    EditText etSearchProduct;
    @BindView(R.id.managementProductList)
    RecyclerView recyclerView;
    @BindView(R.id.lyt_no_result)
    TextView lytNoResult;

    ProdukAdapter adapter2;
    List<Produk> listProduk = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_store);
        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        adapter2 = new ProdukAdapter(this);
        recyclerView.setAdapter(adapter2);
        adapter2.setOnItemClickListener(new ProdukAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intentDetailProduct = new Intent(ManagementStoreActivity.this, AddProductActivity.class);
                intentDetailProduct.putExtra("edit", true);
                intentDetailProduct.putExtra("item", adapter2.getItem(position));
                startActivity(intentDetailProduct);
            }
        });

        AndroidNetworking.get(API_PRODUCT_BY_STORE + new Session(this).getUser().getUserId())
                .addHeaders("X-AUTH-TOKEN", new Session(this).getUser().getAccessToken())
                .addQueryParameter("seller", "true")
                .build()
                .getAsObject(ProductResponse.class, new ParsedRequestListener() {
                    @Override
                    public void onResponse(Object response) {
                        if (response instanceof ProductResponse) {
                            ProductResponse response1 = (ProductResponse) response;
                            listProduk = response1.getResult();

                            if (listProduk.size() > 0) {
                                recyclerView.setVisibility(View.VISIBLE);
                                lytNoResult.setVisibility(View.GONE);
                                adapter2.swap(listProduk);
                            } else {
                                recyclerView.setVisibility(View.GONE);
                                lytNoResult.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(ManagementStoreActivity.this, "Kesalahan teknis !", Toast.LENGTH_SHORT).show();
                    }
                });

        etSearchProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter2.getFilter().filter(s.toString());
            }
        });
    }

    @OnClick({R.id.backBtn, R.id.addProductBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.addProductBtn:
                startActivity(new Intent(this, AddProductActivity.class));
                break;
        }
    }
}
