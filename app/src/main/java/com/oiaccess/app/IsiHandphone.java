package com.oiaccess.app;

public class IsiHandphone {
    int image;
    String judulkategori;


    public IsiHandphone(){
    }

    public IsiHandphone(int images,String judulkategoris){
        this.image = images;
        this.judulkategori = judulkategoris;

    }

    public int getImage(){
        return image;
    }

    public void setImages(int image1){
        this.image = image1;
    }

    public String getJudulkategori(){
        return judulkategori;
    }

    public void setJudulkategori(String judulkategori){
        this.judulkategori = judulkategori;
    }
}
